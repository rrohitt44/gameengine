#ifndef H_SHADERPROGRAM_FillScreenQuad
#define H_SHADERPROGRAM_FillScreenQuad

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsFillScreenQuad(); // get uniform locations
// uniforms
extern GLuint gShaderProgramFillScreenQuad;

void loadTextureSamplerFillScreenQuad(GLuint value);
void bindTexturesFillScreenQuad(struct TexturedModel model);
#endif // !H_SHADERPROGRAM_FillScreenQuad
