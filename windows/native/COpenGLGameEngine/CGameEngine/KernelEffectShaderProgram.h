#ifndef H_SHADERPROGRAM_KERNELEFFECT
#define H_SHADERPROGRAM_KERNELEFFECT

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsKernelEffect(); // get uniform locations
// uniforms
extern GLuint gShaderProgramKernelEffect;

void loadTextureSamplerKernelEffect(GLuint value);
void bindTexturesKernelEffect(struct TexturedModel model);
#endif // !H_SHADERPROGRAM_KERNELEFFECT
