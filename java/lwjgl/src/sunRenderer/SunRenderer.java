package sunRenderer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;
import models.RawModel;
import renderEngine.Loader;

public class SunRenderer {

	private final SunShader shader;

	private static final float[] POSITIONS = { -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, -0.5f };
	private static final String sunTextureFile = "resources/"+"sun"+".png";
	private RawModel quad;
	private int sunTexture;

	public SunRenderer(Loader loader, Matrix4f projectionMatrix) {
		this.shader = new SunShader();
		shader.start();
		shader.connectTextureUnits();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
		this.quad = loader.loadToVAO(POSITIONS, 2);
		this.sunTexture = loader.loadTexture(sunTextureFile);
	}

	public void render(Camera camera) {
		shader.start();
		shader.loadViewMatrix(camera);
		GL30.glBindVertexArray(quad.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		bindTextures();
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, quad.getVertexCount());
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
		shader.stop();
	}

	/**
	 * 
	 */
	private void bindTextures()
	{
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, sunTexture);
	}

	public void cleanUp() {
		shader.cleanUp();
	}

	private void setUpVAO(Loader loader)
	{
		// Just x and z vectex positions here, y is set to 0 in v.shader
		float[] vertices =
		{ -1, -1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1 };
		quad = loader.loadToVAO(vertices, 2);
	}
	
}
