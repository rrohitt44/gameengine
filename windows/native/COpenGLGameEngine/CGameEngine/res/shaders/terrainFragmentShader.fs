#version 330 core

in vec3 out_normal;
in vec2 out_texcoord;
in vec3 out_frag_pos;
in float out_visibility;

out vec4 FragColor;

uniform vec3 u_viewer_position;
uniform vec3 u_sky_color;

struct Material
{
	sampler2D diffuse;
	sampler2D specular;
	sampler2D blendMap_sampler;
	sampler2D backgroundTexture_sampler;
	sampler2D rTexture_sampler;
	sampler2D gTexture_sampler;
	sampler2D bTexture_sampler;
	float shininess;
};

struct Light {
	vec3 direction; // for Directional Lights
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
	int lightType;
	// for point Light
	vec3 position;
	float constant;
	float linear;
	float quadratic;

	// for spot light
	float cutoff; // this also needs spot lights' position and direction vectors
	float outerCutoff;
};

const int MAX_LIGHTS = 6;
uniform Light light[6]; 

uniform Material material;

vec4 calculateFinalColor(vec3 lightDir,float attenuation,float intensity, int lightIndex, vec4 terrainCol);

void main()
{
	// local variables
	float attenuation = 1.0;
	float intensity = 1.0;
	vec4 resultantLight;
	vec3 lightDir;

	// code
	vec4 blendMapColor = texture(material.blendMap_sampler, out_texcoord);
	float backTextureAmount = 1 - (blendMapColor.r + blendMapColor.g + blendMapColor.b);
	vec2 tiledCoords = out_texcoord * 40.0;
	vec4 backgroundTextureColor = texture(material.backgroundTexture_sampler, tiledCoords) * backTextureAmount;
	vec4 rTextureColor = texture(material.rTexture_sampler, tiledCoords) * blendMapColor.r;
	vec4 gTextureColor = texture(material.gTexture_sampler, tiledCoords) * blendMapColor.g;
	vec4 bTextureColor = texture(material.bTexture_sampler, tiledCoords) * blendMapColor.b;
	vec4 totalColor = backgroundTextureColor + rTextureColor + gTextureColor + bTextureColor;

	for(int i=0; i<MAX_LIGHTS;i ++)
	{
		if(light[i].lightType == 2)// flash light
		{
			lightDir = normalize(light[i].position - out_frag_pos); // get light direction vector
			float theta = dot(lightDir, normalize(-light[i].direction));
			float epsilon   = light[i].cutoff - light[i].outerCutoff;
			intensity = clamp((theta - light[i].outerCutoff) / epsilon, 0.0, 1.0);   
			if(theta > light[i].cutoff) // remember that we're working with angles as cosines instead of degrees so a '>' is used.
			{       
			  // do lighting calculations
			  resultantLight += calculateFinalColor(lightDir,attenuation,intensity,i,totalColor);
			}
			else  // else, use ambient light so scene isn't completely dark outside the spotlight.
			  resultantLight += vec4(light[i].ambient * vec3(1.0), 1.0);
		}else
		{

		// Direction light calculations
		if(light[i].lightType == 0) // direction light
		{
			// switch its direction; it's now a direction vector pointing towards the light source
			lightDir = normalize(-light[i].direction); // get light direction vector
		}else if(light[i].lightType == 1)// point light
		{
		
			float distance = length(light[i].position - out_frag_pos);
			attenuation = 1.0 / (light[i].constant + light[i].linear * distance + light[i].quadratic * (distance * distance));
			lightDir = normalize(light[i].position - out_frag_pos); // get light direction vector
		}

			resultantLight += calculateFinalColor(lightDir,attenuation,intensity,i,totalColor);
		}
	}
	FragColor = resultantLight;
	FragColor = mix(vec4(u_sky_color, 1.0), FragColor, out_visibility);
}

vec4 calculateFinalColor(vec3 lightDir,float attenuation,float intensity, int lightIndex, vec4 terrainCol)
{
	// ambient lighting
	//vec3 ambient = light[lightIndex].ambient * vec3(texture(material.diffuse, out_texcoord));

	// diffuse lighting
	vec3 norm = normalize(out_normal);
	
	
	float diff = max(dot(norm, lightDir), 0.0);
	vec4 diffuseTexColor = terrainCol;
	/*if(diffuseTexColor.a < 0.1)
	{
		discard;
	}*/
	vec3 diffuse = diff * diffuseTexColor.rgb * light[lightIndex].diffuse;
	// specular lighting
	vec3 viewDir = normalize(u_viewer_position - out_frag_pos);
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	vec3 specular = spec * vec3(texture(material.specular, out_texcoord)) * light[lightIndex].specular;

	// include attenuation value to ambient diffuse and specular components
	//ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	diffuse *= intensity;
	specular *= intensity;

	//vec4 result = vec4(ambient + diffuse + specular, 1.0) * objectColor;
	//vec3 result = ambient + diffuse + specular;
	vec3 result = diffuse + specular;
	
	return vec4(result, 1.0);
	//return diffuseTexColor;
}