/**
 * 
 */
package guis;

import org.lwjgl.util.vector.Vector2f;

/**
 * @author Rohit Muneshwar
 * @date May 25, 2019
 */
public class GuiTexture
{
	private int textureID;
	private Vector2f position;
	private Vector2f scale;
	
	public GuiTexture(int textureID, Vector2f position, Vector2f scale)
	{
		super();
		this.textureID = textureID;
		this.position = position;
		this.scale = scale;
	}
	// getters/setters
	public int getTextureID()
	{
		return textureID;
	}
	public void setTextureID(int textureID)
	{
		this.textureID = textureID;
	}
	public Vector2f getPosition()
	{
		return position;
	}
	public void setPosition(Vector2f position)
	{
		this.position = position;
	}
	public Vector2f getScale()
	{
		return scale;
	}
	public void setScale(Vector2f scale)
	{
		this.scale = scale;
	}
	
	
}
