package shaders;

import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import entities.Camera;
import entities.Light;
import toolbox.Maths;

/**
 * @author Rohit Muneshwar
 * @date May 22, 2019
 */
public class StaticShader extends ShaderProgram
{
	private static final int MAX_LIGHTS = 4;
	private static final String vertexShaderFile = "src/shaders/vertexShader";
	private static final String fragmentShaderFile = "src/shaders/fragmentShader";
	
	// uniform locations
	private int locationTransformationMatrix;
	private int locationProjectionMatrix;
	private int locationViewMatrix;
	
	private int locationLightPosition[]; // light position
	private int locationLightColor[]; // light color
	private int locationLightAttenuation[]; // attenuation
	
	// shininess
	private int locShineDamper;
	private int locReflectivity;
	
	//fake lighting on objects like grass below surface
	private int locUseFakeLighting;
	
	// for fog calculations
	private int locSkyColor;
	private int locFogDensity;
	private int locFogGradient;
	
	// for texture atlases
	private int locNumberOfRows;
	private int locAtlasOffset;
	
	// clip plane
	private int locClipPlane;
	
	public StaticShader()
	{
		super(vertexShaderFile, fragmentShaderFile);
	}

	@Override
	public void bindAttributes()
	{
		super.bindAttribute(0, "aPosition");
		super.bindAttribute(1, "aTexCoords");
		super.bindAttribute(2, "aNormal");
	}

	@Override
	public void getAllUniformLocations()
	{
		locationTransformationMatrix = super.getUniformLocation("u_transaformation_matrix");
		locationProjectionMatrix = super.getUniformLocation("u_projection_matrix");
		locationViewMatrix = super.getUniformLocation("u_view_matrix");
		
		locShineDamper =super.getUniformLocation("u_shine_damper");
		locReflectivity = super.getUniformLocation("u_reflectivity");
		locUseFakeLighting = super.getUniformLocation("u_use_fake_lighting");
		locSkyColor = super.getUniformLocation("u_sky_color");
		locFogDensity = super.getUniformLocation("u_density");
		locFogGradient = super.getUniformLocation("u_gradient");
		
		locNumberOfRows = super.getUniformLocation("u_number_of_rows");
		locAtlasOffset = super.getUniformLocation("u_offset");
		
		// multiple lights
		locationLightPosition = new int[MAX_LIGHTS];
		locationLightColor = new int[MAX_LIGHTS];
		locationLightAttenuation = new int[MAX_LIGHTS];
		for(int i=0; i<MAX_LIGHTS; i++)
		{
			locationLightPosition[i] = super.getUniformLocation("u_light_position["+i+"]");
			locationLightColor[i] = super.getUniformLocation("u_light_color["+i+"]");
			locationLightAttenuation[i] = super.getUniformLocation("u_attenuation["+i+"]");
		}
		
		// cliping plane
		locClipPlane = super.getUniformLocation("u_plane");
	}
	
	/**
	 * @param plane
	 * Load a cliping plane
	 */
	public void loadClipPlane(Vector4f plane)
	{
		super.loadVector(locClipPlane, plane);
	}
	
	/**
	 * @param density
	 * @param gradient
	 * Loads fog parameters
	 */
	public void loadFogParameters(float density, float gradient)
	{
		super.loadFloat(locFogDensity, density);
		super.loadFloat(locFogGradient, gradient);
	}
	
	/**
	 * @param rows
	 * Loads number of rows present in the texture atlas
	 */
	public void loadNumberOfRows(int rows)
	{
		super.loadFloat(locNumberOfRows, rows);
	}
	
	/**
	 * @param x
	 * @param y
	 * Loads the offset for the choosen texture present in the texture atlas
	 */
	public void loadTextureAtlasOffsets(float x, float y)
	{
		super.load2DVector(locAtlasOffset, new Vector2f(x, y));
	}
	/**
	 * @param r
	 * @param g
	 * @param b
	 * Loads sky color
	 */
	public void loadSkyColor(float r, float g, float b)
	{
		loadSkyColor(new Vector3f(r,g,b));
	}
	
	/**
	 * @param skyColor
	 *  Loads sky color
	 */
	public void loadSkyColor(Vector3f skyColor)
	{
		super.loadVector(locSkyColor, skyColor);
	}
	
	public void setUseFakeLighting(boolean useFakeLighting)
	{
		super.loadBoolean(locUseFakeLighting, useFakeLighting);
	}

	/**
	 * @param transformationMatrix
	 * transformation means translation, rotation and scaling
	 */
	public void loadTransformationMatrix(Matrix4f transformationMatrix)
	{
		super.loadMatrix(locationTransformationMatrix, transformationMatrix);
	}
	
	/**
	 * @param damper
	 * @param reflectivity
	 * Loads the shine variables
	 */
	public void loadShineVariables(float damper, float reflectivity)
	{
		super.loadFloat(locShineDamper, damper);
		super.loadFloat(locReflectivity, reflectivity);
	}
	
	/**
	 * @param light
	 * Loads the light uniform variables
	 */
	public void loadLights(List<Light> lights)
	{
		for(int i=0; i<MAX_LIGHTS; i++)
		{
			if(i<lights.size())
			{
				super.loadVector(locationLightPosition[i], lights.get(i).getPosition());
				super.loadVector(locationLightColor[i], lights.get(i).getColor());
				super.loadVector(locationLightAttenuation[i], lights.get(i).getAttenuation());
			}else
			{
				super.loadVector(locationLightPosition[i], new Vector3f(0,0,0));
				super.loadVector(locationLightColor[i], new Vector3f(0, 0, 0));
				super.loadVector(locationLightAttenuation[i], new Vector3f(1,0,0));
			}
		}
	}
	
	/**
	 * @param projectionMatrix
	 * loads the projection matrix
	 */
	public void loadProjectionMatrix(Matrix4f projectionMatrix)
	{
		super.loadMatrix(locationProjectionMatrix, projectionMatrix);
	}
	
	/**
	 * @param camera
	 * loads the projection matrix
	 */
	public void loadViewMatrix(Camera camera)
	{
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		super.loadMatrix(locationViewMatrix, viewMatrix);
	}
}
