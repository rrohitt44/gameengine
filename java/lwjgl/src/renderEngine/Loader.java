package renderEngine;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;
import models.RawModel;
import textures.TextureData;

/**
 * @author rohit
 * Date: 20/05/2019
 * 
 * Loader class of the entities
 */
public class Loader
{
	// instance variables
	List<Integer> vaoIDs = new ArrayList<>();
	List<Integer> vboIDs = new ArrayList<>();
	List<Integer> textureIDs = new ArrayList<>();
	
	/**
	 * creates the Vertex Array Object and make it as active
	 */
	private int createVAO()
	{
		int vaoID = GL30.glGenVertexArrays(); // create empty VAO
		vaoIDs.add(vaoID);
		GL30.glBindVertexArray(vaoID); // bind VAO to make it active
		return vaoID;
	}
	
	/**
	 * @param attributeNumber
	 * @param data
	 * Store the data in attribute list
	 * To do this, we need to create VBO and put data into it
	 */
	private void storeDataInAttributeList(int attributeNumber, int coordinateSize, float[] data)
	{
		// create VBO
		int vboID = GL15.glGenBuffers();
		vboIDs.add(vboID);
		// bind it to make it active
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		
		// first covert data to FloadBuffer object
		FloatBuffer buffer = convertFloatArrayToFloatBuffer(data);
		
		// store this data into VBO
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		
		// map this buffer data to the shaders' attribute number
		GL20.glVertexAttribPointer(
				attributeNumber, // attribute number in the shader
				coordinateSize, // each vertex is of coordinateSize floats
				GL11.GL_FLOAT, // type of data
				false, // no need to normalize the data; it is already between -1 and 1
				0, // stride
				0 // offset
				);
		
		// unbind the vbo now
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}
	
	private FloatBuffer convertFloatArrayToFloatBuffer(float[] data)
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	/**
	 * unbind VAO
	 */
	private void unbindVAO()
	{
		GL30.glBindVertexArray(0); // unbind VAO
	}
	
	/**
	 * @param positions
	 * @param texCoords 
	 * @param normals
	 * @return
	 * Loads the position data to the VAO and returns the RawModel
	 */
	public RawModel loadToVAO(float[] positions, float[] texCoords, float[] normals, int[] indices)
	{
		// create the VAO and bind it
		int vaoID = createVAO();
		
		// bind indices buffer
		bindIndicesBuffer(indices);
		
		// store the data in the attribute list
		storeDataInAttributeList(0, 3, positions);
		storeDataInAttributeList(1, 2, texCoords);
		storeDataInAttributeList(2, 3, normals);
		
		//unbind the VAO now
		unbindVAO();
		
		// create Raw model from the above data
		RawModel model = new RawModel(vaoID, indices.length);
		return model;
	}
	
	/**
	 * @param positions
	 * @param textureCoords
	 * @param normals
	 * @param tangents
	 * @param indices
	 * @return
	 * Loads the data including tangents to the vao
	 */
	public RawModel loadToVAO(float[] positions, float[] textureCoords, float[] normals, float[] tangents,
			int[] indices) 
	{
		int vaoID = createVAO();
		bindIndicesBuffer(indices);
		storeDataInAttributeList(0, 3, positions);
		storeDataInAttributeList(1, 2, textureCoords);
		storeDataInAttributeList(2, 3, normals);
		storeDataInAttributeList(3, 3, tangents);
		unbindVAO();
		return new RawModel(vaoID, indices.length);
	}
	
	
	/**
	 * @param positions
	 * #param dimensions
	 * @return
	 * Loads the position data to the VAO and returns the RawModel
	 */
	public RawModel loadToVAO(float[] positions, int dimensions)
	{
		// create the VAO and bind it
		int vaoID = createVAO();
		// store the data in the attribute list
		storeDataInAttributeList(0, dimensions, positions);
		//unbind the VAO now
		unbindVAO();
		
		// create Raw model from the above data
		RawModel model = new RawModel(vaoID, positions.length/2);
		return model;
	}
	
	/**
	 * @param fileName
	 * @return
	 * decodes the texture file and put the data in the TextureData class
	 */
	private TextureData decodeTextureFile(String fileName) 
	{
		int width = 0;
		int height = 0;
		ByteBuffer buffer = null;
		try {
			FileInputStream in = new FileInputStream(fileName);
			PNGDecoder decoder = new PNGDecoder(in);
			width = decoder.getWidth();
			height = decoder.getHeight();
			buffer = ByteBuffer.allocateDirect(4 * width * height);
			decoder.decode(buffer, width * 4, Format.RGBA);
			buffer.flip();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Tried to load texture " + fileName + ", didn't work");
			System.exit(-1);
		}
		return new TextureData(buffer, width, height);
	}
	
	/**
	 * @param textureFiles
	 * @return
	 * Loads the files in the paramenters to the cubemap and returns the id of the cubemap
	 */
	public int loadCubeMap(String[] textureFiles) 
	{
		// generate the texture
		int texID = GL11.glGenTextures();
		
		// make it as an active texture
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		
		// bind the texture created above to the cubemap
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, texID);

		// loads the texture files to cubemap
		for (int i = 0; i < textureFiles.length; i++) {
			TextureData data = decodeTextureFile("resources/" + textureFiles[i] + ".png");
			// Right-Left-Top-Bottom-Back-Front
			GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL11.GL_RGBA, data.getWidth(), data.getHeight(), 0,
					GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, data.getBuffer());
		}
		
		// set the texture parameters
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		textureIDs.add(texID);
		return texID;
	}
	
	/**
	 * @param fileName
	 * @return
	 * Loads a texture into memory and returns the texture id
	 */
	public int loadTexture(String fileName)
	{
		// local variables
		Texture texture = null; // for storing texture
		
		try
		{
			File textureFile = new File("resources/"+fileName+".png");
			FileInputStream fis = new FileInputStream(textureFile);
			
			// code
			texture = TextureLoader.getTexture("PNG", fis);
			GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D); // generates mipmapping
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
			GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -0.4f); // level of detail bias
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		int textureID = texture.getTextureID();
		textureIDs.add(textureID);
		return textureID;
	}
	
	/**
	 * Clean up the memory objects created for rendering
	 */
	public void cleanUp()
	{
		// delete VAOs
		for(int vao: vaoIDs)
		{
			GL30.glDeleteVertexArrays(vao);
		}
		
		// delete VBOs
		for(int vbo: vboIDs)
		{
			GL15.glDeleteBuffers(vbo);
		}
		
		// delete textures
		for(int texId: textureIDs)
		{
			GL11.glDeleteTextures(texId);
		}
	}
	
	/**
	 * @param indices
	 * Bind the indices buffer to the VAO
	 */
	public void bindIndicesBuffer(int[] indices)
	{
		// create VBO
		int iboID = GL15.glGenBuffers();
		vboIDs.add(iboID);
		// bind it to make it active
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, iboID);
		
		// first covert data to FloadBuffer object
		IntBuffer buffer = convertIntArrayToIntBuffer(indices);
		
		// store this data into VBO
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}
	
	private IntBuffer convertIntArrayToIntBuffer(int[] data)
	{
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
}
