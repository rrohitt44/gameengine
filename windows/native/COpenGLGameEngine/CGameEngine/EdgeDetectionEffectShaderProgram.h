#ifndef H_SHADERPROGRAM_EDGE_DETECTION_EFFECT
#define H_SHADERPROGRAM_EDGE_DETECTION_EFFECT

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsEdgeDetectionEffect(); // get uniform locations
// uniforms
extern GLuint gShaderProgramEdgeDetectionEffect;

void loadTextureSamplerEdgeDetectionEffect(GLuint value);
void bindTexturesEdgeDetectionEffect(struct TexturedModel model);
#endif // !H_SHADERPROGRAM_EDGE_DETECTION_EFFECT
