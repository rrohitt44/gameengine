#ifndef H_MESH
#define H_MESH

// headers
#include<iostream>
#include<stdio.h>
#include<Windows.h>
#include<vector>

#include <string>
#include <fstream>
#include <sstream>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include"Models.h"
#include"GameUtils.h"

using namespace std;


class Mesh
{
public:
	// mesh data
	std::vector<struct Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<struct Texture> textures;
	unsigned int VAO;

	// constructor
	Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<struct Texture> textures)
	{
		this->vertices = vertices;
		this->indices = indices;
		this->textures = textures;

		// now that we have all the required data, set the vertex buffers and its attribute pointers.
		setupMesh();
	}

	// function
	void draw(GLuint shaderProgramObject)
	{
		// bind appropriate textures
		unsigned int diffuseNr = 1;
		unsigned int specularNr = 1;
		unsigned int normalNr = 1;
		unsigned int heightNr = 1;

		for (unsigned int i = 0; i < textures.size(); i++)
		{
			// activate proper texture units before binding
			glActiveTexture(GL_TEXTURE0+i);
			// retrieve texture number (the N in diffuse_textureN)
			string number;
			string name = textures[i].type;

			if (name == "texture_diffuse")
			{
				number = std::to_string(diffuseNr++);
			}else if (name == "texture_specular")
			{
				number = std::to_string(specularNr++);
			}else if (name == "texture_normal")
			{
				number = std::to_string(normalNr++);
			}else if (name == "texture_height")
			{
				number = std::to_string(heightNr++);
			}

			// get uniform location
			unsigned int loc = glGetUniformLocation(shaderProgramObject, ("material."+name+number).c_str());

			// set uniform
			glUniform1i(loc, i);

			// finally bind the texture
			glBindTexture(GL_TEXTURE_2D, textures[i].textureID);
		}

		// Draw mesh
		glBindVertexArray(VAO);

		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		// unbind
		glBindVertexArray(0);

		// always good practice to set everything back to defaults once configured.
		glActiveTexture(GL_TEXTURE0);
	}

private:
	// render data
	unsigned int VBO, EBO;

	// functions
	void setupMesh()
	{
		// create buffers/arrays
		glGenVertexArrays(1, &VAO);
		// bind VAO
		glBindVertexArray(VAO);
		gVAOList.push_back(VAO); // for clearance
		// create EBO
		glGenBuffers(1, &EBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO); // bind EBO
		gVBOList.push_back(EBO); // for clearance
		//pass data to EBO
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

		// create VBO
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO); // bind VBO
		gVBOList.push_back(VBO); // for clearance
		// pass data to buffers
		// A great thing about structs is that their memory layout is sequential for all its items.
		// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
		// again translates to 3/2 floats which translates to a byte array.
		glBufferData(
			GL_ARRAY_BUFFER, // target
			vertices.size() * sizeof(Vertex), // size
			&vertices[0], // data
			GL_STATIC_DRAW // usage
			);

		// set vertex attribute pointers
		// vertex Positions
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		// vertex normals
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
		// vertex texture coords
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
		// vertex tangent
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
		// vertex bitangent
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, biTangent));

		glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind VBO
		// unbind VAO
		glBindVertexArray(0);
	}
};
#endif // !H_MESH
