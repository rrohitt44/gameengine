#ifndef H_SKYBOX_RENDERER
#define H_SKYBOX_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"SkyboxShaderProgram.h"
#include"Camera.h"
#include"framebuffers.h"
#include"PostProcessing.h"

extern struct Entity gCubeMapEntity;

void prepareSkybox(glm::mat4 projectionMatrix);
void renderSkybox(); // render entities using FBO textures
#endif // !H_SKYBOX_RENDERER
