/**
 * 
 */
package entities;

import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;
import renderEngine.DisplayManager;
import terrains.Terrain;

/**
 * @author Rohit Muneshwar
 * @date May 24, 2019
 * Player in out game
 */
public class Player extends Entity
{
	// player movement
	public static final float RUN_SPEED = 20; // units per seconds
	public static final float TURN_SPEED = 160; // degrees per seconds
	private float currentSpeed = 0;
	private float currentTurnSpeed = 0;
	//jumping
	private static final float GRAVITY = -50;
	private static final float JUMP_POWER = 30;
	private float upwardSpeed = 0;
	private static final float TERRAIN_HEIGHT = 0;
	private boolean isInAir = false;
	
	/**
	 * @param texturedModel
	 * @param position
	 * @param rotX
	 * @param rotY
	 * @param rotZ
	 * @param scale
	 */
	public Player(TexturedModel texturedModel, Vector3f position, float rotX, float rotY, float rotZ, float scale)
	{
		super(texturedModel, position, rotX, rotY, rotZ, scale);
	}

	/**
	 * Move the player in the world
	 */
	public void move(Terrain terrain)
	{
		checkInputs(); // check use inputs
		super.increaseRotation(0, currentTurnSpeed * DisplayManager.getFrameTimeSeconds(), 0);
		float distance = currentSpeed * DisplayManager.getFrameTimeSeconds();
		float dx = (float) (distance * Math.sin(Math.toRadians(super.getRotY())));
		float dz = (float) (distance * Math.cos(Math.toRadians(super.getRotY())));
		super.increasePosition(dx, 0, dz);
		upwardSpeed += GRAVITY * DisplayManager.getFrameTimeSeconds();
		super.increasePosition(0, upwardSpeed * DisplayManager.getFrameTimeSeconds(), 0);
		
		if(terrain.getHeights()!=null)
		{
			float terrainHeight = terrain.generateHeightOfTerrain(super.getPosition().x, super.getPosition().z);
			if(super.getPosition().y < terrainHeight)
			{
				upwardSpeed = 0;
				super.getPosition().y = terrainHeight;
				isInAir = false;
			}
		}
	}
	
	/**
	 * jump player upwards
	 */
	private void jump()
	{
		// if not in the air then only jump
		if(!isInAir)
		{
			upwardSpeed = JUMP_POWER;
			isInAir = true;
		}
	}
	
	/**
	 * Check the user inputs for player movement
	 */
	private void checkInputs()
	{
		// W and S keys
		if(Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_UP)) // move ahead
		{
			this.currentSpeed = RUN_SPEED;
		}else if(Keyboard.isKeyDown(Keyboard.KEY_S) || Keyboard.isKeyDown(Keyboard.KEY_DOWN)) // move back
		{
			this.currentSpeed = -RUN_SPEED;
		}else
		{
			this.currentSpeed = 0;
		}
		
		// A and D keys
		if(Keyboard.isKeyDown(Keyboard.KEY_A) || Keyboard.isKeyDown(Keyboard.KEY_LEFT)) // move left
		{
			this.currentTurnSpeed = TURN_SPEED; // clock-wise
		}else if(Keyboard.isKeyDown(Keyboard.KEY_D) || Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) // move right
		{
			this.currentTurnSpeed = -TURN_SPEED; // anti-clockwise
		}else
		{
			this.currentTurnSpeed = 0;
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
		{
			jump();
		}
	}

}
