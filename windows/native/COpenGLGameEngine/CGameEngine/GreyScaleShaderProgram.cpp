#include"GrayScaleShaderProgram.h"

using namespace std;

GLuint gShaderProgramGrayScale;
static GLuint gLocTextureSamplerGrayScale;

// gets all uniform locations
void getAllUniformLocationsGrayScale()
{
	gLocTextureSamplerGrayScale = glGetUniformLocation(gShaderProgramGrayScale, "u_texture_sampler");
}

void loadTextureSamplerGrayScale(GLuint value)
{
	setInt(gLocTextureSamplerGrayScale, value);
}

void bindTexturesGrayScale(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}