/**
 * 
 */
package renderEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import engineTester.MainGameLoop;
import entities.Camera;
import entities.Entity;
import entities.Light;
import models.TexturedModel;
import normalMappingRenderer.NormalMappingRenderer;
import shaders.StaticShader;
import shaders.TerrainShader;
import skybox.SkyboxRenderer;
import sunRenderer.SunRenderer;
import terrains.Terrain;

/**
 * @author Rohit Muneshwar
 * @date May 22, 2019
 */
public class MasterRenderer
{
	// projection matrix constants
	private static final float FOV = 70;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 1000f;
	
	// sky color constants
	public static final float RED = 0.5444f;
	public static final float GREEN = 0.62f;
	public static final float BLUE = 0.69f;
	
	// fog params
	private static final float FOG_DENSITY = 0.0015f;
	private static final float FOG_GRADIENT = 10.0f;
	
	// entities
	private StaticShader shader = new StaticShader();
	private EntityRenderer renderer; // entity renderer
	
	// terrains
	private TerrainRenderer terrainRenderer; // terrain renderer
	private TerrainShader terrainShader = new TerrainShader(); // terrain shader
	
	private Matrix4f projectionMatrix; // projection matrix
	private Map<TexturedModel, List<Entity>> entities = new HashMap<>();
	private Map<TexturedModel, List<Entity>> normalMapEntities = new HashMap<>(); // normal mapped entities
	private List<Terrain> terrains = new ArrayList<>();
	
	// skybox renderer
	private SkyboxRenderer skyboxRenderer;
	
	// normal mapping renderer
	private NormalMappingRenderer normalMappingRenderer;
	
	public MasterRenderer(Loader loader)
	{
		enableCulling();
		createProjectionMatrix(); // creates the projection matrix
		renderer = new EntityRenderer(shader, projectionMatrix); // create entity renderer
		terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix); // create terrain renderer
		skyboxRenderer =new SkyboxRenderer(loader, projectionMatrix); // create skybox renderer
		normalMappingRenderer = new NormalMappingRenderer(projectionMatrix); // create normal mapping renderer
	}
	
	public Matrix4f getProjectionMatrix()
	{
		return projectionMatrix;
	}


	public void setProjectionMatrix(Matrix4f projectionMatrix)
	{
		this.projectionMatrix = projectionMatrix;
	}


	/**
	 * Enables backface culling
	 */
	public static void enableCulling()
	{
		// cull the back faces as those are not necessary to render 
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}
	
	/**
	 * Disable backface culling
	 */
	public static void disableCulling()
	{
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
	
	/**
	 * Enables alpha blending
	 */
	public static void enableAlphaBlending()
	{
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	}
	
	/**
	 * Disable Alpha blending
	 */
	public static void disableAlphaBlending()
	{
		GL11.glDisable(GL11.GL_BLEND);
	}
	/**
	 * Prepare the models for rendering
	 */
	public void prepare()
	{
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // clear the color buffer
		GL11.glClearColor(RED, GREEN, BLUE, 1.0f);
	}
	
	
	/**
	 * Creates the projection matrix
	 */
	private void createProjectionMatrix()
	{
		float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
		float y_scale = (float) ((1f / Math.tan(Math.toRadians(FOV / 2f))));
		float x_scale = y_scale / aspectRatio;
		float frustum_length = FAR_PLANE - NEAR_PLANE;
		
		projectionMatrix = new Matrix4f();
		projectionMatrix.m00 = x_scale;
		projectionMatrix.m11 = y_scale;
		projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
		projectionMatrix.m33 = 0;
	}
	
	/**
	 * @param sun
	 * @param camera
	 * Render the entities
	 */
	public void render(List<Light> lights, Camera camera, Vector4f clipPlane)
	{
		prepare();
		
		// render entities
		shader.start();
		shader.loadClipPlane(clipPlane);
		shader.loadSkyColor(RED, GREEN, BLUE);
		shader.loadLights(lights);
		shader.loadViewMatrix(camera);
		shader.loadFogParameters(FOG_DENSITY, FOG_GRADIENT);
		renderer.render(entities);
		shader.stop();
		
		// render normal mapped entities
		normalMappingRenderer.render(normalMapEntities, clipPlane, lights, camera);
		
		// render terrain
		terrainShader.start();
		terrainShader.loadSkyColor(RED, GREEN, BLUE);
		terrainShader.loadClipPlane(clipPlane);
		// temporary adjustment for lights on only one terrain
		for(Terrain ter : terrains)
		{
			if(ter.isHasMultipleLights())
			{
				terrainShader.loadLights(lights);
			}else
			{
				List<Light> lts = new ArrayList<>();
				lts.add(MainGameLoop.sun);
				terrainShader.loadLights(lts);
			}
		}
		
		terrainShader.loadViewMatrix(camera);
		terrainShader.loadFogParameters(FOG_DENSITY, FOG_GRADIENT);
		terrainRenderer.render(terrains);
		terrainShader.stop();
		
		// render skybox
		skyboxRenderer.render(camera, RED, GREEN, BLUE);
		
		// clear entities, terrains
		terrains.clear();
		entities.clear();
		normalMapEntities.clear();
	}
	
	/**
	 * @param entity
	 * adds entities in the entity map
	 */
	public void processEntity(Entity entity)
	{
		TexturedModel texturedModel = entity.getTexturedModel();
		List<Entity> batch = entities.get(texturedModel);
		if(batch!=null)
		{
			batch.add(entity);
		}else
		{
			List<Entity> newBatch = new ArrayList<>();
			newBatch.add(entity);
			entities.put(texturedModel, newBatch);
		}
	}
	
	/**
	 * @param entity
	 * adds entities in the normal mapped entity map
	 */
	public void processNormalMappedEntity(Entity entity)
	{
		TexturedModel texturedModel = entity.getTexturedModel();
		List<Entity> batch = normalMapEntities.get(texturedModel);
		if(batch!=null)
		{
			batch.add(entity);
		}else
		{
			List<Entity> newBatch = new ArrayList<>();
			newBatch.add(entity);
			normalMapEntities.put(texturedModel, newBatch);
		}
	}
	
	/**
	 * @param terrain
	 * adds a terrain to the terrain list
	 */
	public void processTerrain(Terrain terrain)
	{
		terrains.add(terrain);
	}
	
	/**
	 * Does all the clean up tasks
	 */
	public void cleanUp()
	{
		normalMappingRenderer.cleanUp();
		shader.cleanUp(); // clean up entity shader objects
		terrainShader.cleanUp(); // clean up the terrain shader objects
	}

	/**
	 * @param entityList
	 * @param terrainList
	 * @param lights
	 * @param camera
	 */
	public void renderScene(List<Entity> entityList, List<Entity> normalMappedEntityList, List<Terrain> terrainList, List<Light> lights, 
			Camera camera, Vector4f clipPlane)
	{
		// terrain rendering
		//renderer.processTerrain(planeTerrain);
		for(Terrain terrain: terrainList)
		{
			processTerrain(terrain);
		}
		
		for(Entity entity: entityList)
		{
			processEntity(entity);
		}
		
		for(Entity entity: normalMappedEntityList)
		{
			processNormalMappedEntity(entity);
		}
		
		// render the scene now
		render(lights, camera, clipPlane);
	}
}
