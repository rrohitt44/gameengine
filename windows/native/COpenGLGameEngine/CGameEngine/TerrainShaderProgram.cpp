#include"TerrainShaderProgram.h"

using namespace std;

GLuint gShaderProgramTerrain;
static GLuint gLocModelMatrixTerrain;
static GLuint gLocViewMatrixTerrain;
static GLuint gLocProjectionMatrixTerrain;
static GLuint gLocLightAmbientTerrain[6];
static GLuint gLocLightDiffuseTerrain[6];
static GLuint gLocLightSpecularTerrain[6];
static GLuint gLocLightPositionTerrain[6];
static GLuint gLocLightTypeTerrain[6];
static GLuint gLocLightCutoffTerrain[6];
static GLuint gLocLightOuterCutoffTerrain[6];
static GLuint gLocLightDirectionTerrain[6]; // light direction for directional light
static GLuint gLocLightAttenuationLineraTerrain[6];
static GLuint gLocLightAttenuationQuadraticTerrain[6];
static GLuint gLocLightAttenuationConstantTerrain[6];
static GLuint gLocViewerPositionTerrain;
static GLuint gLocMaterialDiffuseTerrain;
static GLuint gLocMaterialSpecularTerrain;
static GLuint gLocMaterialBlendMapTextureSamplerTerrain;
static GLuint gLocMaterialBackgroundTextureSamplerTerrain;
static GLuint gLocMaterialrTextureSamplerTerrain;
static GLuint gLocMaterialgTextureSamplerTerrain;
static GLuint gLocMaterialbTextureSamplerTerrain;
static GLuint gLocMaterialShininessTerrain;
static GLuint gLocSkyColor;

// gets all uniform locations
void getAllUniformLocationsTerrain()
{
	gLocModelMatrixTerrain = glGetUniformLocation(gShaderProgramTerrain, "u_model_matrix");
	gLocViewMatrixTerrain = glGetUniformLocation(gShaderProgramTerrain, "u_view_matrix");
	gLocProjectionMatrixTerrain = glGetUniformLocation(gShaderProgramTerrain, "u_projection_matrix");

	for(int i=0; i<6; i++)
	{
		std::string result = "light[" + std::to_string(i) + "].ambient";
	gLocLightAmbientTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].diffuse";
	gLocLightDiffuseTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].specular";
	gLocLightSpecularTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].position";
	gLocLightPositionTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].lightType";
	gLocLightTypeTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].direction";
	gLocLightDirectionTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].constant";
	gLocLightAttenuationConstantTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].linear";
	gLocLightAttenuationLineraTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].quadratic";
	gLocLightAttenuationQuadraticTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].cutoff";
	gLocLightCutoffTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	result = "light[" + std::to_string(i) + "].outerCutoff";
	gLocLightOuterCutoffTerrain[i] = glGetUniformLocation(gShaderProgramTerrain, result.c_str());
	}
	gLocViewerPositionTerrain = glGetUniformLocation(gShaderProgramTerrain, "u_viewer_position");
	gLocMaterialDiffuseTerrain = glGetUniformLocation(gShaderProgramTerrain, "material.diffuse");
	gLocMaterialSpecularTerrain = glGetUniformLocation(gShaderProgramTerrain, "material.specular");
	gLocMaterialShininessTerrain = glGetUniformLocation(gShaderProgramTerrain, "material.shininess");
	gLocMaterialBlendMapTextureSamplerTerrain = glGetUniformLocation(gShaderProgramTerrain, "material.blendMap_sampler");
	gLocMaterialBackgroundTextureSamplerTerrain = glGetUniformLocation(gShaderProgramTerrain, "material.backgroundTexture_sampler");
	gLocMaterialrTextureSamplerTerrain = glGetUniformLocation(gShaderProgramTerrain, "material.rTexture_sampler");
	gLocMaterialgTextureSamplerTerrain = glGetUniformLocation(gShaderProgramTerrain, "material.gTexture_sampler");
	gLocMaterialbTextureSamplerTerrain = glGetUniformLocation(gShaderProgramTerrain, "material.bTexture_sampler");
	gLocSkyColor = glGetUniformLocation(gShaderProgramTerrain, "u_sky_color");
}



// bind texture units
void bindTextureUnitsTerrain(struct TexturedModel texturedModel, struct EntityMapMaterial entityMapMapterial)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.diffuse);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.specular);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.blendMap_sampler);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.backgroundTexture_sampler);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.rTexture_sampler);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.gTexture_sampler);
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.bTexture_sampler);
}


void loadViewerPositionTerrain(glm::vec3 viewerPos)
{
	setVector3v(gLocViewerPositionTerrain, viewerPos);
}


void loadDiffuseSamplerTerrain(GLuint samplerID)
{
	setInt(gLocMaterialDiffuseTerrain, samplerID);
}

void loadSpecularSamplerTerrain(GLuint samplerID)
{
	setInt(gLocMaterialSpecularTerrain, samplerID);
}

void loadSamplersTerrain()
{
	setInt(gLocMaterialDiffuseTerrain, 0);
	setInt(gLocMaterialSpecularTerrain, 1);
	setInt(gLocMaterialBlendMapTextureSamplerTerrain, 2);
	setInt(gLocMaterialBackgroundTextureSamplerTerrain, 3);
	setInt(gLocMaterialrTextureSamplerTerrain, 4);
	setInt(gLocMaterialgTextureSamplerTerrain, 5);
	setInt(gLocMaterialbTextureSamplerTerrain, 6);
}

void loadModelMatrixTerrain(glm::mat4 transformationMatrix)
{
	setMat4(gLocModelMatrixTerrain, transformationMatrix);
}

void loadViewMatrixTerrain(glm::mat4 transformationMatrix)
{
	setMat4(gLocViewMatrixTerrain, transformationMatrix);
}

void loadProjectionMatrixTerrain(glm::mat4 transformationMatrix)
{
	setMat4(gLocProjectionMatrixTerrain, transformationMatrix);
}

void loadMaterialPropertiesTerrain(struct EntityMapMaterial material)
{
	setFloat(gLocMaterialShininessTerrain, material.shininess);
}

void loadSkyColorTerrain(glm::vec3 objectColor)
{
	setVector3v(gLocSkyColor, objectColor);
}

void loadLightPropertiesTerrain(std::vector<struct Light> lights, struct Camera* camera)
{
	for (int i = 0; i < lights.size(); i++)
	{
		setVector3v(gLocLightAmbientTerrain[i], lights[i].ambient);
		setVector3v(gLocLightDiffuseTerrain[i], lights[i].diffuse);
		setVector3v(gLocLightSpecularTerrain[i], lights[i].specular);

		// for directional light
		if (lights[i].lightType == 0) // if directional light
		{
			setVector3v(gLocLightDirectionTerrain[i], lights[i].lightDirection);
		}

		// for point light
		if (lights[i].lightType == 1)
		{
			setVector3v(gLocLightPositionTerrain[i], lights[i].lightPosition);
			setFloat(gLocLightAttenuationConstantTerrain[i], lights[i].constant);
			setFloat(gLocLightAttenuationLineraTerrain[i], lights[i].linear);
			setFloat(gLocLightAttenuationQuadraticTerrain[i], lights[i].quadratic);
		}

		// for spot light
		if (lights[i].lightType == 2)
		{
			setVector3v(gLocLightPositionTerrain[i], camera->position);
			setVector3v(gLocLightDirectionTerrain[i], camera->cameraFront);
			setFloat(gLocLightCutoffTerrain[i], lights[i].cutoff);
			setFloat(gLocLightOuterCutoffTerrain[i], lights[i].outerCutoff);
		}
		setInt(gLocLightTypeTerrain[i], lights[i].lightType);
	}
}
