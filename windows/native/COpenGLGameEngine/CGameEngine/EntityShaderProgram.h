#ifndef H_SHADERPROGRAM_ENTITY
#define H_SHADERPROGRAM_ENTITY

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"
#include"Camera.h"

void getAllUniformLocations(); // get uniform locations

// uniforms
extern GLuint gShaderProgramEntity;
extern GLuint gLocColor;
extern GLuint gLocPositionOffset;
extern GLuint gLocBricksSampler;
extern GLuint gLocFaceSampler;
extern GLuint gLocMixParam;
extern GLuint gLocModelMatrix;
extern GLuint gLocViewMatrix;
extern GLuint gLocProjectionMatrix;
extern GLuint gLocObjectColor;
extern GLuint gLocLightAmbient[6];
extern GLuint gLocLightDiffuse[6];
extern GLuint gLocLightSpecular[6];
extern GLuint gLocLightPosition[6];
extern GLuint gLocLightType[6];
extern GLuint gLocLightCutoff[6];
extern GLuint gLocLightOuterCutoff[6];
extern GLuint gLocLightDirection[6]; // light direction for directional light
extern GLuint gLocLightAttenuationLinera[6];
extern GLuint gLocLightAttenuationQuadratic[6];
extern GLuint gLocLightAttenuationConstant[6];
extern GLuint gLocViewerPosition;
extern GLuint gLocMaterialAmbient;
extern GLuint gLocMaterialDiffuse;
extern GLuint gLocMaterialSpecular;
extern GLuint gLocMaterialShininess;

void loadPositionOffset(float offset);
void bindTextureUnits(struct TexturedModel modelTexture);
void loadBricksTextureSampler(GLuint samplerID);
void loadFaceTextureSampler(GLuint samplerID);
void loadMixParam(GLfloat value);
void loadModelMatrix(glm::mat4 transformationMatrix);
void loadViewMatrix(glm::mat4 transformationMatrix);
void loadProjectionMatrix(glm::mat4 transformationMatrix);
void loadObjectColor(glm::vec3 objectColor);
void loadViewerPosition(glm::vec3 viewerPos);
void loadMaterialProperties(struct Material material);
void loadLightProperties(std::vector<struct Light> lights, struct Camera* camera);
void loadSkyboxSampler(GLuint samplerID);
#endif // !H_SHADERPROGRAM_ENTITY
