// headers
#include"SkyboxRenderer.h"

// global variables
extern GLuint gShaderProgramSkybox;
struct Entity gCubeMapEntity;

void prepareSkybox(glm::mat4 projectionMatrix)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("skybox.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("skybox.fs");

	// code
	// create shader program object
	gShaderProgramSkybox = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("Skybox Shader program object created.");

	// Load uniforms
	getAllUniformLocationsSkybox(); // get the uniform locations
	logStaticData("Loaded Skybox uniform locations.");

	// load uniform data
	startProgram(gShaderProgramSkybox);
	loadTextureSamplerSkybox(0);
	loadProjectionMatrixSkybox(projectionMatrix);
	stopProgram();

}


void renderSkybox()
{
	glDepthMask(GL_FALSE);
	glBindVertexArray(gCubeMapEntity.texturedModel.rawModel.vaoID);
		bindTexturesSkybox(gCubeMapEntity.texturedModel);
		// draw entity
		draw(gCubeMapEntity.texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
		glDepthMask(GL_TRUE);
}