// headers
#include"GeneralRenderer.h"

// global variables
extern GLuint gShaderProgramGeneral;
std::vector<struct Entity> gGeneralEntities;

void prepareGeneralRenderer(glm::mat4 projectionMatrix)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("general.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("general.fs");

	// code
	// create shader program object
	gShaderProgramGeneral = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("General Shader program object created.");

	// Load uniforms
	getAllUniformLocationsGeneral(); // get the uniform locations
	logStaticData("Loaded General uniform locations.");

	startProgram(gShaderProgramGeneral);
	loadProjectionMatrixGeneral(projectionMatrix);
	loadGeneralTextureSampler(0);
	stopProgram();
}

// add entity to the entity list for rendering
void processGeneral(struct Entity entity)
{
	gGeneralEntities.push_back(entity);
}

// render lights
void renderGeneral(struct Camera *camera)
{
	loadViewMatrixGeneral(createViewMatrix(*camera));
	for (int i = 0; i < gGeneralEntities.size(); i++)
	{
		glm::mat4 transformationMatrix = glm::mat4(1.0);
		// load uniforms
		loadModelMatrixGeneral(getTransformationMatrix(transformationMatrix, gGeneralEntities[i]));
		bindTexturesGeneral(gGeneralEntities[i].texturedModel);
		// draw entity
		draw(gGeneralEntities[i].texturedModel.rawModel.vertexCount, true);
	}
}

// clean up the shader program object
void cleanUpGeneral()
{
	gGeneralEntities.clear();
}



void renderGeneralUsingFBO(struct Camera* camera, struct FBO *fbo)
{
	loadViewMatrixGeneral(createViewMatrix(*camera));
	for (int i = 0; i < gGeneralEntities.size(); i++)
	{
		glm::mat4 transformationMatrix = glm::mat4(1.0);
		gGeneralEntities[i].texturedModel.textureID = fbo->colorAttachment;
		// load uniforms
		loadModelMatrixGeneral(getTransformationMatrix(transformationMatrix, gGeneralEntities[i]));
		bindTexturesGeneral(gGeneralEntities[i].texturedModel);
		// draw entity
		draw(gGeneralEntities[i].texturedModel.rawModel.vertexCount, true);
	}
}