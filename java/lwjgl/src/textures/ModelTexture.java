package textures;

/**
 * @author rohit
 * Date: 21/05/2019
 * 
 * This class represents the texture that we will use in our game
 */
public class ModelTexture
{
	private int textureID;
	private int normalMap;
	private int specularMap;
	private float shineDamper = 1.0f;
	private float reflectivity = 0.0f;

	// for objects like grass, fern
	private boolean hasTransparency = false;
	private boolean hasFakeLighting = false;
	
	// for texture atlas
	private int numberOfRows = 1;
	
	public ModelTexture(int textureID)
	{
		super();
		this.textureID = textureID;
	}

	// getters/setters
	
	public boolean isHasTransparency()
	{
		return hasTransparency;
	}


	public int getNormalMap()
	{
		return normalMap;
	}

	public void setNormalMap(int normalMap)
	{
		this.normalMap = normalMap;
	}

	public int getSpecularMap()
	{
		return specularMap;
	}

	public void setSpecularMap(int specularMap)
	{
		this.specularMap = specularMap;
	}

	public int getNumberOfRows()
	{
		return numberOfRows;
	}

	public void setNumberOfRows(int numberOfRows)
	{
		this.numberOfRows = numberOfRows;
	}

	public boolean isHasFakeLighting()
	{
		return hasFakeLighting;
	}

	public void setHasFakeLighting(boolean hasFakeLighting)
	{
		this.hasFakeLighting = hasFakeLighting;
	}

	public void setHasTransparency(boolean hasTransparency)
	{
		this.hasTransparency = hasTransparency;
	}


	public int getTextureID()
	{
		return textureID;
	}

	public void setTextureID(int textureID)
	{
		this.textureID = textureID;
	}

	public float getShineDamper()
	{
		return shineDamper;
	}

	public void setShineDamper(float shineDamper)
	{
		this.shineDamper = shineDamper;
	}

	public float getReflectivity()
	{
		return reflectivity;
	}

	public void setReflectivity(float reflectivity)
	{
		this.reflectivity = reflectivity;
	}
	
	
}
