#version 330 core

in vec3 out_tex_coords;

out vec4 FragColor;

uniform samplerCube u_texture_sampler;

void main()
{
	FragColor = texture(u_texture_sampler, out_tex_coords);
}