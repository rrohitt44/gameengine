#ifndef H_ENTITY_MAPS_RENDERER
#define H_ENTITY_MAPS_RENDERER

// headers
#include<iostream>
#include<vector>
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"EntityMapsShaderProgram.h"
#include"Camera.h"
#include"Loader.h"
#include"OpenGLUtils.h"
#include"Mesh.h"
#include"Player.h"

extern std::vector<EntityMap> entityMaps;
extern std::vector<EntityMap> objEntityMaps;
extern std::vector<Mesh> gLoadedModels;;
extern GLuint gShaderProgramEntityMaps;
void prepareEntityMapsRenderer(glm::mat4 projectionMatrix);
void processEntityMaps(struct EntityMap entity); // add entity to the entity list for rendering
void processObjModels(struct EntityMap models);
void processLoadedModels(std::vector<Mesh> models);
void renderLoadedModels();
void renderEntityMaps(); // render entities
void renderObjEntityMaps(std::vector<struct Light> lights, struct Camera* camera, glm::mat4 projectionMatrix);
void cleanUpEntityMaps(); // clean up the shader program object
void increateEntityMapsRotation(float rotX, float rotY, float rotZ);
#endif // !H_ENTITY_MAPS_RENDERER
