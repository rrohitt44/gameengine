package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

/**
 * @author rohit
 * Date: 21/05/2019
 * 
 * Camera in our game
 */
public class Camera
{
	// camera variables
	private Vector3f position= new Vector3f(0,3,0);//-100, 3, -55);
	private float pitch=20; // rotation around x, y and z axis or how high or low the camera is held
	private float yaw; // how left or right the camera is aiming
	private float roll; // how much the camera is tilted to one side
	
	private Player player;
	
	// camera-player variables
	private float distanceFromPlayer = 50;
	private float angleAroundPlayer = 0;
	
	public Camera(Player player)
	{
		super();
		this.player = player;
	}
	public Vector3f getPosition()
	{
		return position;
	}
	public void setPosition(Vector3f position)
	{
		this.position = position;
	}
	public float getPitch()
	{
		return pitch;
	}
	public void setPitch(float pitch)
	{
		this.pitch = pitch;
	}
	public float getYaw()
	{
		return yaw;
	}
	public void setYaw(float yaw)
	{
		this.yaw = yaw;
	}
	public float getRoll()
	{
		return roll;
	}
	public void setRoll(float roll)
	{
		this.roll = roll;
	}

	/**
	 * To move the camera every frame
	 */
	public void move()
	{
		calculateZoom(); // distance between camera and player
		calculatePitch(); // height between camera and player
		calculateAngleAroundThePlayer(); // angle around the player
		
		// calculate horizontal and vertical distances of the camera from the player
		float horizontalDist = calculateHorizontalDistance();
		float verticalDist = calculateVerticalDistance();
		calculateCameraPosition(horizontalDist, verticalDist);
		
		// calculate yaw
		this.yaw = 180 - (player.getRotY() + angleAroundPlayer);
	}
	
	/**
	 * Calculates the actual position of the camera
	 */
	private void calculateCameraPosition(float horizontal, float vertical)
	{
		float theta = player.getRotY() + angleAroundPlayer;
		float offsetX = (float) (horizontal * Math.sin(Math.toRadians(theta)));
		float offsetZ = (float) (horizontal * Math.cos(Math.toRadians(theta)));
		position.x = player.getPosition().x - offsetX;
		position.z = player.getPosition().z - offsetZ;
		position.y = player.getPosition().y + vertical;
	}
	
	/**
	 * @return
	 * Calculates the horizontal distance of the camera from the player as per the pitch and distance between them
	 */
	private float calculateHorizontalDistance()
	{
		return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
	}
	
	/**
	 * @return
	 * Calculates the vertical distance of the camera from the player as per the pitch and distance between them
	 */
	private float calculateVerticalDistance()
	{
		return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
	}
	
	/**
	 * adjust the distance between camera and player as per the mouse wheel movement
	 */
	private void calculateZoom()
	{
		float zoomLevel = Mouse.getDWheel() * 0.1f; // middle button
		distanceFromPlayer -= zoomLevel;
		if(distanceFromPlayer<5)
		{
			distanceFromPlayer = 5f;
		}
	}
	
	/**
	 * adjust the height between camera and player as per the mouse right key button
	 * 0=left mouse button and 1=right mouse button
	 */
	private void calculatePitch()
	{
		if(Mouse.isButtonDown(1)) // right
		{
			// check how much mouse has gone up or down
			float pitchChange = Mouse.getDY() * 0.1f;
			pitch -= pitchChange;
			
			if(pitch<5)
			{
				pitch=5;
			}else if(pitch>170)
			{
				pitch=170;
			}
		}
	}
	
	/**
	 * calculates how much the camera should move around the player
	 */
	private void calculateAngleAroundThePlayer()
	{
		if(Mouse.isButtonDown(0)) // left
		{
			float angleChange = Mouse.getDX() * 0.3f;
			angleAroundPlayer -= angleChange;
		}
	}
	
	/**
	 * Inverts the camera pitch
	 */
	public void invertPitch()
	{
		pitch = -pitch;
	}
}
