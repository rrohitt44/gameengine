package models;

import textures.ModelTexture;

/**
 * @author rohit
 * Date: 21/05/2019
 * 
 * This class represents a textured model
 */
public class TexturedModel
{
	private RawModel rawModel;
	private ModelTexture modelTexture;
	
	public TexturedModel(RawModel rawModel, ModelTexture modelTexture)
	{
		super();
		this.rawModel = rawModel;
		this.modelTexture = modelTexture;
	}
	public RawModel getRawModel()
	{
		return rawModel;
	}
	public void setRawModel(RawModel rawModel)
	{
		this.rawModel = rawModel;
	}
	public ModelTexture getModelTexture()
	{
		return modelTexture;
	}
	public void setModelTexture(ModelTexture modelTexture)
	{
		this.modelTexture = modelTexture;
	}
	
}
