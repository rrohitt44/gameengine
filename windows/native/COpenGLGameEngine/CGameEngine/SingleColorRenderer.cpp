// headers
#include"SingleColorRenderer.h"

// global variables
extern GLuint gShaderProgramSingleColor;

void prepareSingleColorRenderer(glm::mat4 projectionMatrix)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("general.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("object_outline.fs");

	// code
	// create shader program object
	gShaderProgramSingleColor = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("Object Outliner Shader program object created.");

	// Load uniforms
	getAllUniformLocationsSingleColor(); // get the uniform locations
	logStaticData("Loaded Object Outliner uniform locations.");

	startProgram(gShaderProgramSingleColor);
	loadProjectionMatrixSingleColor(projectionMatrix);
	loadSingleColorTextureSampler(0);
	stopProgram();
}

// render lights
void renderSingleColor(struct Camera *camera, std::vector<struct EntityMap> generalEntities)
{
	loadViewMatrixSingleColor(createViewMatrix(*camera));
	for (int i = 0; i < generalEntities.size(); i++)
	{
		if (generalEntities[i].isBordered)
		{
			glm::mat4 transformationMatrix = glm::mat4(1.0);
			generalEntities[i].scale = 1.1;
			// load uniforms
			loadModelMatrixSingleColor(getTransformationMatrixEntityMaps(transformationMatrix, generalEntities[i]));
			//bindTexturesSingleColor(generalEntities[i].texturedModel);
			// draw entity
			draw(generalEntities[i].texturedModel.rawModel.vertexCount, true);
		}
	}
}
