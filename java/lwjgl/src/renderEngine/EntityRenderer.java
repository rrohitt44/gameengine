package renderEngine;

import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import entities.Entity;
import models.RawModel;
import models.TexturedModel;
import shaders.StaticShader;
import textures.ModelTexture;
import toolbox.Maths;

/**
 * @author rohit
 * Date: 20/05/2019
 */
public class EntityRenderer
{
	private StaticShader shader;
	
	public EntityRenderer(StaticShader shader,Matrix4f projectionMatrix)
	{
		this.shader = shader;
		
		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
	}
	
	/**
	 * @param entities
	 * Render the entities
	 */
	public void render(Map<TexturedModel, List<Entity>> entities)
	{
		for(TexturedModel model: entities.keySet())
		{
			prepareTexturedModel(model); // prepare textured model
			List<Entity> batch = entities.get(model);
			
			for(Entity entity: batch)
			{
				prepareInstance(entity);
				//render here
				/*GL11.glDrawArrays(
				GL11.GL_TRIANGLES, // draw triangles
				0, // with 0 as offset to data
				model.getVertexCount() // total number of vertices to draw
				);*/
		
				GL11.glDrawElements(
						GL11.GL_TRIANGLES, // draw triangles
						model.getRawModel().getVertexCount(), // indices count
						GL11.GL_UNSIGNED_INT, // type of data
						0 // indices buffer offset
						);
			}
			unbindTexturedModel();
		}
	}
	
	/**
	 * @param model
	 * prepare the textured model like enabling the attributes starting the shader, etc...
	 */
	public void prepareTexturedModel(TexturedModel texturedModel)
	{
		RawModel rawModel = texturedModel.getRawModel();
		// bind the VAO for rendering
		GL30.glBindVertexArray(rawModel.getVaoID());
		GL20.glEnableVertexAttribArray(0); // enable the attribute in the shader
		GL20.glEnableVertexAttribArray(1); // enable the attribute in the shader
		GL20.glEnableVertexAttribArray(2); // enable the attribute in the shader
		
		ModelTexture texture = texturedModel.getModelTexture();
		shader.loadNumberOfRows(texture.getNumberOfRows());
		// check if transparency for the model is enabled
		if(texture.isHasTransparency())
		{
			MasterRenderer.disableCulling();
		}
		shader.setUseFakeLighting(texture.isHasFakeLighting());
		shader.loadShineVariables(texture.getShineDamper(), texture.getReflectivity());
		
		// tell OpenGL that which texture you want to render over the quad
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturedModel.getModelTexture().getTextureID());
	}
	
	/**
	 * Unbind the textured model
	 */
	private void unbindTexturedModel()
	{
		MasterRenderer.enableCulling();
		GL20.glDisableVertexAttribArray(0); // disable the attribute in the shader
		GL20.glDisableVertexAttribArray(1); // disable the attribute in the shader
		GL20.glDisableVertexAttribArray(2); // disable the attribute in the shader
		GL30.glBindVertexArray(0); // unbind the VAO
	}
	
	/**
	 * @param entity
	 * prepare the entity for the transformation models
	 */
	private void prepareInstance(Entity entity)
	{
		// set the uniforms to the shaders
		Matrix4f transformationMatrix = Maths.createTransformationMatrix(entity.getPosition(), entity.getRotX(), 
									entity.getRotY(), entity.getRotZ(), entity.getScale());
		shader.loadTransformationMatrix(transformationMatrix);
		shader.loadTextureAtlasOffsets(entity.getTextureXOffset(), entity.getTextureYOffset());
	}
	
	
}
