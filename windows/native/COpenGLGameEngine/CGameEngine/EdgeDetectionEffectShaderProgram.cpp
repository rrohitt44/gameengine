#include"EdgeDetectionEffectShaderProgram.h"

using namespace std;

GLuint gShaderProgramEdgeDetectionEffect;
static GLuint gLocTextureSamplerEdgeDetectionEffect;

// gets all uniform locations
void getAllUniformLocationsEdgeDetectionEffect()
{
	gLocTextureSamplerEdgeDetectionEffect = glGetUniformLocation(gShaderProgramEdgeDetectionEffect, "u_texture_sampler");
}

void loadTextureSamplerEdgeDetectionEffect(GLuint value)
{
	setInt(gLocTextureSamplerEdgeDetectionEffect, value);
}

void bindTexturesEdgeDetectionEffect(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}