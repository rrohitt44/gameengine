#ifndef H_SINGLE_COLOR_RENDERER
#define H_SINGLE_COLOR_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"SingleColorShaderProgram.h"
#include"Camera.h"

void prepareSingleColorRenderer(glm::mat4 projectionMatrix);
void renderSingleColor(struct Camera* camera, std::vector<struct EntityMap> generalEntities); // render entities
#endif // !H_SINGLE_COLOR_RENDERER
