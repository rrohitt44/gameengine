#ifndef H_OBJ_LOADER
#define H_OBJ_LOADER

#include<iostream>
#include<string>
#include"Models.h"
#include"Loader.h"

// function prototype declarations
void loadObjFile(std::string objFileName, RawModel* model);
#endif // !H_OBJ_LOADER
