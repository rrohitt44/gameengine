#version 330 core
layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec3 out_normal;
out vec2 out_texcoord;
out vec3 out_frag_pos;
out float out_visibility;

uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;
uniform float u_useFakeLighting;

const float density = 0.007;
const float gradient = 1.5;

void main()
{
	vec4 worldPos = u_model_matrix * vec4(aPosition.x, aPosition.y, aPosition.z, 1.0);
	vec4 positionRelativeToCamera = u_view_matrix * worldPos; // for fog calculations
    gl_Position =   u_projection_matrix * positionRelativeToCamera;

	vec3 actualNormal = aNormal;
	if(u_useFakeLighting > 0.5)
	{
		actualNormal = vec3(0,1,0);
	}
    out_normal = mat3(transpose(inverse(u_model_matrix))) * actualNormal; // will work for non-uniform scaling also
	out_frag_pos = worldPos.xyz;
	out_texcoord = aTexCoords;

	// fog calculations
	float distance = length(positionRelativeToCamera.xyz);
	out_visibility = exp(-pow((distance * density), gradient));
	out_visibility = clamp(out_visibility, 0.0, 1.0);
}