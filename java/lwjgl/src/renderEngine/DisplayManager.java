package renderEngine;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

/**
 * @author rohit
 * Date - 20/05/2019
 * 
 * Manages the display which we will use for rendering our game
 */
public class DisplayManager
{
	private static final int WIDTH = 1280; // width of the display
	private static final int HEIGHT = 720; // height of the display
	private static final int FPS_CAP = 120; // frame rate
	private static boolean isFullscreen = false;
	
	// for player movement
	private static long lastFrameTime; // last frame time
	private static float delta; // time between two frames
	/**
	 * Creates the display for rendering
	 */
	public static void createDisplay()
	{
		// local variables
		PixelFormat pixelFormat = new PixelFormat();
		ContextAttribs contextAttribs = new ContextAttribs(3,2)
				.withForwardCompatible(true).withProfileCore(true);
		
		// code
		// creates the display mode
		DisplayMode displayMode = new DisplayMode(WIDTH, HEIGHT);
		try
		{
			// set the display mode
			Display.setDisplayMode(displayMode);
			
			// create the display now
			Display.create(pixelFormat, contextAttribs);
		} catch (LWJGLException e)
		{
			e.printStackTrace();
		}
		
		// set the OpenGL viewport here
		GL11.glViewport(0, 0, WIDTH, HEIGHT);
		
		// initialize last frame time
		lastFrameTime = getCurrentTime();
	}
	
	/**
	 * Update the display each frame
	 */
	public static void updateDisplay()
	{
		Display.sync(FPS_CAP);
		Display.update();
		Display.setTitle("GLEngine");
		long currentFrameTime = getCurrentTime();
		// calculate how long the last frame took to render
		delta = currentFrameTime - lastFrameTime;
		delta = delta/1000; // convert to seconds
		lastFrameTime = currentFrameTime;
	}
	
	/**
	 * Return how much seconds last frame took to render
	 */
	public static float getFrameTimeSeconds()
	{
		return delta;
	}
	
	/**
	 * Closes the display once we are done with the game
	 */
	public static void closeDisplay()
	{
		Display.destroy();
	}
	
	public static void handleKeys()
	{
		// local variables
		int width = WIDTH;
		int height = HEIGHT;
		
		// code
		// close the display if escape key is pressed
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
		{
			System.exit(0); // exiting from the game
		}else if(Keyboard.isKeyDown(Keyboard.KEY_F))
		{
			if(isFullscreen == true)
			{
				width = Display.getDesktopDisplayMode().getWidth();
				height = Display.getDesktopDisplayMode().getHeight();
				
				// make the window fullscreen
				try
				{
					setDisplayMode(
							width, 
							height, 
							isFullscreen);
					Display.setFullscreen(true);
				} catch (LWJGLException e)
				{
					e.printStackTrace();
				}
				
				// set fullscreen status to false now
				isFullscreen = false;
			}
			else
			{
				width = WIDTH;
				height = HEIGHT;
				
				// exit full screen
				try
				{
					setDisplayMode(WIDTH, HEIGHT, isFullscreen);
					Display.setFullscreen(false);
				} catch (LWJGLException e)
				{
					e.printStackTrace();
				}
				
				// set fullscreen status to true now
				isFullscreen = true;
			}
			// set the OpenGL viewport accordingly
			GL11.glViewport(0, 0, width, height);
		}
	}
	
	/**
	 * Set the display mode to be used 
	 * 
	 * @param width The width of the display required
	 * @param height The height of the display required
	 * @param fullscreen True if we want fullscreen mode
	 */
	public static void setDisplayMode(int width, int height, boolean fullscreen)
	{

		// return if requested DisplayMode is already set
		if ((Display.getDisplayMode().getWidth() == width) && (Display.getDisplayMode().getHeight() == height)
				&& (Display.isFullscreen() == fullscreen))
		{
			return;
		}

		try
		{
			DisplayMode targetDisplayMode = null;

			if (fullscreen)
			{
				DisplayMode[] modes = Display.getAvailableDisplayModes();
				int freq = 0;
				/*System.out.println("RSM: Desktop supported screen resolution :"+Display.getDesktopDisplayMode().getWidth()+
						"x"+Display.getDesktopDisplayMode().getHeight()+
						"x"+Display.getDesktopDisplayMode().getFrequency());*/
				
				for (int i = 0; i < modes.length; i++)
				{
					DisplayMode current = modes[i];
					
					// print the resolutions supported
					/*System.out.println("RSM: Fullscreen supported resolution "+i+": "+current.getWidth() + "x" + current.getHeight() + "x" +
	                        current.getBitsPerPixel() + " " + current.getFrequency() + "Hz");
*/
					if ((current.getWidth() == width) && (current.getHeight() == height))
					{
						if ((targetDisplayMode == null) || (current.getFrequency() >= freq))
						{
							if ((targetDisplayMode == null)
									|| (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel()))
							{
								targetDisplayMode = current;
								freq = targetDisplayMode.getFrequency();
							}
						}

						// if we've found a match for bpp and frequence against
						// the
						// original display mode then it's probably best to go
						// for this one
						// since it's most likely compatible with the monitor
						if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel())
								&& (current.getFrequency() == Display.getDesktopDisplayMode().getFrequency()))
						{
							targetDisplayMode = current;
							break;
						}
					}
				}
			} else
			{
				targetDisplayMode = new DisplayMode(width, height);
			}

			if (targetDisplayMode == null)
			{
				System.out.println("Failed to find value mode: " + width + "x" + height + " fs=" + fullscreen);
				return;
			}

			Display.setDisplayMode(targetDisplayMode);
			Display.setFullscreen(fullscreen);

		} catch (LWJGLException e)
		{
			System.out.println("Unable to setup mode " + width + "x" + height + " fullscreen=" + fullscreen + e);
		}
	}
	
	/**
	 * @return
	 * Returns the current time in milliseconds
	 */
	private static long getCurrentTime()
	{
		long currentTimeInTicks = Sys.getTime(); // current time in ticks in milliseconds
		long numberOfTicksPerSecond = Sys.getTimerResolution(); // number of ticks per seconds
		long timeInMilliSec = currentTimeInTicks * 1000 / numberOfTicksPerSecond; // multiply by 1000 to convert to millisec
		return timeInMilliSec;
	}
}
