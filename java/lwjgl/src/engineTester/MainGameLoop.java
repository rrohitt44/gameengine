package engineTester;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.openal.AL10;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import audio.AudioMaster;
import audio.Source;
import entities.Camera;
import entities.Entity;
import entities.Light;
import entities.Player;
import guis.GuiRenderer;
import guis.GuiTexture;
import models.RawModel;
import models.TexturedModel;
import normalMappingObjConverter.NormalMappedObjLoader;
import objConverter.OBJFileLoader;
import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;
import renderEngine.ObjLoader;
import terrains.Terrain;
import terrains.TerrainTexture;
import terrains.TerrainTexturePack;
import textures.ModelTexture;
import toolbox.MousePicker;
import water.WaterFrameBuffers;
import water.WaterRenderer;
import water.WaterShader;
import water.WaterTile;

/**
 * @author rohit
 * Date: 20/05/2019
 */
public class MainGameLoop
{
	public static Light sun = new Light(new Vector3f(1000000, 1000000, -1000000), new Vector3f(1.3f, 1.3f, 1.3f)); // we don't like the sun light to be attenuated so not set the attenuation for sun
	
	public static void main(String[] args)
	{
		// local variables
		Loader loader = new Loader();
		List<Entity> normalMappedEntities = new ArrayList<>();
		
		// code
		// create display
		DisplayManager.createDisplay();
		
		// Terrains
		TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grassy"));
		TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("dirt"));
		TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("pinkFlowers"));
		TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));
		TerrainTexturePack texturePack =new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
		TerrainTexture blendMapTexture = new TerrainTexture(loader.loadTexture("blendMap"));
		
		Terrain terrain1 = new Terrain(0, 0, loader,texturePack, blendMapTexture,  "heightmap");
		//terrain1.setHasMultipleLights(true);
		Terrain terrain2 = new Terrain(1, 0, loader,texturePack, blendMapTexture,  "heightmap");
		terrain2.setHasMultipleLights(true);
		Terrain terrain3 = new Terrain(1, 1, loader,texturePack, blendMapTexture,  "heightmap");
		//terrain3.setHasMultipleLights(true);
		Terrain planeTerrain = new Terrain(0, 1, loader,texturePack, blendMapTexture,  null); // terrain without height
		//planeTerrain.setHasMultipleLights(true);
		
		
		// load the entity vertices
		// tree model and texture
		ModelTexture cherryModelTexture = new ModelTexture(loader.loadTexture("cherry"));
		cherryModelTexture.setHasTransparency(true);
		cherryModelTexture.setShineDamper(10);
		cherryModelTexture.setReflectivity(1);
		TexturedModel cherryTexturedModel = new TexturedModel(ObjLoader.loadObjModel("cherry", loader),
				cherryModelTexture);
		
		RawModel treeModel = ObjLoader.loadObjModel("tree", loader);
		ModelTexture treeTexture = new ModelTexture(loader.loadTexture("tree"));
		treeTexture.setShineDamper(10);
		treeTexture.setReflectivity(1);
		TexturedModel treeTexturedModel = new TexturedModel(treeModel, treeTexture);
		
		// grass model and texture
		RawModel grassModel = ObjLoader.loadObjModel("grassModel", loader);
		ModelTexture grassTexture = new ModelTexture(loader.loadTexture("grassTexture"));
		grassTexture.setHasTransparency(true);
		grassTexture.setHasFakeLighting(true);
		TexturedModel grassTexturedModel = new TexturedModel(grassModel, grassTexture);
		
		// fern model and texture
		RawModel fernModel = ObjLoader.loadObjModel("fern", loader);
		ModelTexture fernTexture = new ModelTexture(loader.loadTexture("fern"));
		fernTexture.setHasTransparency(true);
		fernTexture.setNumberOfRows(2);
		TexturedModel fernTexturedModel = new TexturedModel(fernModel, fernTexture);
		
		// flowers texture
		ModelTexture flowerTexture = new ModelTexture(loader.loadTexture("flower"));
		flowerTexture.setHasTransparency(true);
		TexturedModel flowerTexturedModel = new TexturedModel(grassModel, flowerTexture);
		
		// bobble - low poly models and texture
		/*RawModel lowPolyBobbleModel = ObjLoader.loadObjModel("lowPolyTree", loader);
		ModelTexture lowPolyBobbleTexture = new ModelTexture(loader.loadTexture("lowPolyTree"));
		lowPolyBobbleTexture.setHasTransparency(true);
		TexturedModel lowPolyBobbleTexturedModel = new TexturedModel(lowPolyBobbleModel, lowPolyBobbleTexture);*/
		
		// bobble - low poly models and texture
		RawModel pineModel = ObjLoader.loadObjModel("pine", loader);
		ModelTexture pineTexture = new ModelTexture(loader.loadTexture("pine"));
		pineTexture.setHasTransparency(true);
		TexturedModel pineTexturedModel = new TexturedModel(pineModel, pineTexture);
		
		// crate- model and texture
		RawModel crateModel = ObjLoader.loadObjModel("crate", loader);
		ModelTexture crateTexture = new ModelTexture(loader.loadTexture("crate"));
		TexturedModel crateTexturedModel = new TexturedModel(crateModel, crateTexture);
		
		// crate- model and texture
		RawModel lampModel = ObjLoader.loadObjModel("lamp", loader);
		ModelTexture lampTexture = new ModelTexture(loader.loadTexture("lamp"));
		lampTexture.setHasFakeLighting(true);
		TexturedModel lampTexturedModel = new TexturedModel(lampModel, lampTexture);
		
		// Rocks model and textures
		TexturedModel rocks = new TexturedModel(OBJFileLoader.loadOBJ("rocks", loader),
				new ModelTexture(loader.loadTexture("rocks")));
		
		List<Entity> entityList = new ArrayList<>();
		List<Terrain> terrainList = new ArrayList<>();
		terrainList.add(terrain1);
		terrainList.add(terrain2);
		terrainList.add(terrain3);
		terrainList.add(planeTerrain);
		
		// coordinates of the entities
		Random random = new Random();
		float x = 0;
		float y = 0;
		float z = 0;
		int moduloFactor = 15;
		// generate random entities
		for(int i=0; i<500; i++)
		{
			for (Terrain terrain : terrainList)
			{
				if (i % moduloFactor == 0)
				{
					// tree
					x = (random.nextFloat() * 800 - 400) + terrain.getX();
					z = (random.nextFloat() * -600) + terrain.getZ();
					if(terrain.getHeights()!=null)
					{
						y = terrain.generateHeightOfTerrain(x, z);
					}else
					{
						y = 0;
					}
					entityList.add(new Entity(cherryTexturedModel, new Vector3f(x, y, z), // translate
							0, 0, 0, // rotate
							7 // scale
					));
				}

				if (i % moduloFactor == 0)
				{
					// grass
					x = (random.nextFloat() * 800 - 400) + terrain.getX();
					z = (random.nextFloat() * -600) + terrain.getZ();
					if(terrain.getHeights()!=null)
					{
						y = terrain.generateHeightOfTerrain(x, z);
					}else
					{
						y = 0;
					}
					entityList.add(new Entity(grassTexturedModel, new Vector3f(x, y, z), // translate
							0, 0, 0, // rotate
							1 // scale
					));
				}

				if (i % moduloFactor == 0)
				{
					// fern
					x = (random.nextFloat() * 800 - 400) + terrain.getX();
					z = (random.nextFloat() * -600) + terrain.getZ();
					if(terrain.getHeights()!=null)
					{
						y = terrain.generateHeightOfTerrain(x, z);
					}else
					{
						y = 0;
					}
					entityList.add(new Entity(fernTexturedModel, random.nextInt(4), new Vector3f(x, y, z), // translate
							0, 0, 0, // rotate
							1.4f // scale
					));
				}

				if (i % moduloFactor == 0)
				{
					// flowers
					x = (random.nextFloat() * 800 - 400) + terrain.getX();
					z = (random.nextFloat() * -600) + terrain.getZ();
					if(terrain.getHeights()!=null)
					{
						y = terrain.generateHeightOfTerrain(x, z);
					}else
					{
						y = 0;
					}
					entityList.add(new Entity(flowerTexturedModel, new Vector3f(x, y, z), // translate
							0, 0, 0, // rotate
							3f // scale
					));
				}

				if (i % moduloFactor == 0)
				{
					// bobbles
					x = (random.nextFloat() * 800 - 400) + terrain.getX();
					z = (random.nextFloat() * -600) + terrain.getZ();
					if(terrain.getHeights()!=null)
					{
						y = terrain.generateHeightOfTerrain(x, z);
					}else
					{
						y = 0;
					}
					entityList.add(new Entity(pineTexturedModel, new Vector3f(x, y, z), // translate
							0, 0, 0, // rotate
							3f // scale
					));
				}
				
				// crate
				if(i%100==0)
				{
					x = (random.nextFloat() * 800 - 400) + terrain.getX();
					z = (random.nextFloat() * -600) + terrain.getZ();
					if(terrain.getHeights()!=null)
					{
						y = terrain.generateHeightOfTerrain(x, z);
					}else
					{
						y = 0;
					}
					entityList.add(new Entity(crateTexturedModel, new Vector3f(x, y, z), // translate
							0, 0, 0, // rotate
							0.1f // scale
					));
				}
			}
		}
		 
		// create the light source
		List<Light> lights = new ArrayList<>();
		lights.add(sun);
		Light light1 = new Light(new Vector3f(185, 10, -293),new Vector3f(2, 0, 0), new Vector3f(1,0.01f,0.002f));
		Light light2 = new Light(new Vector3f(370, 17, -300),new Vector3f(0, 2, 0), new Vector3f(1,0.01f,0.002f));
		Light light3 = new Light(new Vector3f(293, 7, -305),new Vector3f(2, 2, 0), new Vector3f(1,0.01f,0.002f));
		lights.add(light1);
		lights.add(light2);
		lights.add(light3);
		
		// add lamps as an entities
		Entity greenLampEntity = new Entity(lampTexturedModel, new Vector3f(370, 4.2f, -300), 0 ,0 , 0, 1);
		entityList.add(new Entity(lampTexturedModel, new Vector3f(185, -4.7f, -293), 0 ,0 , 0, 1));
		entityList.add(greenLampEntity );
		entityList.add(new Entity(lampTexturedModel, new Vector3f(293, -6.8f, -305), 0 ,0 , 0, 1));
		entityList.add(new Entity(lampTexturedModel, new Vector3f(0,0,0), 0 ,0 , 0, 1));
		
		// add rocks in the entities
		entityList.add(new Entity(rocks, new Vector3f(75, 0.0f, -75), 0, 0, 0, 75));
		
		// Normal mapped entities
		TexturedModel barrelModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("barrel", loader),
				new ModelTexture(loader.loadTexture("barrel")));
		barrelModel.getModelTexture().setNormalMap(loader.loadTexture("barrelNormal"));
		barrelModel.getModelTexture().setShineDamper(10);
		barrelModel.getModelTexture().setReflectivity(0.5f);
		
		TexturedModel crateNmModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("crate", loader),
				new ModelTexture(loader.loadTexture("crate")));
		crateNmModel.getModelTexture().setNormalMap(loader.loadTexture("crateNormal"));
		crateNmModel.getModelTexture().setShineDamper(10);
		crateNmModel.getModelTexture().setReflectivity(0.5f);
		
		TexturedModel boulderModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("boulder", loader),
				new ModelTexture(loader.loadTexture("boulder")));
		boulderModel.getModelTexture().setNormalMap(loader.loadTexture("boulderNormal"));
		boulderModel.getModelTexture().setShineDamper(10);
		boulderModel.getModelTexture().setReflectivity(0.5f);
		
		Entity entity = new Entity(barrelModel, new Vector3f(65, 10, -30), 0, 0, 0, 1f);
		Entity entity2 = new Entity(boulderModel, new Vector3f(85, 10, -30), 0, 0, 0, 1f);
		Entity entity3 = new Entity(crateNmModel, new Vector3f(45, 10, -30), 0, 0, 0, 0.04f);
		normalMappedEntities.add(entity);
		normalMappedEntities.add(entity2);
		normalMappedEntities.add(entity3);
		
		MasterRenderer renderer = new MasterRenderer(loader);
		
		// Create Player
		// standford bunny models and texture
		RawModel bunnyModel = ObjLoader.loadObjModel("person", loader);
		ModelTexture bunnyTexture = new ModelTexture(loader.loadTexture("playerTexture"));
		TexturedModel bunnyTexturedModel = new TexturedModel(bunnyModel, bunnyTexture);
		
		Player player = new Player(bunnyTexturedModel, new Vector3f(-10, 0, 0), 0, 180, 0, 1);
		entityList.add(player);
		
		// create camera
		Camera camera = new Camera(player);
		
		// GUI textures
		List<GuiTexture> guis = new ArrayList<>();
		GuiTexture socuwanGuiTexture = new GuiTexture(
				loader.loadTexture("socuwan"), // texture id
				new Vector2f(0.75f, 0.75f), // quad position 
				new Vector2f(0.25f,0.25f) // scale
				);
		guis.add(socuwanGuiTexture); // add to GUI texture list
		GuiRenderer guiRenderer = new GuiRenderer(loader);
		
		// Create mouse picker
		MousePicker mousePicket = new MousePicker(camera, renderer.getProjectionMatrix(), terrain2);
		
		// Water render set-up
		WaterFrameBuffers fbos = new WaterFrameBuffers();
		WaterShader waterShader = new WaterShader();
		WaterRenderer waterRenderer = new WaterRenderer(loader, waterShader, renderer.getProjectionMatrix(), fbos);
		List<WaterTile> waters =new ArrayList<>();
		WaterTile waterTile = new WaterTile(0, 0, 1);
		waters.add(waterTile);
		
		// Audio
		AudioMaster.init();
		AudioMaster.setListenerData(0, 0, 0);
		AL10.alDistanceModel(AL10.AL_INVERSE_DISTANCE_CLAMPED);
		int buffer = AudioMaster.loadSound("audio/ChillingMusic.wav");
		Source source = new Source();
		source.setLooping(true);
		source.play(buffer);
		
		// main render loop
		while(!Display.isCloseRequested())
		{
			// set audio location
			source.setPosition(player.getPosition().x,player.getPosition().x,player.getPosition().z);
			
			// rotate models
			entity.increaseRotation(0, 1, 0);
			entity2.increaseRotation(0, 1, 0);
			entity3.increaseRotation(0, 1, 0);
			
			// enable clipping plane
			GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
			
			camera.move(); // move the camera every frame
			
			for(Terrain terrain : terrainList) 
			{
			    if(!(terrain.getX() <= player.getPosition().x && terrain.getZ() <= player.getPosition().z
			    		&& terrain.getX() + Terrain.SIZE > player.getPosition().x
			    		&& terrain.getZ() + Terrain.SIZE > player.getPosition().z)) 
			    { 
			        player.move(terrain);
			        //mousePicket.update(); // update the mouse picker every frame
			    }
			 }
			
			/*Vector3f terrainPoint = mousePicket.getCurrentTerrainPoint();
			if(terrainPoint!=null)
			{
				greenLampEntity.setPosition(terrainPoint);
				light2.setPosition(new Vector3f(terrainPoint.x, terrainPoint.y + 15, terrainPoint.z));
			}*/
			
			// bind reflection buffer
			fbos.bindReflectionFrameBuffer();
			
			// adjust the camera position
			float distance = 2 * (camera.getPosition().y - waterTile.getHeight());
			camera.getPosition().y -= distance;
			camera.invertPitch();
			renderer.renderScene(entityList, normalMappedEntities, terrainList, lights, camera,
					new Vector4f(0, 1, 0, -waterTile.getHeight()) // clip plane
					); // render the entities now
			camera.getPosition().y += distance;
			camera.invertPitch();
			// bind refraction buffer
			fbos.bindRefractionFrameBuffer();
			renderer.renderScene(entityList, normalMappedEntities, terrainList, lights, camera,
					new Vector4f(0, -1, 0, waterTile.getHeight()) // clip plane
					); // render the entities now
			
			// render to screen
			GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
			fbos.unbindCurrentFrameBuffer();
			renderer.renderScene(entityList, normalMappedEntities, terrainList, lights, camera,
					new Vector4f(0, -1, 0, 15000) // clip plane
					); // render the entities now
			
			// render water
			waterRenderer.render(waters, camera, sun);
			
			guiRenderer.render(guis);
			
			DisplayManager.updateDisplay(); // update display each frame
			DisplayManager.handleKeys(); // handle the keys
		}
		
		// clean up activites
		fbos.cleanUp();
		waterShader.cleanUp();
		guiRenderer.cleanUp();
		renderer.cleanUp();
		loader.cleanUp();
		// close the display now
		DisplayManager.closeDisplay();
	}
}
