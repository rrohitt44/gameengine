/**
 * 
 */
package terrains;

/**
 * @author Rohit Muneshwar
 * @date May 24, 2019
 * This class contains all the textures which we are going to apply on terrain
 */
public class TerrainTexturePack
{
	private TerrainTexture backgroundTexture;
	private TerrainTexture rTexture;
	private TerrainTexture gTexture;
	private TerrainTexture bTexture;
	public TerrainTexturePack(TerrainTexture backgroundTexture, TerrainTexture rTexture, TerrainTexture gTexture,
			TerrainTexture bTexture)
	{
		super();
		this.backgroundTexture = backgroundTexture;
		this.rTexture = rTexture;
		this.gTexture = gTexture;
		this.bTexture = bTexture;
	}
	
	// getters/setters
	public TerrainTexture getBackgroundTexture()
	{
		return backgroundTexture;
	}
	public void setBackgroundTexture(TerrainTexture backgroundTexture)
	{
		this.backgroundTexture = backgroundTexture;
	}
	public TerrainTexture getrTexture()
	{
		return rTexture;
	}
	public void setrTexture(TerrainTexture rTexture)
	{
		this.rTexture = rTexture;
	}
	public TerrainTexture getgTexture()
	{
		return gTexture;
	}
	public void setgTexture(TerrainTexture gTexture)
	{
		this.gTexture = gTexture;
	}
	public TerrainTexture getbTexture()
	{
		return bTexture;
	}
	public void setbTexture(TerrainTexture bTexture)
	{
		this.bTexture = bTexture;
	}
	
	
}
