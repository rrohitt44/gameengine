#ifndef H_FILSCREENQUAD_RENDERER
#define H_FILSCREENQUAD_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"FillScreenShaderProgram.h"
#include"Camera.h"
#include"framebuffers.h"
#include"PostProcessing.h"
extern struct Entity fillScreenQuadEntity;
void processFillScreenQuadEntity(struct Entity* entity);
void prepareFillScreenQuadRenderer();
void renderFillScreenQuad(struct FBO* fbo); // render entities using FBO textures
void renderFillScreenQuadAfterPostprocessing(GLuint textureId);
#endif // !H_FILSCREENQUAD_RENDERER
