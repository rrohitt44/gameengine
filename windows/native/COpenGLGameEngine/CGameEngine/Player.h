#ifndef H_PLAYER
#define H_PLAYER

#include"DisplayManager.h"
#include"Models.h"
#include<math.h>
#include"MathUtils.h"

extern struct EntityMap player;
extern float gCurrentSpeed;
extern float gCurrentTurnSpeed;

void movePlayer();
#endif // !H_PLAYER
