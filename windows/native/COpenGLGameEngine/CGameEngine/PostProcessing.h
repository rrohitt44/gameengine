#ifndef H_POST_PROCESSING
#define H_POST_PROCESSING

// headers
#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

// for inverting the colors
#include"inversionShaderProgram.h"
#include"InversionRenderer.h"
#include"GrayScaleShaderProgram.h"
#include"GrayScaleRenderer.h"
#include"KernelEffectShaderProgram.h"
#include"KernelEffectRenderer.h"
#include"BlurEffectRenderer.h"
#include"BlurEffectShaderProgram.h"
#include"EdgeDetectionEffectShaderProgram.h"
#include"EdgeDetectionEffectRenderer.h"
#include"framebuffers.h"

// for final post processing output
#include"FillScreenRenderer.h"
#include"FillScreenShaderProgram.h"

void startPostProcessing();
void stopPostProcessing();
void preparePostProcessing();
void doPostProcessing(struct FBO* fbo);
#endif // !H_POST_PROCESSING
