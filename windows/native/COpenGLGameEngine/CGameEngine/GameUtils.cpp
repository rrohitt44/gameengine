// headers
#include"GameUtils.h"

// global variables
std::string SHADER_RESOURCE_FILE_LOC = "C:\\Users\\RohitMuneshwar\\Documents\\IBM\\RTR\\projects\\learn_opengl\\OpenGL_On_Windows\\native\\1.GettingStarted\\CGameEngine\\CGameEngine\\res\\shaders\\";
std::string IMAGES_RESOURCE_FILE_LOC = "C:\\Users\\RohitMuneshwar\\Documents\\IBM\\RTR\\projects\\learn_opengl\\OpenGL_On_Windows\\native\\1.GettingStarted\\CGameEngine\\CGameEngine\\res\\images\\";
std::string MODELS_RESOURCE_FILE_LOC= "C:\\Users\\RohitMuneshwar\\Documents\\IBM\\RTR\\projects\\learn_opengl\\OpenGL_On_Windows\\native\\1.GettingStarted\\CGameEngine\\CGameEngine\\res\\models\\";
const GLuint NUMBER_OF_LIGHTS_USED=6;
const GLuint NUMBER_OF_MATERIALS_USED=1;
long openGLInitializationTime = 0.0f; // time at which OpenGL is initialized
FILE* gLogfile = NULL; // log file
float RED = 0.5f;
float GREEN = 0.5f;
float BLUE = 0.5f;

// for clean up activities
std::vector<GLuint> gShaderList;
std::vector<GLuint> gShaderProgramObjectList;
std::vector<GLuint> gVBOList;
std::vector<GLuint> gVAOList;
std::vector<GLuint> gTexturesList;
std::vector<GLuint> gFBOList;
std::vector<GLuint> gRBOList;

/*
get current system time: std::chrono::system_clock::now()
get time since epoch: .time_since_epoch()
translate the underlying unit to milliseconds: duration_cast<milliseconds>(d)
translate std::chrono::milliseconds to integer (uint64_t to avoid overflow)
*/
uint64_t timeSinceEpochMillisec() 
{
	using namespace std::chrono;
	return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

/*
	Reads the shader file and returns the contents of it in a array of null-terminated sequence of characters
*/
std::string readShader(const char* fileName)
{
	// local variables
	std::string shaderCode;
	std::ifstream shaderFile;
	std::stringstream shaderStream;

	// ensure ifstream objects can throw exceptions
	shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try
	{
		shaderFile.open(fileName);
		// read file's buffers contents into the stream
		shaderStream << shaderFile.rdbuf();

		// close file handler
		shaderFile.close();

		// convert stream to string
		shaderCode = shaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
	}

	return shaderCode;
}


void draw(GLuint vertexCount, bool isIndexed)
{
	if (isIndexed) // if indexed drawing requested
	{
		glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0);
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, vertexCount);
	}
}

long getTimeInSecondsSinceOpenGLIsInitialized()
{
	return (timeSinceEpochMillisec() - openGLInitializationTime);
}

float getHeightAtPixel(int x, int y, unsigned char* imageData, int nrChannels, int imageHeight)
{
	const float MAX_HEIGHT = 40; // min height -40
	const float MAX_PIXEL_COLOR = 256 * 256 * 256;
	unsigned bytePerPixel = nrChannels;
	unsigned char* pixelOffset = imageData + (x + imageHeight * y) * bytePerPixel;
	unsigned char r = pixelOffset[0];
	unsigned char g = pixelOffset[1];
	unsigned char b = pixelOffset[2];
	float height = r * g * b;
	height += MAX_PIXEL_COLOR / 2.0f;
	height /= MAX_PIXEL_COLOR / 2.0f;
	height *= MAX_HEIGHT;
	
	// convert to range from 0 to MAX_HEIGHT
	float oldRange = MAX_HEIGHT +MAX_HEIGHT;
	float newRange = MAX_HEIGHT - 0;
	height = (((height +-MAX_HEIGHT) * newRange) / oldRange) - MAX_HEIGHT/8;

	return height;
}

glm::vec3 calculateNormal(int x, int z, unsigned char* imageData, int nrChannels, int imageHeight)
{
	float heightL = getHeightAtPixel(x-1, z, imageData, nrChannels, imageHeight);
	float heightR = getHeightAtPixel(x + 1, z, imageData, nrChannels, imageHeight);
	float heightD = getHeightAtPixel(x, z-1, imageData, nrChannels, imageHeight);
	float heightU = getHeightAtPixel(x, z+1, imageData, nrChannels, imageHeight);
	glm::vec3 normal(heightL-heightR, 2.0f, heightD-heightU);
	return glm::normalize(normal);
}