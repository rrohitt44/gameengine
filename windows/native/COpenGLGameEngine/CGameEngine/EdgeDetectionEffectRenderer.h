#ifndef H_EDGE_DETECTION_EFFECT_RENDERER
#define H_EDGE_DETECTION_EFFECT_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"EdgeDetectionEffectShaderProgram.h"
#include"Camera.h"
#include"framebuffers.h"
#include"PostProcessing.h"

void prepareEdgeDetectionEffect(struct FBO* fbo);
void renderEdgeDetectionEffect(struct FBO* inputFbo, struct FBO* outputFbo); // render entities using FBO textures
#endif // !H_EDGE_DETECTION_EFFECT_RENDERER
