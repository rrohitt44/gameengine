/**
 * 
 */
package guis;

import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import models.RawModel;
import renderEngine.Loader;
import toolbox.Maths;

/**
 * @author Rohit Muneshwar
 * @date May 25, 2019
 */
public class GuiRenderer
{
	private final RawModel rawModel;
	private GuiShader shader;
	/**
	 * 
	 */
	public GuiRenderer(Loader loader)
	{
		float positions[] = {
				-1,1,  // top left
				-1,-1, // bottom left
				1,1, // top right
				1, -1 // bottom right
				};
		rawModel = loader.loadToVAO(positions, 2);
		
		shader = new GuiShader();
	}
	
	/**
	 * Clean up the shader objects
	 */
	public void cleanUp()
	{
		shader.cleanUp();
	}
	
	public void render(List<GuiTexture> guiTextures)
	{
		shader.start(); // start the shader
		GL30.glBindVertexArray(rawModel.getVaoID()); // bind VAO
		GL20.glEnableVertexAttribArray(0); // enables attribute
		//enable blending
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		// draw
		for(GuiTexture guiTexture: guiTextures)
		{
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, guiTexture.getTextureID());
			Matrix4f transformationMatrix = Maths.createTransformationMatrix(guiTexture.getPosition(), guiTexture.getScale());
			shader.loadTransformationMatrix(transformationMatrix);
			GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, rawModel.getVertexCount());
		}
		GL11.glDisable(GL11.GL_BLEND); // disable blending
		GL20.glDisableVertexAttribArray(0); // disable 0th attribute
		GL30.glBindVertexArray(0); // unbind VAO
		shader.stop(); // stop the shader
	}
}
