package models;

/**
 * @author rohit
 * Date: 20/05/2019
 * 
 * This class represents a raw model i.e. a model without texture
 */
public class RawModel
{
	private int vaoID; // vao id
	private int vertexCount; // vertex count
	public RawModel(int vaoID, int vertexCount)
	{
		super();
		this.vaoID = vaoID;
		this.vertexCount = vertexCount;
	}
	
	// getters setters
	public int getVaoID()
	{
		return vaoID;
	}
	public void setVaoID(int vaoID)
	{
		this.vaoID = vaoID;
	}
	public int getVertexCount()
	{
		return vertexCount;
	}
	public void setVertexCount(int vertexCount)
	{
		this.vertexCount = vertexCount;
	}
	
	
}
