#include"EntityShaderProgram.h"

using namespace std;

GLuint gShaderProgramEntity;
GLuint gLocColor;
GLuint gLocPositionOffset;
GLuint gLocBricksSampler;
GLuint gLocFaceSampler;
GLuint gLocMixParam;
GLuint gLocSkyboxSampler;
GLuint gLocModelMatrix;
GLuint gLocViewMatrix;
GLuint gLocProjectionMatrix;
GLuint gLocObjectColor;
GLuint gLocLightAmbient[6];
GLuint gLocLightDiffuse[6];
GLuint gLocLightSpecular[6];
GLuint gLocLightPosition[6];
GLuint gLocLightType[6];
GLuint gLocLightCutoff[6];
GLuint gLocLightOuterCutoff[6];
GLuint gLocLightDirection[6]; // light direction for directional light
GLuint gLocLightAttenuationLinera[6];
GLuint gLocLightAttenuationQuadratic[6];
GLuint gLocLightAttenuationConstant[6];
GLuint gLocViewerPosition;
GLuint gLocMaterialAmbient;
GLuint gLocMaterialDiffuse;
GLuint gLocMaterialSpecular;
GLuint gLocMaterialShininess;

// gets all uniform locations
void getAllUniformLocations()
{
	gLocColor = glGetUniformLocation(gShaderProgramEntity, "u_vertex_color");
	gLocPositionOffset = glGetUniformLocation(gShaderProgramEntity, "u_position_offset");
	gLocBricksSampler = glGetUniformLocation(gShaderProgramEntity, "u_bricks_sampler");
	gLocFaceSampler = glGetUniformLocation(gShaderProgramEntity, "u_face_sampler");
	gLocMixParam = glGetUniformLocation(gShaderProgramEntity, "u_mix_texture_param");
	gLocSkyboxSampler = glGetUniformLocation(gShaderProgramEntity, "u_skybox_sampler");
	gLocModelMatrix = glGetUniformLocation(gShaderProgramEntity, "u_model_matrix");
	gLocViewMatrix = glGetUniformLocation(gShaderProgramEntity, "u_view_matrix");
	gLocProjectionMatrix = glGetUniformLocation(gShaderProgramEntity, "u_projection_matrix");
	gLocObjectColor = glGetUniformLocation(gShaderProgramEntity, "u_object_color");

	for (int i = 0; i < NUMBER_OF_LIGHTS_USED; i++)
	{
		std::string result = "light[" + std::to_string(i) + "].ambient";
		gLocLightAmbient[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].diffuse";
		gLocLightDiffuse[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].specular";
		gLocLightSpecular[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].position";
		gLocLightPosition[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].lightType";
		gLocLightType[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].direction";
		gLocLightDirection[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].constant";
		gLocLightAttenuationConstant[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].linear";
		gLocLightAttenuationLinera[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].quadratic";
		gLocLightAttenuationQuadratic[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].cutoff";
		gLocLightCutoff[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
		result = "light[" + std::to_string(i) + "].outerCutoff";
		gLocLightOuterCutoff[i] = glGetUniformLocation(gShaderProgramEntity, result.c_str());
	}
	gLocViewerPosition = glGetUniformLocation(gShaderProgramEntity, "u_viewer_position");
	gLocMaterialAmbient = glGetUniformLocation(gShaderProgramEntity, "material.ambient");
	gLocMaterialDiffuse = glGetUniformLocation(gShaderProgramEntity, "material.diffuse");
	gLocMaterialSpecular = glGetUniformLocation(gShaderProgramEntity, "material.specular");
	gLocMaterialShininess = glGetUniformLocation(gShaderProgramEntity, "material.shininess");
}


void loadObjectColor(glm::vec3 objectColor)
{
	setVector3v(gLocObjectColor, objectColor);

}

// set the position offset to the triangle
void loadPositionOffset(float offset)
{
	setFloat(gLocPositionOffset, offset);
}

// bind texture units
void bindTextureUnits(struct TexturedModel texturedModel)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texturedModel.textureID);
}

void loadBricksTextureSampler(GLuint samplerID)
{
	setInt(gLocBricksSampler, samplerID);
}

void loadSkyboxSampler(GLuint samplerID)
{
	setInt(gLocSkyboxSampler, samplerID);
}

void loadViewerPosition(glm::vec3 viewerPos)
{
	setVector3v(gLocViewerPosition, viewerPos);
}

void loadFaceTextureSampler(GLuint samplerID)
{
	setInt(gLocFaceSampler, samplerID);
}

void loadMixParam(GLfloat value)
{
	setFloat(gLocMixParam, value);
}

void loadModelMatrix(glm::mat4 transformationMatrix)
{
	setMat4(gLocModelMatrix, transformationMatrix);
}

void loadViewMatrix(glm::mat4 transformationMatrix)
{
	setMat4(gLocViewMatrix, transformationMatrix);
}

void loadProjectionMatrix(glm::mat4 transformationMatrix)
{
	setMat4(gLocProjectionMatrix, transformationMatrix);
}

void loadMaterialProperties(struct Material material)
{
	setVector3v(gLocMaterialAmbient, material.ambient);
	setVector3v(gLocMaterialDiffuse, material.diffuse);
	setVector3v(gLocMaterialSpecular, material.specular);
	setFloat(gLocMaterialShininess, material.shininess);
}

void loadLightProperties(std::vector<struct Light> lights, struct Camera* camera)
{
	for(int i=0; i<lights.size(); i++)
	{
		setVector3v(gLocLightAmbient[i], lights[i].ambient);
		setVector3v(gLocLightDiffuse[i], lights[i].diffuse);
		setVector3v(gLocLightSpecular[i], lights[i].specular);

		// for directional light
		if(lights[i].lightType == 0) // if directional light
		{
			setVector3v(gLocLightDirection[i], lights[i].lightDirection);
		}

		// for point light
		if (lights[i].lightType == 1)
		{
			setVector3v(gLocLightPosition[i], lights[i].lightPosition);
			setFloat(gLocLightAttenuationConstant[i], lights[i].constant);
			setFloat(gLocLightAttenuationLinera[i], lights[i].linear);
			setFloat(gLocLightAttenuationQuadratic[i], lights[i].quadratic);
		}

		// for spot light
		if (lights[i].lightType == 2)
		{
			setVector3v(gLocLightPosition[i], camera->position);
			setVector3v(gLocLightDirection[i], camera->cameraFront);
			setFloat(gLocLightCutoff[i], lights[i].cutoff);
			setFloat(gLocLightOuterCutoff[i], lights[i].outerCutoff);
		}
		setInt(gLocLightType[i], lights[i].lightType);
	}
}