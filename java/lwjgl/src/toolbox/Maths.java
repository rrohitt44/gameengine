package toolbox;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import entities.Camera;

/**
 * @author rohit
 * Date: 21/05/2019
 * 
 * This is for all the maths related activities we are using in our game
 */
public class Maths
{
	public static Matrix4f createTransformationMatrix(
				Vector3f translation, // translation
				float rx, float ry, float rz, // rotation
				float scale // scale
			)
	{
		// create a new empty matrix first
		Matrix4f matrix = new Matrix4f();
		
		// set this matrix to identity
		matrix.setIdentity();
		
		// translate
		Matrix4f.translate(translation, matrix, matrix);
		
		// rotate
		Matrix4f.rotate((float) Math.toRadians(rx), new Vector3f(1, 0, 0), matrix, matrix); // around x axis
		Matrix4f.rotate((float) Math.toRadians(ry), new Vector3f(0, 1, 0), matrix, matrix); // around y axis
		Matrix4f.rotate((float) Math.toRadians(rz), new Vector3f(0, 0, 1), matrix, matrix); // around z axis
		
		// scale
		Matrix4f.scale(new Vector3f(scale, scale, scale), matrix, matrix);
		return matrix;
	}
	
	/**
	 * @param translation
	 * @param scale
	 * @return
	 * 
	 * Create transformation matrix from 2d position and scale
	 */
	public static Matrix4f createTransformationMatrix(Vector2f translation, Vector2f scale) 
	{
		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();
		Matrix4f.translate(translation, matrix, matrix);
		Matrix4f.scale(new Vector3f(scale.x, scale.y, 1f), matrix, matrix);
		return matrix;
	}
	
	/**
	 * @param camera
	 * @return
	 * Creates the view matrix
	 */
	public static Matrix4f createViewMatrix(Camera camera)
	{
		// create a new empty matrix first
		Matrix4f matrix = new Matrix4f();
		
		// set this matrix to identity
		matrix.setIdentity();
		
		// rotate
		Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), matrix, matrix); // around x axis
		Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), matrix, matrix); // around y axis
		Matrix4f.rotate((float) Math.toRadians(camera.getRoll()), new Vector3f(0, 0, 1), matrix, matrix); // around z axis
		
		Vector3f cameraPos = camera.getPosition();
		Vector3f negativeCameraPos = new Vector3f(-cameraPos.x, -cameraPos.y, -cameraPos.z);
		
		// now translate the camera in the exactly opposite direction of the world
		// because world moves in opposite direction of the camera
		Matrix4f.translate(negativeCameraPos, matrix, matrix);
		return matrix;
	}
	
	/**
	 * @param p1
	 * @param p2
	 * @param p3
	 * @param pos
	 * @return
	 * method for barycentric interpolation which calculates height of the triangle at the players position 
	 */
	public static float barryCentric(
			Vector3f p1, Vector3f p2, Vector3f p3, // three points on the triangle
			Vector2f pos // players x and z positions
			) 
	{
		float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
		float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
		float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
		float l3 = 1.0f - l1 - l2;
		return l1 * p1.y + l2 * p2.y + l3 * p3.y;
	}
}
