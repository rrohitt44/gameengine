package sunRenderer;

import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;
import shaders.ShaderProgram;
import toolbox.Maths;

/**
 * @author Rohit Muneshwar
 * @date May 26, 2019
 */
public class SunShader extends ShaderProgram
{
	private static final String VERTEX_FILE = "src/sunRenderer/sunVertex.glsl";
    private static final String FRAGMENT_FILE = "src/sunRenderer/sunFragment.glsl";
	protected int sunTexture;
	private int locationTransformationMatrix;
	private int locationProjectionMatrix;
	private int locationViewMatrix;

	public SunShader()
	{
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	/**
	 * Connects the texture units
	 */
	public void connectTextureUnits()
	{
		super.loadInt(sunTexture, 0);
	}

	/* (non-Javadoc)
	 * @see shaders.ShaderProgram#bindAttributes()
	 */
	@Override
	public void bindAttributes()
	{
		 super.bindAttribute(0, "aPosition");
	}

	/* (non-Javadoc)
	 * @see shaders.ShaderProgram#getAllUniformLocations()
	 */
	@Override
	public void getAllUniformLocations()
	{
		sunTexture = super.getUniformLocation("sunTexture");
		locationTransformationMatrix = super.getUniformLocation("u_transaformation_matrix");
		locationProjectionMatrix = super.getUniformLocation("u_projection_matrix");
		locationViewMatrix = super.getUniformLocation("u_view_matrix");
	}
	
	/**
	 * @param transformationMatrix
	 * transformation means translation, rotation and scaling
	 */
	public void loadTransformationMatrix(Matrix4f transformationMatrix)
	{
		super.loadMatrix(locationTransformationMatrix, transformationMatrix);
	}
	
	/**
	 * @param projectionMatrix
	 * loads the projection matrix
	 */
	public void loadProjectionMatrix(Matrix4f projectionMatrix)
	{
		super.loadMatrix(locationProjectionMatrix, projectionMatrix);
	}
	
	/**
	 * @param camera
	 * loads the projection matrix
	 */
	public void loadViewMatrix(Camera camera)
	{
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		super.loadMatrix(locationViewMatrix, viewMatrix);
	}


}
