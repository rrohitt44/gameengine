/**
 * 
 */
package terrains;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import models.RawModel;
import renderEngine.Loader;
import toolbox.Maths;

/**
 * @author Rohit Muneshwar
 * @date May 22, 2019
 * Represents a terrain in our game
 */
public class Terrain
{
	public static final int SIZE = 800;
	private static final float MAX_HEIGHT = 40;
	private static final float MAX_PIXELL_VALUE = 256 * 256 * 256;
	private float x;
	private float z;
	
	private RawModel rawModel;
	private TerrainTexturePack texturePack;
	private TerrainTexture blendMap;
	
	private float[][] heights;
	
	private boolean hasMultipleLights;
	
	public Terrain(int gridX, int gridZ, Loader loader, 
			TerrainTexturePack texturePack, TerrainTexture blendMap, String heightMap)
	{
		this.texturePack = texturePack;
		this.blendMap = blendMap;
		this.x = gridX * SIZE;
		this.z = gridZ * SIZE; 
		this.rawModel = generateTerrain(loader, heightMap);
	}
	
	
	public boolean isHasMultipleLights()
	{
		return hasMultipleLights;
	}


	public void setHasMultipleLights(boolean hasMultipleLights)
	{
		this.hasMultipleLights = hasMultipleLights;
	}


	/**
	 * @param loader
	 * @return
	 * generates the vertices position, normals and texcoords for the terrain
	 * It is the completely flat terrain currently, we will provide the heights to each vertex later to look it like a terrain
	 */
	private RawModel generateTerrain(Loader loader, String heightMap)
	{
		// load heightmap image into buffer
		BufferedImage image = null;
		int VERTEX_COUNT = 128;
		try
		{
			if(heightMap!=null)
			{
				image = ImageIO.read(new File("resources/"+heightMap+".png"));
				VERTEX_COUNT = image.getHeight();
				heights =new float[VERTEX_COUNT][VERTEX_COUNT];
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
				
		
		int count = VERTEX_COUNT * VERTEX_COUNT; // total vertex count for the terrain
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count*2];
		int[] indices = new int[6*(VERTEX_COUNT-1)*(VERTEX_COUNT-1)];
		int vertexPointer = 0;
		for(int i=0;i<VERTEX_COUNT;i++){
			for(int j=0;j<VERTEX_COUNT;j++){
				vertices[vertexPointer*3] = -(float)j/((float)VERTEX_COUNT - 1) * SIZE;
				float height=0;
				if(image!=null)
				{
					height = getHeight(j, i, image);
					heights[j][i] = height;
				}
				vertices[vertexPointer*3+1] = height;
				vertices[vertexPointer*3+2] = -(float)i/((float)VERTEX_COUNT - 1) * SIZE;
				
				Vector3f normal = new Vector3f(0, 1, 0);
				if(image!=null)
				{
					normal = calculateNormal(j, i, image);
				}
				normals[vertexPointer*3] = normal.x;
				normals[vertexPointer*3+1] = normal.y;
				normals[vertexPointer*3+2] = normal.z;
				textureCoords[vertexPointer*2] = (float)j/((float)VERTEX_COUNT - 1);
				textureCoords[vertexPointer*2+1] = (float)i/((float)VERTEX_COUNT - 1);
				vertexPointer++;
			}
		}
		int pointer = 0;
		for(int gz=0;gz<VERTEX_COUNT-1;gz++){
			for(int gx=0;gx<VERTEX_COUNT-1;gx++){
				int topLeft = (gz*VERTEX_COUNT)+gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz+1)*VERTEX_COUNT)+gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}
		return loader.loadToVAO(vertices, textureCoords, normals, indices);
	}
	
	/**
	 * @param worldX
	 * @param worldZ
	 * @return
	 * Generates height of the terrain for any give x,z coordinates
	 */
	public float generateHeightOfTerrain(float worldX, float worldZ)
	{
		// convert these world coordinates to the position relative to the terrain
		float terrainX =  this.x - worldX;
		float terrainZ =  this.z - worldZ;
		float gridSquareSize = SIZE / ((float) heights.length - 1);
		
		// calculate the point is in which grid
		int gridX = (int) Math.floor(terrainX/gridSquareSize);
		int gridZ = (int) Math.floor(terrainZ/gridSquareSize);
		
		if(gridX>=heights.length-1 || gridZ>=heights.length-1 || gridX<0 || gridZ<0)
		{
			return 0;
		}
		
		// calculate where on the grid square the player is
		float xCoord = (terrainX % gridSquareSize)/gridSquareSize;
		float zCoord = (terrainZ % gridSquareSize)/gridSquareSize;
		
		// find on which triangle the player is standing on
		float answer;
		
		// use barrycentric interpolation
		if (xCoord <= (1-zCoord)) 
		{
			answer = Maths
					.barryCentric(new Vector3f(0, heights[gridX][gridZ], 0), new Vector3f(1,
							heights[gridX + 1][gridZ], 0), new Vector3f(0,
							heights[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
		} else 
		{
			answer = Maths
					.barryCentric(new Vector3f(1, heights[gridX + 1][gridZ], 0), new Vector3f(1,
							heights[gridX + 1][gridZ + 1], 1), new Vector3f(0,
							heights[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
		}
		return answer;
	}
	
	/**
	 * @param x
	 * @param z
	 * @param generator
	 * @return
	 * calculates normal of a particular vertex
	 */
	private Vector3f calculateNormal(int x, int z, BufferedImage image)
	{
		//calculate height of all four neighbouring vertices
		float heightL = getHeight(x-1, z, image);
		float heightR = getHeight(x+1, z, image);
		float heightD = getHeight(x, z-1, image);
		float heightU = getHeight(x, z+1, image);
		Vector3f normal = new Vector3f(heightL-heightR, 2f, heightD - heightU);
		normal.normalise();
		return normal;
	}
	
	/**
	 * @param x
	 * @param z
	 * @param image
	 * @return
	 * 
	 * Get height of a particular pixel on the height map
	 */
	private float getHeight(int x, int z, BufferedImage image)
	{
		if(x<0 || x>=image.getHeight() || z<0 || z>=image.getHeight()) // we are out of bound here
		{
			return 0;
		}
		
		float height = image.getRGB(x, z); // it will return a value between -MAX_PIXEL_COLOR and 0
		height += MAX_PIXELL_VALUE/2f;
		height /= MAX_PIXELL_VALUE/2f;
		height *= MAX_HEIGHT;
		return height;
	}

	// getters/setters
	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getZ()
	{
		return z;
	}

	public void setZ(float z)
	{
		this.z = z;
	}

	public RawModel getRawModel()
	{
		return rawModel;
	}

	public void setRawModel(RawModel rawModel)
	{
		this.rawModel = rawModel;
	}

	public TerrainTexturePack getTexturePack()
	{
		return texturePack;
	}

	public void setTexturePack(TerrainTexturePack texturePack)
	{
		this.texturePack = texturePack;
	}

	public TerrainTexture getBlendMap()
	{
		return blendMap;
	}

	public void setBlendMap(TerrainTexture blendMap)
	{
		this.blendMap = blendMap;
	}

	public float[][] getHeights()
	{
		return heights;
	}

	public void setHeights(float[][] heights)
	{
		this.heights = heights;
	}

	
	
}
