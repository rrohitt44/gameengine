#ifndef H_MASTER_RENDERER
#define H_MASTER_RENDERER

// headers
#include<iostream>
#include<vector>
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"EntityShaderProgram.h"
#include"Camera.h"
#include"Loader.h"
#include"OpenGLUtils.h"
#include"EntityRenderer.h"
#include"EntityMapsRenderer.h"
#include"SunRenderer.h"
#include"TerrainRenderer.h"
#include"GeneralRenderer.h"
#include"SingleColorRenderer.h"
#include"SingleColorShaderProgram.h"
#include"framebuffers.h"
#include"FillScreenRenderer.h"
#include"PostProcessing.h"
#include"SkyboxRenderer.h"
#include"SkyboxShaderProgram.h"

extern struct FBO gFbo;;

void prepareMasterRenderer(glm::mat4 projectionMatrix);
void renderAll(std::vector<struct Light> lights, struct Camera* camera);
void cleanUpMasterRenderer(); // clean up activities
#endif // !H_MASTER_RENDERER
