#version 330 core

in vec3 out_normal;
in vec2 out_texcoord;
in vec3 out_frag_pos;

out vec4 FragColor;

uniform samplerCube u_skybox_sampler;

uniform vec3 u_viewer_position;


struct Material
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

struct Light {
	vec3 direction; // for Directional Lights
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
	int lightType;
	// for point Light
	vec3 position;
	float constant;
	float linear;
	float quadratic;

	// for spot light
	float cutoff; // this also needs spot lights' position and direction vectors
	float outerCutoff;
};

uniform Light light[6]; 

uniform Material material;

vec4 calculateFinalColor(vec3 lightDir,float attenuation, float intensity, int lightIndex);

void main()
{
	// local variables
	float attenuation = 1.0;
	float intensity = 1.0;
	vec4 resultantLight;
	// code
	vec3 lightDir;
	for(int i=0; i<6;i ++)
	{
		if(light[i].lightType == 2)// flash light
		{
			lightDir = normalize(light[i].position - out_frag_pos); // get light direction vector
			float theta = dot(lightDir, normalize(-light[i].direction));
			float epsilon   = light[i].cutoff - light[i].outerCutoff;
			intensity = clamp((theta - light[i].outerCutoff) / epsilon, 0.0, 1.0);   
			if(theta > light[i].cutoff) // remember that we're working with angles as cosines instead of degrees so a '>' is used.
			{       
			  // do lighting calculations
			  resultantLight += calculateFinalColor(lightDir,attenuation,intensity,i);
			}
			else  // else, use ambient light so scene isn't completely dark outside the spotlight.
			  resultantLight += vec4(light[i].ambient * vec3(1.0), 1.0);
		}else
		{

		// Direction light calculations
		if(light[i].lightType == 0) // direction light
		{
			// switch its direction; it's now a direction vector pointing towards the light source
			lightDir = normalize(-light[i].direction); // get light direction vector
		}else if(light[i].lightType == 1)// point light
		{
		
			float distance = length(light[i].position - out_frag_pos);
			attenuation = 1.0 / (light[i].constant + light[i].linear * distance + light[i].quadratic * (distance * distance));
			lightDir = normalize(light[i].position - out_frag_pos); // get light direction vector
		}

			resultantLight += calculateFinalColor(lightDir,attenuation,intensity,i);
		}
	}
	FragColor = resultantLight;
}

vec4 calculateFinalColor(vec3 lightDir,float attenuation,float intensity, int lightIndex)
{
	// ambient lighting
	vec3 ambient = light[lightIndex].ambient * material.ambient;

	// diffuse lighting
	vec3 norm = normalize(out_normal);
	
	
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = (diff * material.diffuse)* light[lightIndex].diffuse;
	
	// specular lighting
	vec3 viewDir = normalize(u_viewer_position - out_frag_pos);
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	vec3 specular = (spec * material.specular) * light[lightIndex].specular;

	// environment mapping
	vec3 I = normalize(out_frag_pos - u_viewer_position);// camera direction vector
	vec3 R = reflect(I, norm); // reflection vector
	float ratio = 1.00/1.52;
	//vec3 R = refract(I, norm, ratio); // refraction vector

	//diffuse *= texture(u_skybox_sampler, R).rgb;
	// include attenuation value to ambient diffuse and specular components
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	diffuse *= intensity;
	specular *= intensity;

	//vec4 result = vec4(ambient + diffuse + specular, 1.0) * objectColor;
	vec3 result = ambient + diffuse + specular;
	
	 return vec4(result, 1.0);
	// for environment mapping
	//return vec4(texture(u_skybox_sampler, R).rgb, 1.0);
}