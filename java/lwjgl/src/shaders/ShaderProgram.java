package shaders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * @author rohit
 * Date: 20/5/2019
 */
public abstract class ShaderProgram
{
	private int programID;
	private int vertexShaderID;
	private int fragmentShaderID;
	private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
	
	/**
	 * @param vertexShaderFile
	 * @param fragmentShaderFile
	 * Take the vertex and fragment shader files and create shader program out of it
	 */
	public ShaderProgram(String vertexShaderFile, String fragmentShaderFile)
	{
		// load vertex shader file
		vertexShaderID = loadShader(vertexShaderFile,GL20.GL_VERTEX_SHADER);
		//load fragment shader file
		fragmentShaderID = loadShader(fragmentShaderFile,GL20.GL_FRAGMENT_SHADER);
		
		// create shader program
		programID = GL20.glCreateProgram();
		
		// attach vertex and fragment shaders to this program
		GL20.glAttachShader(programID, vertexShaderID);
		GL20.glAttachShader(programID, fragmentShaderID);

		bindAttributes(); // bind the texture attributes
		
		// link the shaders to the shader program
		GL20.glLinkProgram(programID);
		
		// check if any linkage error occured
		GL20.glValidateProgram(programID);
		
		// get all the uniform locations
		getAllUniformLocations();
	}
	
	// for binding the shader program specific attributes
	public abstract void bindAttributes();
	
	public abstract void getAllUniformLocations();
	
	
	/**
	 * @param uniformName
	 * @return
	 * returns the uniform location of a uniform name
	 */
	public int getUniformLocation(String uniformName)
	{
		return GL20.glGetUniformLocation(programID, uniformName);
	}
	/**
	 * @param file
	 * @param type
	 * @return
	 * 
	 * Load the shader file into memory and create the shader from this shader source
	 */
	private static int loadShader(String file, int type)
	{
		StringBuilder shaderSource = new StringBuilder();
		try{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while((line = reader.readLine())!=null){
				shaderSource.append(line).append("//\n");
			}
			reader.close();
		}catch(IOException e){
			e.printStackTrace();
			System.exit(-1);
		}
		// create empty shader
		int shaderID = GL20.glCreateShader(type);
		
		// provide the shader source
		GL20.glShaderSource(shaderID, shaderSource);
		
		// compile the shader source
		GL20.glCompileShader(shaderID);
		
		// check if any compile time error occured
		if(GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS )== GL11.GL_FALSE){
			System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
			System.err.println("Could not compile +"+type+" shader!");
			System.exit(-1);
		}
		
		// return the shader id created
		return shaderID;
	}
	
	/**
	 * Use this shader program object
	 */
	public void start()
	{
		GL20.glUseProgram(this.programID);
	}
	
	/**
	 * unbind this shader program object
	 */
	public void stop()
	{
		GL20.glUseProgram(0);
	}
	
	/**
	 * clean up the memory objects created for rendering
	 */
	public void cleanUp(){
		stop();
		GL20.glDetachShader(programID, vertexShaderID);
		GL20.glDetachShader(programID, fragmentShaderID);
		GL20.glDeleteShader(vertexShaderID);
		GL20.glDeleteShader(fragmentShaderID);
		GL20.glDeleteProgram(programID);
	}
	
	/**
	 * @param attribute
	 * @param variableName
	 * bind the attribute to the specified location
	 */
	protected void bindAttribute(int attribute, String variableName){
		GL20.glBindAttribLocation(programID, attribute, variableName);
	}
	
	protected void loadFloat(int location, float value){
		GL20.glUniform1f(location, value);
	}
	
	protected void loadInt(int location, int value){
		GL20.glUniform1i(location, value);
	}
	
	protected void loadVector(int location, Vector3f vector){
		GL20.glUniform3f(location,vector.x,vector.y,vector.z);
	}
	
	protected void loadVector(int location, Vector4f vector){
		GL20.glUniform4f(location,vector.x,vector.y,vector.z, vector.w);
	}
	
	protected void load2DVector(int location, Vector2f vector){
		GL20.glUniform2f(location,vector.x,vector.y);
	}
	
	protected void loadBoolean(int location, boolean value){
		float toLoad = 0;
		if(value){
			toLoad = 1;
		}
		GL20.glUniform1f(location, toLoad);
	}
	
	protected void loadMatrix(int location, Matrix4f matrix){
		matrix.store(matrixBuffer);
		matrixBuffer.flip();
		GL20.glUniformMatrix4(location, false, matrixBuffer);
	}
}
