// headers
#include"framebuffers.h"

// function prototype declarations
GLuint createTextureAttachmentForRenderbuffer(GLint width, GLint height, GLenum format);
GLuint createRenderBufferAttachmentForFramebuffer(GLint width, GLint height, GLenum format);

// create a framebuffer and attach the necessary textures and return framebuffer id
/*
We have to attach at least one buffer (color, depth or stencil buffer).
There should be at least one color attachment.
All attachments should be complete as well (reserved memory).
Each buffer should have the same number of samples.
*/
GLuint setUpFrameBuffer(struct FBO *gFbo)
{
	// code
	// generate framebuffer
	glGenFramebuffers(1, &gFbo->fboId);

	// bind framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, gFbo->fboId);
	gFBOList.push_back(gFbo->fboId);

	// create texture attachments
	gFbo->colorAttachment = createTextureAttachmentForRenderbuffer(WIN_WIDTH, WIN_HEIGHT, GL_RGB); // color attachment
	//GLuint depthTextureAttachment = createTextureAttachmentForRenderbuffer(WIN_WIDTH, WIN_HEIGHT, GL_DEPTH_COMPONENT); // depth attachment

	// attach the texture attachments the framebuffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gFbo->colorAttachment, 0); // color attachment
	//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTextureAttachment, 0); // depth attachment

	// create renderbuffer attachment
	GLuint rboDepthStencileAttachment = createRenderBufferAttachmentForFramebuffer(WIN_WIDTH, WIN_HEIGHT, GL_DEPTH24_STENCIL8);

	// attach renderbuffer as an attachment
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rboDepthStencileAttachment);

	// check if our framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		logStaticData("ERROR::FRAMEBUFFER:: Framebuffer creation is not complete. Some error has occured.");
	}

	// unbind framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return gFbo->fboId;
}

void bindFbo(GLuint fbo)
{
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void unbindFbo()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/*
createTextureAttachmentForRenderbuffer:
For this texture, we're only allocating memory and not actually filling it. 
Filling the texture will happen as soon as we render to the framebuffer
*/
GLuint createTextureAttachmentForRenderbuffer(GLint width, GLint height, GLenum format)
{
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	gTexturesList.push_back(texture);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	return texture;
}

/*
renderbuffer objects are generally write-only they are often used as depth and stencil attachments, 
since most of the time we don't really need to read values from the depth and stencil buffers but 
still care about depth and stencil testing.
We need the depth and stencil values for testing, but don't need to sample these values so a renderbuffer 
object suits this perfectly. When we're not sampling from these buffers, a renderbuffer object is generally 
preferred since it's more optimized.
*/
GLuint createRenderBufferAttachmentForFramebuffer(GLint width, GLint height, GLenum format)
{
	GLuint rbo;
	glGenRenderbuffers(1, &rbo);
	gRBOList.push_back(rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, format, 800, 600);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	return rbo;
}