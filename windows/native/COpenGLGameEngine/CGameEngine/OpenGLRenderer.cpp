// header files

#include"OpenGLRenderer.h"
#include"DisplayManager.h"
#include"ShaderProgram.h"
#include"Models.h"
#include"OpenGLUtils.h"
#include"GameUtils.h"
#include"Loader.h"
#include"MathUtils.h"
#include"EntityRenderer.h"
#include"SunRenderer.h"
#include"MasterRenderer.h"
#include"AssimpLoader.h"
#include"GeneralRenderer.h"
#include"SkyboxRenderer.h"
#include"Player.h"
#include"ObjLoader.h"

using namespace std;

// function prototype declarations

// global variables
struct TexturedModel gCubeModelTexture;
struct TexturedModel gCubeMapModelTexture;
struct TexturedModel gQuadTexturedModel;
struct TexturedModel gFillScreenQuadTexturedModel;

// transformation matrices
glm::mat4 gModelMatrix;
glm::mat4 gViewMatrix;
extern glm::mat4 gProjectionMatrix;
extern std::vector<struct Light> lights;
extern struct Camera camera;

float gSideOffset = -4.5f;
float gYOffset = 3.8f;
float gZOffset = -8.0f;
glm::vec3 cubePositions[] = {
  glm::vec3(0.0f + gSideOffset,  0.0f+ gYOffset,  -20.0f + gZOffset),
  glm::vec3(2.0f + gSideOffset,  5.0f + gYOffset, -15.0f + gZOffset),
  glm::vec3(-1.5f + gSideOffset, -2.2f + gYOffset, -2.5f + gZOffset),
  glm::vec3(-3.8f + gSideOffset, -2.0f + gYOffset, -12.3f + gZOffset),
  glm::vec3(2.4f + gSideOffset, -0.4f + gYOffset, -3.5f + gZOffset),
  glm::vec3(-1.7f + gSideOffset,  3.0f + gYOffset, -7.5f + gZOffset),
  glm::vec3(1.3f + gSideOffset, -2.0f + gYOffset, -2.5f + gZOffset),
  glm::vec3(1.5f + gSideOffset,  2.0f + gYOffset, -2.5f + gZOffset),
  glm::vec3(1.5f + gSideOffset,  0.2f + gYOffset, -1.5f + gZOffset),
  glm::vec3(-1.3f + gSideOffset,  1.0f + gYOffset, -1.5f + gZOffset)
};

extern float deltaTime; // Time between current frame and last frame
long lastFrame = 0; // Time of last frame
extern long openGLInitializationTime; // time at which OpenGL is initialized
GLfloat rotationAngle = 0.0f;
static GLfloat gPrevRotationAngle=0.0f;

std::vector<Mesh> nanoSuitMeshes;
std::vector<Mesh> playerMeshes;

extern float gFov;

// initialize rendering
void init(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// open logger
	openFileLogger();

	// create window
	createWindow(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
	logStaticData("Window created successfully.");

	openGLInitializationTime = timeSinceEpochMillisec();

	// initialize OpenGL context
	initializeOpenGL();
	logStaticData("OpenGL context initialized successfully.");

	// prepare the rendering
	prepareOpenGLForRendering();
}

// initialize OpenGL Context
void initializeOpenGL(void)
{
	//function prototypes
	void resize(int, int);

	//local variables
	//this structure describes the pixel format of the drawing surface
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//initialization of the structure PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR); //size of this data structure
	pfd.nVersion = 1; //version of this data structure
	//specify properties of pixel buffer
	pfd.dwFlags = PFD_DRAW_TO_WINDOW //buffer can draw to window or device surface
		| PFD_SUPPORT_OPENGL //the buffer supports OpenGL drawing
		| PFD_DOUBLEBUFFER; //for double buffer
	pfd.iPixelType = PFD_TYPE_RGBA; //type of pixel data
	pfd.cColorBits = 32;  //number of color bitplanes; It considers only 24, ignores alpha
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	//DC of WM_PAINT can only paint client area, in order to paint whole window, we must use GetDC
	//Every window has it's only rendering context; Usually one viewport have one rendering context
	ghdc = GetDC(ghwnd);

	//attemp to match an appropriate pixel format supported by a device context
	//to a given pixel format specification
	iPixelFormatIndex = ChoosePixelFormat(
		ghdc, //device context to search for a best pixel format match
		&pfd); // pixel format for which a best match is sought

	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.1f, 0.2f, 0.5f, 1.0f);

	//resize(WIN_WIDTH, WIN_HEIGHT); //not needed for double buffering
}

// render scene to display
void render(void)
{
	display(); // display our game
}

// initialize VAOs, VBOs, EBOs, Shader programs
void prepareOpenGLForRendering()
{
	logStaticData("Initializing GLEW");
	//GLEW initialization code for GLSL
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	logStaticData("GLEW initialized successfully.");

	// create projection matrix
	gFov = 45.0f;
	gProjectionMatrix = createPerspectiveProjectionMatrix(gFov, WIN_WIDTH, WIN_HEIGHT);

	// prepare shaders for rendering
	prepareMasterRenderer(gProjectionMatrix);

	// load data to VAO
	loadCubeToVAO(&gCubeModelTexture.rawModel, false);
	loadCubeToVAO(&gCubeMapModelTexture.rawModel, true);
	loadQuadToVAO(&gQuadTexturedModel.rawModel,false);
	loadQuadToVAO(&gFillScreenQuadTexturedModel.rawModel, true);

	/************TEXTURES/MODEL FILES********************/
	// images
	const string containerDiffuseImageFile = IMAGES_RESOURCE_FILE_LOC + string("container2.png");
	const string containerSpecularImageFile = IMAGES_RESOURCE_FILE_LOC + string("container2_specular.png");
	const string marbleDiffuseImageFile = IMAGES_RESOURCE_FILE_LOC + string("marble.jpg");
	const string whiteImageFile = IMAGES_RESOURCE_FILE_LOC + string("white.png");
	const string plainDiffuseImageFile = IMAGES_RESOURCE_FILE_LOC + string("metal.png");
	const string flowersPlantImageFile = IMAGES_RESOURCE_FILE_LOC + string("flower.png");
	const string pinePlantImageFile = IMAGES_RESOURCE_FILE_LOC + string("pine.png");
	const string cherryPlantImageFile = IMAGES_RESOURCE_FILE_LOC + string("cherry.png");
	const string grassImageFile = IMAGES_RESOURCE_FILE_LOC + string("grass.png");
	const string grassyGroundImageFile = IMAGES_RESOURCE_FILE_LOC + string("metal.png");
	const string transparentWindowImageFile = IMAGES_RESOURCE_FILE_LOC + string("blending_transparent_window.png");
	const string blendMapGroundImageFile = IMAGES_RESOURCE_FILE_LOC + string("blendMap.png");
	const string grassy2GroundImageFile = IMAGES_RESOURCE_FILE_LOC + string("grassy.png");
	const string mudGroundImageFile = IMAGES_RESOURCE_FILE_LOC + string("mud.png");
	const string pathwayGroundImageFile = IMAGES_RESOURCE_FILE_LOC + string("path.png");
	const string dirtGroundImageFile = IMAGES_RESOURCE_FILE_LOC + string("dirt.png");
	const string grassFlowersGroundImageFile = IMAGES_RESOURCE_FILE_LOC + string("grassFlowers.png");

	// models
	const string nanosuitFile = MODELS_RESOURCE_FILE_LOC + string("nanosuit/nanosuit.obj");
	const string playerFile = MODELS_RESOURCE_FILE_LOC + string("person.obj");
	const string grassModelFile = MODELS_RESOURCE_FILE_LOC + string("grassModel.obj");
	const string pineModelFile = MODELS_RESOURCE_FILE_LOC + string("pine.obj");
	const string cherryModelFile = MODELS_RESOURCE_FILE_LOC + string("cherry.obj");
	const string playerTexture = IMAGES_RESOURCE_FILE_LOC + string("playerTexture.png");

	// textures
	GLuint containerDiffuseTextureID = loadTexture(containerDiffuseImageFile);
	GLuint containerSpecularTextureID = loadTexture(containerSpecularImageFile);
	GLuint marbleDiffuseTextureID = loadTexture(marbleDiffuseImageFile);
	GLuint plainDiffuseTextureID = loadTexture(plainDiffuseImageFile);
	GLuint whiteTextureID = loadTexture(whiteImageFile);
	GLuint grassDiffuseTextureID = loadTexture(grassImageFile);
	GLuint pineDiffuseTextureID = loadTexture(pinePlantImageFile);
	GLuint cherryDiffuseTextureID = loadTexture(cherryPlantImageFile);
	GLuint flowersPlantDiffuseTextureID = loadTexture(flowersPlantImageFile);
	GLuint grassyGroundDiffuseTextureID = loadTexture(grassyGroundImageFile);
	GLuint transparentWindowDiffuseTextureID = loadTexture(transparentWindowImageFile);
	GLuint skyboxId = loadSkybox();
	GLuint playerTextureID = loadTexture(playerTexture);
	GLuint grassyBackgroundGroundTextureID = loadTexture(grassy2GroundImageFile);
	GLuint pathwayGroundTextureID = loadTexture(pathwayGroundImageFile);
	GLuint mudGroundTextureID = loadTexture(mudGroundImageFile);
	GLuint dirtGroundTextureID = loadTexture(dirtGroundImageFile);
	GLuint grassFlowersGroundTextureID = loadTexture(grassFlowersGroundImageFile);
	GLuint blendMapGroundTextureID = loadTexture(blendMapGroundImageFile);

	/*********OBJ/COLLADA MODELS*************************/
	// load models
	//nanoSuitMeshes = loadModelUsingAssimp(nanosuitFile);
	//processLoadedModels(nanoSuitMeshes);

	//playerMeshes = loadModelUsingAssimp(playerFile);
	//processLoadedModels(playerMeshes);

	// raw models
	RawModel playerRawModel;
	RawModel grassRawModel;
	RawModel pineRawModel;
	RawModel cherryRawModel;

	// loaded models
	loadObjFile(playerFile, &playerRawModel);
	loadObjFile(grassModelFile, &grassRawModel);
	loadObjFile(pineModelFile, &pineRawModel);
	loadObjFile(cherryModelFile, &cherryRawModel);
	
	// set player properties
	player.texturedModel.rawModel = playerRawModel;
	player.material.diffuse = playerTextureID;
	player.material.specular = playerTextureID;
	player.material.shininess = 1.0f;
	player.translate = camera.position;//glm::vec3(-50, -0.1, -10);
	player.scale = 0.2;
	player.rotateX = 0;
	player.rotateY = 180;
	player.rotateZ = 0;
	player.useFakeLighting = false;
	//processObjModels(player);


	// skybox
	gCubeMapModelTexture.textureID = skyboxId;
	//gCubeMapEntity.rotateZ = 90;
	gCubeMapEntity.texturedModel = gCubeMapModelTexture;

	/*************LIGHTS*******************/
	// Create Light entities
	struct Light sun;
	struct Light pointLight1;
	struct Light pointLight2;
	struct Light pointLight3;
	struct Light pointLight4;
	struct Light spotLight;

	//loadInterleavedDataToVAO(numCubeElements, cubeVertices, false, true, &gLightObjectModel.rawModel);
	setUpDirectionalLight(&sun, &gCubeModelTexture);
	setUpPointLight(&pointLight1, &gCubeModelTexture, glm::vec3(-50.7f, 5.2f, -50.0f), glm::vec3(0,0,1));
	setUpPointLight(&pointLight2, &gCubeModelTexture, glm::vec3(-34.3f, 4.3f, -40.0f), glm::vec3(1, 0,0));
	setUpPointLight(&pointLight3, &gCubeModelTexture, glm::vec3(-20.0f, 5.0f, -25.0f), glm::vec3(0, 1, 0));
	setUpPointLight(&pointLight4, &gCubeModelTexture, glm::vec3(-7.0f, 4.0f, -9.0f), glm::vec3(1, 1, 0));
	setUpSpotLight(&spotLight, &gCubeModelTexture);

	// add these lights to the list of lights
	processLight(sun);
	processLight(pointLight1);
	processLight(pointLight2);
	processLight(pointLight3);
	processLight(pointLight4);
	processLight(spotLight);
	
	/***********ENTITIES**************************/
	// Create Other entities
	for (unsigned int i = 0; i < 10; i++)
	{
		struct Entity entity;
		entity.texturedModel = gCubeModelTexture;
		entity.translate = cubePositions[i];
		entity.rotateX = 0;
		entity.rotateY = 0;
		entity.rotateZ = 0;
		entity.scale = 0.5;
		entity.texturedModel.skyoxID = skyboxId;

		//http://devernay.free.fr/cours/opengl/materials.html - for material values of different types of objects
		if (i == 0)
		{
			// emerald
			entity.material.ambient = glm::vec3(0.0215f, 0.1745f, 0.0215f);
			entity.material.diffuse = glm::vec3(0.07568f,0.61424f, 0.07568f);
			entity.material.specular = glm::vec3(0.633f, 0.727811f,0.633f);
			entity.material.shininess = 0.6f;
		}else if (i == 1)
		{
			// pearl
			entity.material.ambient = glm::vec3(0.25f, 0.20725f, 0.20725f);
			entity.material.diffuse = glm::vec3(1.0f, 0.829f, 0.829f);
			entity.material.specular = glm::vec3(0.296648f, 0.296648f, 0.296648f);
			entity.material.shininess = 0.088f;
		}
		else if (i == 2)
		{
			// ruby
			entity.material.ambient = glm::vec3(0.1745f, 0.01175f, 0.01175f);
			entity.material.diffuse = glm::vec3(0.61424f, 0.04136f, 0.04136f);
			entity.material.specular = glm::vec3(0.727811f, 0.626959f, 0.626959f);
			entity.material.shininess = 0.6f;
		}
		else if (i == 3)
		{
			// chrome
			entity.material.ambient = glm::vec3(0.25f, 0.25f, 0.25f);
			entity.material.diffuse = glm::vec3(0.4f, 0.4f, 0.4f);
			entity.material.specular = glm::vec3(0.774597f, 0.774597f, 0.774597f);
			entity.material.shininess = 0.6f;
		}
		else if (i == 4)
		{
			// copper
			entity.material.ambient = glm::vec3(0.19125f, 0.0735f, 0.0225f);
			entity.material.diffuse = glm::vec3(0.7038f,0.27048f, 0.0828f);
			entity.material.specular = glm::vec3(0.256777f, 0.137622f, 0.086014f);
			entity.material.shininess = 0.1f;
		}
		else if (i == 5)
		{
			// gold
			entity.material.ambient = glm::vec3(0.24725f, 0.1995f, 0.0745f);
			entity.material.diffuse = glm::vec3(0.75164f, 0.60648f, 0.22648f);
			entity.material.specular = glm::vec3(0.628281f, 0.555802f, 0.366065f);
			entity.material.shininess = 0.4f;
		}
		else if (i == 6)
		{
			// silver
			entity.material.ambient = glm::vec3(0.19225f, 0.19225f, 0.19225f);
			entity.material.diffuse = glm::vec3(0.50754f, 0.50754f, 0.50754f);
			entity.material.specular = glm::vec3(0.508273f, 0.508273f, 0.508273f);
			entity.material.shininess = 0.4f;
		}
		else if (i == 7)
		{
			// cyan rubber
			entity.material.ambient = glm::vec3(0.0f, 0.05f, 0.05f);
			entity.material.diffuse = glm::vec3(0.4f, 0.5f, 0.5f);
			entity.material.specular = glm::vec3(0.04, 0.7f, 0.7f);
			entity.material.shininess = 0.078125f;
		}
		else if (i == 8)
		{
			// yellow rubber
			entity.material.ambient = glm::vec3(0.05f, 0.05f, 0.0f);
			entity.material.diffuse = glm::vec3(0.5f, 0.5f, 0.4f);
			entity.material.specular = glm::vec3(0.7, 0.7f, 0.04f);
			entity.material.shininess = 0.078125f;
		}
		else if (i == 9)
		{
			// red plastic
			entity.material.ambient = glm::vec3(0.0f, 0.0f, 0.0f);
			entity.material.diffuse = glm::vec3(0.5f, 0.0f, 0.0f);
			entity.material.specular = glm::vec3(0.7f, 0.6f, 0.6f);
			entity.material.shininess = 0.25f;
		}
		else
		{
			// white plastic
			entity.material.ambient = glm::vec3(0.0f, 0.0f, 0.0f);
			entity.material.diffuse = glm::vec3(0.55f, 0.55f, 0.55f);
			entity.material.specular = glm::vec3(0.70f, 0.70f, 0.70f);
			entity.material.shininess = 0.25f;
		}
		processEntity(entity);
	}

	struct Entity fillScreenQuadEntity;
	fillScreenQuadEntity.texturedModel = gFillScreenQuadTexturedModel;
	processFillScreenQuadEntity(&fillScreenQuadEntity);
	/********ENTITIES WITH MAPS*************************/
	// create entity with maps
	struct EntityMap entityMap;
	entityMap.texturedModel = gCubeModelTexture;
	entityMap.translate = glm::vec3(1.5 + gSideOffset, 0.8, -3.0+gZOffset);
	entityMap.rotateX = 40;
	entityMap.rotateY = 45;
	entityMap.rotateZ = 30;
	entityMap.scale = 1;
	entityMap.isBordered = false;
	entityMap.material.diffuse = containerDiffuseTextureID;
	entityMap.material.specular = containerSpecularTextureID;
	entityMap.material.shininess = 0.25f;
	processEntityMaps(entityMap);

	
	struct EntityMap cube1Entity;
	cube1Entity.texturedModel = gCubeModelTexture;
	cube1Entity.translate = glm::vec3(-1.0f+gSideOffset, 0.8f, -1.0f + gZOffset);
	cube1Entity.rotateX = 0;
	cube1Entity.rotateY = 0;
	cube1Entity.rotateZ = 0;
	cube1Entity.scale = 1;
	cube1Entity.material.diffuse = marbleDiffuseTextureID;
	//cube1Entity.material.specular = marbleDiffuseTextureID;
	cube1Entity.material.shininess = 0.25f;
	cube1Entity.isBordered = false;
	processEntityMaps(cube1Entity);
	struct EntityMap cube2Entity;
	cube2Entity.texturedModel = gCubeModelTexture;
	cube2Entity.translate = glm::vec3(2.0f+gSideOffset, 0.8f, 0.0f+gZOffset);
	cube2Entity.rotateX = 0;
	cube2Entity.rotateY = 0;
	cube2Entity.rotateZ = 0;
	cube2Entity.scale = 1;
	cube2Entity.material.diffuse = marbleDiffuseTextureID;
	//cube2Entity.material.specular = marbleDiffuseTextureID;
	cube2Entity.material.shininess = 0.25f;
	cube2Entity.isBordered = false;
	processEntityMaps(cube2Entity);

	for (unsigned int i = 0; i < 100; i++)
	{
		float x = 0;
		float y = 0;
		float z = 0;
		
		struct EntityMap eml;
		eml.rotateX = 0;
		eml.rotateY = 0;
		eml.rotateZ = 0;


		if(i%2== 0)
		{
			// generate random positions from -400 to 400
			x = getNextFloatRandomNumberBetween0And1() * 800 - 400;
			y = 3;
			z = getNextFloatRandomNumberBetween0And1() * -600;
			eml.texturedModel = gQuadTexturedModel;
			eml.material.shininess = 0.25f;
			eml.isBordered = false;
			eml.useFakeLighting = true;
		eml.material.diffuse = flowersPlantDiffuseTextureID;//grassDiffuseTextureID;
		eml.translate = glm::vec3(x, y, z);
		eml.scale = 3;
		
		}if (i % 4 == 0)
		{
			// generate random positions from -400 to 400
			x = getNextFloatRandomNumberBetween0And1() * 800 - 400;
			y = 0;
			z = getNextFloatRandomNumberBetween0And1() * -600;
			eml.texturedModel.rawModel = pineRawModel;
			eml.material.shininess = 1;
			eml.isBordered = false;
			eml.useFakeLighting = false;
			eml.material.diffuse = pineDiffuseTextureID;
			eml.translate = glm::vec3(x, y, z);
			eml.scale = 1;
		}
		else if (i % 6 == 0)
		{
			// generate random positions from -400 to 400
			x = getNextFloatRandomNumberBetween0And1() * 800 - 400;
			y = 0;
			z = getNextFloatRandomNumberBetween0And1() * -600;
			eml.texturedModel.rawModel = cherryRawModel;
			eml.material.shininess = 1;
			eml.isBordered = false;
			eml.useFakeLighting = false;
			eml.material.diffuse = cherryDiffuseTextureID;
			eml.translate = glm::vec3(x, y, z);
			eml.scale = 2;
		}
		else
		{
			// generate random positions from -400 to 400
			x = getNextFloatRandomNumberBetween0And1() * 800 - 400;
			y = 3;
			z = getNextFloatRandomNumberBetween0And1() * -600;
			eml.texturedModel = gQuadTexturedModel;
			eml.material.shininess = 0.25f;
			eml.isBordered = false;
			eml.useFakeLighting = true;
			eml.material.diffuse = grassDiffuseTextureID;
			eml.translate = glm::vec3(x, y, z);
			eml.scale = 4;
		}

		processObjModels(eml);
	}


	/************TERRAIN*******************************/
	// create terrain
	struct EntityMap terrainMap;
	generateTerrainModel(0, 0, &terrainMap.texturedModel.rawModel);
	terrainMap.translate = glm::vec3(0.0, 0.0, 0.0);
	terrainMap.rotateX = 0;
	terrainMap.rotateY = 0;
	terrainMap.rotateZ = 0;
	terrainMap.scale = 1;
	//terrainMap.material.diffuse = grassyGroundDiffuseTextureID;
	terrainMap.material.shininess = 0.25f;
	terrainMap.material.blendMap_sampler = blendMapGroundTextureID;
	terrainMap.material.backgroundTexture_sampler = grassyBackgroundGroundTextureID;
	terrainMap.material.rTexture_sampler = dirtGroundTextureID;
	terrainMap.material.gTexture_sampler = grassFlowersGroundTextureID;
	terrainMap.material.bTexture_sampler = pathwayGroundTextureID;
	processTerrain(terrainMap);

	lastFrame = getTimeInSecondsSinceOpenGLIsInitialized();


	// initialize camera
	initializeCamera();

	/***********OPENGL STATES CONFIGURATIONS************************/
	// configure OpenGL states
	enableDepthTesting();
	enableStencileTesting();
	enableBlending();
	//enableFaceCulling();
	// rough
	
}

void display(void)
{
	// local variables
	rotationAngle += 0.1f;
	if (rotationAngle >= 360.0f)
	{
		rotationAngle = 0.0f;
	}

	//code
	long currentFrame = getTimeInSecondsSinceOpenGLIsInitialized();
	deltaTime = (currentFrame - lastFrame)/1000.0f;
	lastFrame = currentFrame;

	// clear OpenGL color and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// calculate camera offsets
	updateCameraPosition(gCameraPosition);

	gProjectionMatrix = createPerspectiveProjectionMatrix(gFov, WIN_WIDTH, WIN_HEIGHT);

	if (gDoAnimation == true)
	{
		increateEntityRotation(rotationAngle, rotationAngle, rotationAngle);
		//increateEntityMapsRotation(rotationAngle, rotationAngle, rotationAngle);
		gPrevRotationAngle = rotationAngle;
	}
	else
	{
		rotationAngle = gPrevRotationAngle;
	}
	
	// player
	movePlayer();

	// render
	renderAll(lights, &camera);
	
	
	//glFlush(); //removing this as not needed for double buffer; instead use below
	SwapBuffers(ghdc);
}

// uninitialize OpenGL context
void uninitializeOpenGL(void)
{
	// local variables
	std::vector<GLuint>::size_type index;

	// revert states
	disableFaceCulling();
	disableDepthTesting();
	disableStencileTesting();
	disableBlending();

	// clean up renderers
	cleanUpMasterRenderer();

	// delete shaders
	for (index = 0; index != gShaderList.size(); index++)
	{
		if(gShaderList[index])
			glDeleteShader(gShaderList[index]);
	}

	// delete shaders programs
	for (index = 0; index != gShaderProgramObjectList.size(); index++)
	{
		if(gShaderProgramObjectList[index])
			glDeleteProgram(gShaderProgramObjectList[index]);
	}

	// delete buffers
	for (index = 0; index != gVBOList.size(); index++)
	{
		if(gVBOList[index])
			glDeleteBuffers(1, &gVBOList[index]);
	}

	// delete VAOs
	for (index = 0; index != gVAOList.size(); index++)
	{
		if(gVAOList[index])
			glDeleteVertexArrays(1, &gVAOList[index]);
	}

	// delete textures
	for (index = 0; index != gTexturesList.size(); index++)
	{
		if (gTexturesList[index])
			glDeleteTextures(1, &gTexturesList[index]);
	}

	// delete RBOs
	for (index = 0; index != gRBOList.size(); index++)
	{
		if (gRBOList[index])
			glDeleteRenderbuffers(1, &gRBOList[index]);
	}

	// delete FBOs
	for (index = 0; index != gFBOList.size(); index++)
	{
		if (gFBOList[index])
			glDeleteFramebuffers(1, &gFBOList[index]);
	}

	// uninitialize window first
	uninitializeWindow();

	// uninitialize context
	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	// close logger
	closeFileLogger();
}