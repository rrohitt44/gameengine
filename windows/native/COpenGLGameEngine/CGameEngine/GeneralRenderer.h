#ifndef H_GENERAL_RENDERER
#define H_GENERAL_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"GeneralShaderProgram.h"
#include"Camera.h"

extern std::vector<struct Entity> gGeneralEntities;

void prepareGeneralRenderer(glm::mat4 projectionMatrix);
void processGeneral(struct Entity entity);
void renderGeneral(struct Camera* camera); // render entities
void renderGeneralUsingFBO(struct Camera* camera, struct FBO *fbo); // render entities using FBO textures
void cleanUpGeneral(); // clean up the shader program object
#endif // !H_GENERAL_RENDERER
