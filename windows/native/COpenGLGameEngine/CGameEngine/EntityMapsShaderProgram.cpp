#include"EntityMapsShaderProgram.h"

using namespace std;

GLuint gShaderProgramEntityMaps;
GLuint gLocColorEntityMaps;
GLuint gLocPositionOffsetEntityMaps;
GLuint gLocBricksSamplerEntityMaps;
GLuint gLocFaceSamplerEntityMaps;
GLuint gLocMixParamEntityMaps;
GLuint gLocModelMatrixEntityMaps;
GLuint gLocViewMatrixEntityMaps;
GLuint gLocProjectionMatrixEntityMaps;
GLuint gLocObjectColorEntityMaps;
GLuint gLocLightAmbientEntityMaps[6];
GLuint gLocLightDiffuseEntityMaps[6];
GLuint gLocLightSpecularEntityMaps[6];
GLuint gLocLightPositionEntityMaps[6];
GLuint gLocLightTypeEntityMaps[6];
GLuint gLocLightCutoffEntityMaps[6];
GLuint gLocLightOuterCutoffEntityMaps[6];
GLuint gLocLightDirectionEntityMaps[6]; // light direction for directional light
GLuint gLocLightAttenuationLineraEntityMaps[6];
GLuint gLocLightAttenuationQuadraticEntityMaps[6];
GLuint gLocLightAttenuationConstantEntityMaps[6];
GLuint gLocViewerPositionEntityMaps;
GLuint gLocMaterialDiffuseEntityMaps;
GLuint gLocMaterialSpecularEntityMaps;
GLuint gLocMaterialShininessEntityMaps;
GLuint gLocUseFakeLightingEntityMaps;
static GLuint gLocSkyColor;

// gets all uniform locations
void getAllUniformLocationsEntityMaps()
{
	gLocColorEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_vertex_color");
	gLocPositionOffsetEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_position_offset");
	gLocBricksSamplerEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_bricks_sampler");
	gLocFaceSamplerEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_face_sampler");
	gLocMixParamEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_mix_texture_param");
	gLocModelMatrixEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_model_matrix");
	gLocViewMatrixEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_view_matrix");
	gLocProjectionMatrixEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_projection_matrix");
	gLocObjectColorEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_object_color");
	gLocUseFakeLightingEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_useFakeLighting");
	for(int i=0; i<6; i++)
	{
		std::string result = "light[" + std::to_string(i) + "].ambient";
	gLocLightAmbientEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].diffuse";
	gLocLightDiffuseEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].specular";
	gLocLightSpecularEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].position";
	gLocLightPositionEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].lightType";
	gLocLightTypeEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].direction";
	gLocLightDirectionEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].constant";
	gLocLightAttenuationConstantEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].linear";
	gLocLightAttenuationLineraEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].quadratic";
	gLocLightAttenuationQuadraticEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].cutoff";
	gLocLightCutoffEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	result = "light[" + std::to_string(i) + "].outerCutoff";
	gLocLightOuterCutoffEntityMaps[i] = glGetUniformLocation(gShaderProgramEntityMaps, result.c_str());
	}
	gLocViewerPositionEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "u_viewer_position");
	gLocMaterialDiffuseEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "material.diffuse");
	gLocMaterialSpecularEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "material.specular");
	gLocMaterialShininessEntityMaps = glGetUniformLocation(gShaderProgramEntityMaps, "material.shininess");
	gLocSkyColor = glGetUniformLocation(gShaderProgramEntityMaps, "u_sky_color");
}


void loadObjectColorEntityMaps(glm::vec3 objectColor)
{
	setVector3v(gLocObjectColorEntityMaps, objectColor);

}

void loadSkyColorEntityMaps(glm::vec3 objectColor)
{
	setVector3v(gLocSkyColor, objectColor);

}

// set the position offset to the triangle
void loadPositionOffsetEntityMaps(float offset)
{
	setFloat(gLocPositionOffsetEntityMaps, offset);
}

// bind texture units
void bindTextureUnitsEntityMaps(struct TexturedModel texturedModel, struct EntityMapMaterial entityMapMapterial)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.diffuse);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, entityMapMapterial.specular);
}

void loadBricksTextureSamplerEntityMaps(GLuint samplerID)
{
	setInt(gLocBricksSamplerEntityMaps, samplerID);
}

void loadViewerPositionEntityMaps(glm::vec3 viewerPos)
{
	setVector3v(gLocViewerPositionEntityMaps, viewerPos);
}

void loadFaceTextureSamplerEntityMaps(GLuint samplerID)
{
	setInt(gLocFaceSamplerEntityMaps, samplerID);
}

void loadDiffuseSamplerEntityMaps(GLuint samplerID)
{
	setInt(gLocMaterialDiffuseEntityMaps, samplerID);
}

void loadSpecularSamplerEntityMaps(GLuint samplerID)
{
	setInt(gLocMaterialSpecularEntityMaps, samplerID);
}

void loadMixParamEntityMaps(GLfloat value)
{
	setFloat(gLocMixParamEntityMaps, value);
}

void loadUseFakeLightingEntityMaps(bool value)
{
	setFloat(gLocUseFakeLightingEntityMaps, value==true ? 1 : 0);
}

void loadModelMatrixEntityMaps(glm::mat4 transformationMatrix)
{
	setMat4(gLocModelMatrixEntityMaps, transformationMatrix);
}

void loadViewMatrixEntityMaps(glm::mat4 transformationMatrix)
{
	setMat4(gLocViewMatrixEntityMaps, transformationMatrix);
}

void loadProjectionMatrixEntityMaps(glm::mat4 transformationMatrix)
{
	setMat4(gLocProjectionMatrixEntityMaps, transformationMatrix);
}

void loadMaterialPropertiesEntityMaps(struct EntityMapMaterial material)
{
	setFloat(gLocMaterialShininessEntityMaps, material.shininess);
}

void loadLightPropertiesEntityMaps(std::vector<struct Light> lights, struct Camera* camera)
{
	for (int i = 0; i < lights.size(); i++)
	{
		setVector3v(gLocLightAmbientEntityMaps[i], lights[i].ambient);
		setVector3v(gLocLightDiffuseEntityMaps[i], lights[i].diffuse);
		setVector3v(gLocLightSpecularEntityMaps[i], lights[i].specular);

		// for directional light
		if (lights[i].lightType == 0) // if directional light
		{
			setVector3v(gLocLightDirectionEntityMaps[i], lights[i].lightDirection);
		}

		// for point light
		if (lights[i].lightType == 1)
		{
			setVector3v(gLocLightPositionEntityMaps[i], lights[i].lightPosition);
			setFloat(gLocLightAttenuationConstantEntityMaps[i], lights[i].constant);
			setFloat(gLocLightAttenuationLineraEntityMaps[i], lights[i].linear);
			setFloat(gLocLightAttenuationQuadraticEntityMaps[i], lights[i].quadratic);
		}

		// for spot light
		if (lights[i].lightType == 2)
		{
			setVector3v(gLocLightPositionEntityMaps[i], camera->position);
			setVector3v(gLocLightDirectionEntityMaps[i], camera->cameraFront);
			setFloat(gLocLightCutoffEntityMaps[i], lights[i].cutoff);
			setFloat(gLocLightOuterCutoffEntityMaps[i], lights[i].outerCutoff);
		}
		setInt(gLocLightTypeEntityMaps[i], lights[i].lightType);
	}
}
