#ifndef H_ASSIMP_LOADER
#define H_ASSIMP_LOADER

// headers
#include<iostream>
#include<stdio.h>
#include<Windows.h>
#include<vector>

#include <string>
#include <fstream>
#include <sstream>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include"Mesh.h"
//#include"SOIL.h"
//#include"stb_image.h"
#include"Loader.h"
using namespace std;

/*
a model in its entirety, that is, a model that contains multiple meshes, possibly with multiple objects.
A house, that contains a wooden balcony, 
a tower and perhaps a swimming pool could still be loaded as a single model.
*/

// functions
std::vector<Mesh> loadModelUsingAssimp(string path);
#endif // !H_ASSIMP_LOADER
