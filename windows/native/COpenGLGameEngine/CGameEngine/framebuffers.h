#ifndef H_FRAMEBUFFERS
#define H_FRAMEBUFFERS

// headers
#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL
#include"Logger.h" // for logging
#include"GameUtils.h" // for Gaming utilities
#include"DisplayManager.h" // for width and height of the screen

GLuint setUpFrameBuffer(struct FBO *gFbo); // setup a framebuffer and return it's id
void bindFbo(GLuint fbo);
void unbindFbo();
#endif // !H_FRAMEBUFFERS
