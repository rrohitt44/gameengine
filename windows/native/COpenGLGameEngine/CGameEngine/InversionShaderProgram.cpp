#include"InversionShaderProgram.h"

using namespace std;

GLuint gShaderProgramInversion;
static GLuint gLocTextureSamplerInversion;

// gets all uniform locations
void getAllUniformLocationsInversion()
{
	gLocTextureSamplerInversion = glGetUniformLocation(gShaderProgramInversion, "u_texture_sampler");
}

void loadTextureSamplerInversion(GLuint value)
{
	setInt(gLocTextureSamplerInversion, value);
}

void bindTexturesInversion(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}