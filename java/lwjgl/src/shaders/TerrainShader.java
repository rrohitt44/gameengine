package shaders;

import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import entities.Camera;
import entities.Light;
import toolbox.Maths;

/**
 * @author Rohit Muneshwar
 * @date May 22, 2019
 */
public class TerrainShader extends ShaderProgram
{
	private static final int MAX_LIGHTS = 4;
	private static final String vertexShaderFile = "src/shaders/terrainVertexShader";
	private static final String fragmentShaderFile = "src/shaders/terrainFragmentShader";
	
	// uniform locations
	private int locationTransformationMatrix;
	private int locationProjectionMatrix;
	private int locationViewMatrix;
	
	private int locationLightPosition[]; // light position
	private int locationLightColor[]; // light color
	private int locationLightAttenuation[]; // attenuation
	
	// shininess
	private int locShineDamper;
	private int locReflectivity;
	
	// for fog calculations
	private int locSkyColor;
	private int locFogDensity;
	private int locFogGradient;
	
	// multi-texturing
	private int location_backgroundTexture;
	private int location_rTexture;
	private int location_gTexture;
	private int location_bTexture;
	private int location_blendMap;
	
	// clip plane
	private int locClipPlane;
	
	public TerrainShader()
	{
		super(vertexShaderFile, fragmentShaderFile);
	}

	@Override
	public void bindAttributes()
	{
		super.bindAttribute(0, "aPosition");
		super.bindAttribute(1, "aTexCoords");
		super.bindAttribute(2, "aNormal");
	}

	@Override
	public void getAllUniformLocations()
	{
		locationTransformationMatrix = super.getUniformLocation("u_transaformation_matrix");
		locationProjectionMatrix = super.getUniformLocation("u_projection_matrix");
		locationViewMatrix = super.getUniformLocation("u_view_matrix");
		locShineDamper =super.getUniformLocation("u_shine_damper");
		locSkyColor = super.getUniformLocation("u_sky_color");
		locFogDensity = super.getUniformLocation("u_density");
		locFogGradient = super.getUniformLocation("u_gradient");
		
		// multi-texturin
		location_backgroundTexture = super.getUniformLocation("u_background_texture_sampler");
		location_rTexture = super.getUniformLocation("u_r_texture_sampler");
		location_gTexture = super.getUniformLocation("u_g_texture_sampler");
		location_bTexture = super.getUniformLocation("u_b_texture_sampler");
		location_blendMap = super.getUniformLocation("u_blend_map_texture_sampler");
		
		// multiple lights
		locationLightPosition = new int[MAX_LIGHTS];
		locationLightColor = new int[MAX_LIGHTS];
		locationLightAttenuation = new int[MAX_LIGHTS];
		for(int i=0; i<MAX_LIGHTS; i++)
		{
			locationLightPosition[i] = super.getUniformLocation("u_light_position["+i+"]");
			locationLightColor[i] = super.getUniformLocation("u_light_color["+i+"]");
			locationLightAttenuation[i] = super.getUniformLocation("u_attenuation["+i+"]");
		}
		
		// cliping plane
		locClipPlane = super.getUniformLocation("u_plane");
		
	}
	
	/**
	 * Connects all the necessary texture units
	 */
	public void connectTextureUnits()
	{
		super.loadInt(location_backgroundTexture, 0);
		super.loadInt(location_rTexture, 1);
		super.loadInt(location_gTexture, 2);
		super.loadInt(location_bTexture, 3);
		super.loadInt(location_blendMap, 4);
	}
	
	/**
	 * @param density
	 * @param gradient
	 * Loads fog parameters
	 */
	public void loadFogParameters(float density, float gradient)
	{
		super.loadFloat(locFogDensity, density);
		super.loadFloat(locFogGradient, gradient);
	}
	
	
	/**
	 * @param r
	 * @param g
	 * @param b
	 * Loads sky color
	 */
	public void loadSkyColor(float r, float g, float b)
	{
		loadSkyColor(new Vector3f(r,g,b));
	}
	
	/**
	 * @param skyColor
	 *  Loads sky color
	 */
	public void loadSkyColor(Vector3f skyColor)
	{
		super.loadVector(locSkyColor, skyColor);
	}

	/**
	 * @param transformationMatrix
	 * transformation means translation, rotation and scaling
	 */
	public void loadTransformationMatrix(Matrix4f transformationMatrix)
	{
		super.loadMatrix(locationTransformationMatrix, transformationMatrix);
	}
	
	/**
	 * @param damper
	 * @param reflectivity
	 * Loads the shine variables
	 */
	public void loadShineVariables(float damper, float reflectivity)
	{
		super.loadFloat(locShineDamper, damper);
		super.loadFloat(locReflectivity, reflectivity);
	}
	
	/**
	 * @param light
	 * Loads the light uniform variables
	 */
	public void loadLights(List<Light> lights)
	{
		for(int i=0; i<MAX_LIGHTS; i++)
		{
			if(i<lights.size())
			{
				super.loadVector(locationLightPosition[i], lights.get(i).getPosition());
				super.loadVector(locationLightColor[i], lights.get(i).getColor());
				super.loadVector(locationLightAttenuation[i], lights.get(i).getAttenuation());
			}else
			{
				super.loadVector(locationLightPosition[i], new Vector3f(0,0,0));
				super.loadVector(locationLightColor[i], new Vector3f(0, 0, 0));
				super.loadVector(locationLightAttenuation[i], new Vector3f(1,0,0));
			}
		}
	}
	
	/**
	 * @param plane
	 * Load a cliping plane
	 */
	public void loadClipPlane(Vector4f plane)
	{
		super.loadVector(locClipPlane, plane);
	}
	
	/**
	 * @param projectionMatrix
	 * loads the projection matrix
	 */
	public void loadProjectionMatrix(Matrix4f projectionMatrix)
	{
		super.loadMatrix(locationProjectionMatrix, projectionMatrix);
	}
	
	/**
	 * @param camera
	 * loads the projection matrix
	 */
	public void loadViewMatrix(Camera camera)
	{
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		super.loadMatrix(locationViewMatrix, viewMatrix);
	}
}
