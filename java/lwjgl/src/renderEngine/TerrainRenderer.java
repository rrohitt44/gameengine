/**
 * 
 */
package renderEngine;

import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import models.RawModel;
import shaders.TerrainShader;
import terrains.Terrain;
import terrains.TerrainTexturePack;
import textures.ModelTexture;
import toolbox.Maths;

/**
 * @author Rohit Muneshwar
 * @date May 22, 2019
 */
public class TerrainRenderer
{
	private TerrainShader shader;
	private static float TERRAIN_SHINE_DAMPER = 1f;
	private static float TERRAIN_REFLECTIVITY = 0f;
	
	public TerrainRenderer(TerrainShader shader,Matrix4f projectionMatrix)
	{
		this.shader = shader;
		
		shader.start();
		shader.connectTextureUnits();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
	}
	
	/**
	 * @param terrains
	 * Render the terrain
	 */
	public void render(List<Terrain> terrains)
	{
		for(Terrain terrain: terrains)
		{
			prepareTerrain(terrain); // prepare terrain
			loadModelMatrix(terrain); // loads model matrix
			// render here
			GL11.glDrawElements(
					GL11.GL_TRIANGLES, // draw triangles
					terrain.getRawModel().getVertexCount(), // indices count
					GL11.GL_UNSIGNED_INT, // type of data
					0 // indices buffer offset
					);
			unbindTexturedModel(); // unbind the texture models
		}
	}
	
	/**
	 * @param model
	 * prepare the textured model like enabling the attributes starting the shader, etc...
	 */
	public void prepareTerrain(Terrain terrain)
	{
		RawModel rawModel = terrain.getRawModel();
		// bind the VAO for rendering
		GL30.glBindVertexArray(rawModel.getVaoID());
		GL20.glEnableVertexAttribArray(0); // enable the attribute in the shader
		GL20.glEnableVertexAttribArray(1); // enable the attribute in the shader
		GL20.glEnableVertexAttribArray(2); // enable the attribute in the shader
		bindTextures(terrain);
		shader.loadShineVariables(TERRAIN_SHINE_DAMPER, TERRAIN_REFLECTIVITY);
		
		
	}
	
	/**
	 * @param terrain
	 * Binds all the textures required for the terrain rendering
	 */
	private void bindTextures(Terrain terrain)
	{
		TerrainTexturePack texturePack = terrain.getTexturePack();
		// tell OpenGL that which texture you want to render over the quad
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getBackgroundTexture().getTextureID());
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getrTexture().getTextureID());
		GL13.glActiveTexture(GL13.GL_TEXTURE2);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getgTexture().getTextureID());
		GL13.glActiveTexture(GL13.GL_TEXTURE3);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getbTexture().getTextureID());
		GL13.glActiveTexture(GL13.GL_TEXTURE4);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, terrain.getBlendMap().getTextureID());
	}
	/**
	 * Unbind the textured model
	 */
	private void unbindTexturedModel()
	{
		GL20.glDisableVertexAttribArray(0); // disable the attribute in the shader
		GL20.glDisableVertexAttribArray(1); // disable the attribute in the shader
		GL20.glDisableVertexAttribArray(2); // disable the attribute in the shader
		GL30.glBindVertexArray(0); // unbind the VAO
	}
	
	/**
	 * @param entity
	 * prepare the entity for the transformation models
	 */
	private void loadModelMatrix(Terrain terrain)
	{
		// set the uniforms to the shaders
		Matrix4f transformationMatrix = Maths.createTransformationMatrix(
				new Vector3f(terrain.getX(), 0, terrain.getZ()), // position
				0, 0, 0, // rotation
				1 // scale
				);
		shader.loadTransformationMatrix(transformationMatrix);
				
	}
}
