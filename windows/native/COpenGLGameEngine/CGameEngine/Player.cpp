#include"Player.h"


struct EntityMap player;
float gCurrentSpeed = 0;
float gCurrentTurnSpeed = 0;

float PLAYER_RUN_SPEED = 20;
float PLAYER_TURN_SPEED = 160;
float GRAVITY = -50;
float JUMP_POWER = 10;
float upwardSpeed = 0;

float TERRAIN_HEIGHT = 0;
bool isInAir = false;
extern bool gWKeyPressed;
extern bool gSKeyPressed;
extern bool gAKeyPressed;
extern bool gDKeyPressed;

void movePlayer()
{
	if (gWKeyPressed == true)
	{
		gCurrentSpeed = PLAYER_RUN_SPEED;
	}
	else if (gSKeyPressed == true)
	{
		gCurrentSpeed = -PLAYER_RUN_SPEED;
	}
	else
	{
		gCurrentSpeed = 0;
	}

	if (gAKeyPressed == true)
	{
		gCurrentTurnSpeed = PLAYER_TURN_SPEED;
	}
	else if (gDKeyPressed == true)
	{
		gCurrentTurnSpeed = -PLAYER_TURN_SPEED;
	}
	else
	{
		gCurrentTurnSpeed = 0;
	}

	if (gSpacePressed == true)
	{
		if(!isInAir)
		{
		upwardSpeed = JUMP_POWER;
		isInAir = true;
		}
	}
	player.rotateY += gCurrentTurnSpeed * deltaTime;
	float distance = gCurrentSpeed * deltaTime;
	float dx = distance * sin(toRadians(player.rotateY));
	float dz = distance * cos(toRadians(player.rotateY));
	player.translate += glm::vec3(dx, 0, dz);

	upwardSpeed += GRAVITY * deltaTime;
	player.translate.y += upwardSpeed * deltaTime;
	if (player.translate.y < TERRAIN_HEIGHT)
	{
		upwardSpeed = 0;
		isInAir = false;
		player.translate.y = TERRAIN_HEIGHT;
	}
	
	//reset keys
	/*gWKeyPressed = false;
	gSKeyPressed = false;
	gAKeyPressed = false;
	gDKeyPressed = false;
	gSpacePressed = false;*/
}