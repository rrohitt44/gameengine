// headers
#include"EntityMapsRenderer.h"
using namespace std;

// global variables
std::vector<EntityMap> entityMaps;
std::vector<EntityMap> objEntityMaps;
std::vector<Mesh> gLoadedModels;
extern GLuint gShaderProgramEntityMaps;
extern struct Camera camera;

// prepare the entities for rendering
void prepareEntityMapsRenderer(glm::mat4 projectionMatrix)
{
	// local variables
	const string vertexFile = SHADER_RESOURCE_FILE_LOC + string("vertexShaderEntityMaps.vs");
	const string fragmentFile = SHADER_RESOURCE_FILE_LOC + string("fragmentShaderEntityMaps.fs");

	// code
	// create shader program object
	gShaderProgramEntityMaps = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("Entity maps Shader program object created.");

	// Load uniforms
	getAllUniformLocationsEntityMaps(); // get the uniform locations
	logStaticData("Loaded entity maps uniform locations.");

	startProgram(gShaderProgramEntityMaps);
	loadProjectionMatrixEntityMaps(projectionMatrix);
	loadDiffuseSamplerEntityMaps(0);
	loadSpecularSamplerEntityMaps(1);
	loadSkyColorEntityMaps(glm::vec3(RED, GREEN, BLUE));
	stopProgram();
}

// add entity to the entity list for rendering
void processEntityMaps(struct EntityMap entity)
{
	entityMaps.push_back(entity);
}

void processObjModels(struct EntityMap models)
{
	objEntityMaps.push_back(models);
}

// add the list of meshes of the models for rendering
void processLoadedModels(std::vector<Mesh> models)
{
	gLoadedModels.insert(gLoadedModels.end(), models.begin(), models.end());
}

// render entities
void renderEntityMaps()
{
	for (int i = 0; i < entityMaps.size(); i++)
	{
		glm::mat4 transformationMatrix = glm::mat4(1.0);
		// load uniforms
		loadModelMatrixEntityMaps(getTransformationMatrixEntityMaps(transformationMatrix, entityMaps[i]));
		loadMaterialPropertiesEntityMaps(entityMaps[i].material);
		// bind texture units
		bindTextureUnitsEntityMaps(entityMaps[i].texturedModel, entityMaps[i].material);
		// draw entity
		draw(entityMaps[i].texturedModel.rawModel.vertexCount, true);
	}
}

void renderObjEntityMaps(std::vector<struct Light> lights, struct Camera* camera, glm::mat4 projectionMatrix)
{
	
	for (int i = 0; i < objEntityMaps.size(); i++)
	{
		// bind VAO
		glBindVertexArray(objEntityMaps[i].texturedModel.rawModel.vaoID);
		loadUseFakeLightingEntityMaps(objEntityMaps[i].useFakeLighting);
		glm::mat4 transformationMatrix = glm::mat4(1.0);
		// load uniforms
		loadModelMatrixEntityMaps(getTransformationMatrixEntityMaps(transformationMatrix, objEntityMaps[i]));
		loadMaterialPropertiesEntityMaps(objEntityMaps[i].material);
		// bind texture units
		bindTextureUnitsEntityMaps(objEntityMaps[i].texturedModel, objEntityMaps[i].material);
		// draw entity
		draw(objEntityMaps[i].texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
	}
}
void renderLoadedModels()
{
	for (unsigned int i = 0; i < gLoadedModels.size(); i++)
	{
		gLoadedModels[i].draw(gShaderProgramEntityMaps);
	}
}

// clean up activities
void cleanUpEntityMaps()
{
	for (int i = 0; i < entityMaps.size(); i++)
	{
		// delete VAOS
		if (entityMaps[i].texturedModel.rawModel.vaoID)
		{
			glDeleteVertexArrays(1, &entityMaps[i].texturedModel.rawModel.vaoID);
			entityMaps[i].texturedModel.rawModel.vaoID = 0;
		}
	}
	entityMaps.clear();
	gLoadedModels.clear();
	objEntityMaps.clear();
}

void increateEntityMapsRotation(float rotX, float rotY, float rotZ)
{
	for (int i = 0; i < entityMaps.size(); i++)
	{
		entityMaps[i].rotateX = rotX;
		entityMaps[i].rotateY = rotY;
		entityMaps[i].rotateZ = rotZ;
	}
}