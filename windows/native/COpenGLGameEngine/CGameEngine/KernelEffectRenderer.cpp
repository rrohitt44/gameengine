// headers
#include"KernelEffectRenderer.h"

// global variables
extern GLuint gShaderProgramKernelEffect;
extern struct Entity fillScreenQuadEntity;

void prepareKernelEffect(struct FBO * fbo)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("fillScreenQuad.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("kernel_effect.fs");

	// code
	// create shader program object
	gShaderProgramKernelEffect = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("KernelEffect Shader program object created.");

	// Load uniforms
	getAllUniformLocationsInversion(); // get the uniform locations
	logStaticData("Loaded KernelEffect uniform locations.");

	// create FBo for storing inversion buffers
	setUpFrameBuffer(fbo);

	// load uniform data
	startProgram(gShaderProgramKernelEffect);
	loadTextureSamplerKernelEffect(0);
	stopProgram();

}


// render lights
void renderKernelEffect(struct FBO* inputFbo, struct FBO* outputFbo)
{
	bindFbo(outputFbo->fboId);
	startPostProcessing();
	//unbindFbo();
	startProgram(gShaderProgramKernelEffect);
	glBindVertexArray(fillScreenQuadEntity.texturedModel.rawModel.vaoID);
	fillScreenQuadEntity.texturedModel.textureID = inputFbo->colorAttachment;
		bindTexturesInversion(fillScreenQuadEntity.texturedModel);
		// draw entity
		draw(fillScreenQuadEntity.texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
		stopProgram();
		unbindFbo();
		stopPostProcessing();
}