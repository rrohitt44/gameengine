#ifndef H_SHADERPROGRAM_TERRAIN
#define H_SHADERPROGRAM_TERRAIN

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"
#include"Camera.h"

void getAllUniformLocationsTerrain(); // get uniform locations

// uniforms
extern GLuint gShaderProgramTerrain;

void bindTextureUnitsTerrain(struct TexturedModel modelTexture, struct EntityMapMaterial entityMapMapterial);
void loadDiffuseSamplerTerrain(GLuint samplerID);
void loadSpecularSamplerTerrain(GLuint samplerID);
void loadSamplersTerrain();
void loadModelMatrixTerrain(glm::mat4 transformationMatrix);
void loadViewMatrixTerrain(glm::mat4 transformationMatrix);
void loadProjectionMatrixTerrain(glm::mat4 transformationMatrix);
void loadViewerPositionTerrain(glm::vec3 viewerPos);
void loadMaterialPropertiesTerrain(struct EntityMapMaterial material);
void loadLightPropertiesTerrain(std::vector<struct Light> lights, struct Camera* camera);
void loadSkyColorTerrain(glm::vec3 objectColor);
#endif // !H_SHADERPROGRAM_TERRAIN
