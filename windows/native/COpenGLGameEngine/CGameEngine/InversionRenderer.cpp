// headers
#include"InversionRenderer.h"

// global variables
extern GLuint gShaderProgramInversion;
extern struct Entity fillScreenQuadEntity;

void prepareInversion(struct FBO * inversionFbo)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("fillScreenQuad.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("inversion.fs");

	// code
	// create shader program object
	gShaderProgramInversion = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("inversion Shader program object created.");

	// Load uniforms
	getAllUniformLocationsInversion(); // get the uniform locations
	logStaticData("Loaded inversion uniform locations.");

	// create FBo for storing inversion buffers
	setUpFrameBuffer(inversionFbo);

	// load uniform data
	startProgram(gShaderProgramInversion);
	loadTextureSamplerInversion(0);
	stopProgram();

}


// render lights
void renderInversion(struct FBO* inputFbo, struct FBO* outputFbo)
{
	bindFbo(outputFbo->fboId);
	startPostProcessing();
	//unbindFbo();
	startProgram(gShaderProgramInversion);
	glBindVertexArray(fillScreenQuadEntity.texturedModel.rawModel.vaoID);
	fillScreenQuadEntity.texturedModel.textureID = inputFbo->colorAttachment;
		bindTexturesInversion(fillScreenQuadEntity.texturedModel);
		// draw entity
		draw(fillScreenQuadEntity.texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
		stopProgram();
		unbindFbo();
		stopPostProcessing();
}