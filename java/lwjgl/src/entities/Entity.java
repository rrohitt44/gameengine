package entities;

import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;

/**
 * @author rohit
 * Date: 21/05/2019
 * 
 * This class represents an entity in our game which has its own translation rotation and scaling factors
 *  which is an instance of the textured model
 */
public class Entity
{
	private TexturedModel texturedModel;
	private Vector3f position;
	private float rotX, rotY, rotZ;
	private float scale;
	
	private int textureIndex = 0; // for texture atlas
	
	public Entity(TexturedModel texturedModel, Vector3f position, float rotX, float rotY, float rotZ, float scale)
	{
		super();
		this.texturedModel = texturedModel;
		this.position = position;
		this.rotX = rotX;
		this.rotY = rotY;
		this.rotZ = rotZ;
		this.scale = scale;
	}
	
	public Entity(TexturedModel texturedModel, int index, Vector3f position, float rotX, float rotY, float rotZ, float scale)
	{
		super();
		this.texturedModel = texturedModel;
		this.position = position;
		this.rotX = rotX;
		this.rotY = rotY;
		this.rotZ = rotZ;
		this.scale = scale;
		this.textureIndex = index;
	}
	
	/**
	 * @return
	 * Get the X offset of the texture atlas
	 */
	public float getTextureXOffset()
	{
		int column = textureIndex % texturedModel.getModelTexture().getNumberOfRows();
		return (float) column/(float) texturedModel.getModelTexture().getNumberOfRows();
	}
	
	/**
	 * @return
	 * Get the Y offset of the texture atlas
	 */
	public float getTextureYOffset()
	{
		int row = textureIndex / texturedModel.getModelTexture().getNumberOfRows();
		return (float) row/(float) texturedModel.getModelTexture().getNumberOfRows();
	}
	
	public TexturedModel getTexturedModel()
	{
		return texturedModel;
	}
	public void setTexturedModel(TexturedModel texturedModel)
	{
		this.texturedModel = texturedModel;
	}
	public Vector3f getPosition()
	{
		return position;
	}
	public void setPosition(Vector3f position)
	{
		this.position = position;
	}
	public float getRotX()
	{
		return rotX;
	}
	public void setRotX(float rotX)
	{
		this.rotX = rotX;
	}
	public float getRotY()
	{
		return rotY;
	}
	public void setRotY(float rotY)
	{
		this.rotY = rotY;
	}
	public float getRotZ()
	{
		return rotZ;
	}
	public void setRotZ(float rotZ)
	{
		this.rotZ = rotZ;
	}
	public float getScale()
	{
		return scale;
	}
	public void setScale(float scale)
	{
		this.scale = scale;
	}
	
	/**
	 * @param dx
	 * @param dy
	 * @param dz
	 * 
	 * Move the entity as specified in the parameters
	 */
	public void increasePosition(float dx, float dy, float dz)
	{
		this.position.x += dx;
		this.position.y += dy;
		this.position.z += dz;
	}
	
	/**
	 * @param dx
	 * @param dy
	 * @param dz
	 * 
	 * rotate the entity as specified in the parameters
	 */
	public void increaseRotation(float dx, float dy, float dz)
	{
		this.rotX += dx;
		this.rotY += dy;
		this.rotZ += dz;
	}
}
