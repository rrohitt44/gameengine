#ifndef H_LOGGER
#define H_LOGGER

// headers
#include<stdio.h>
#include<Windows.h>
#include<iostream>
#include<string>
//log file
extern FILE* gLogfile;
void openFileLogger();
void logStaticData(const char data[]);
void logString(std::string data);
void closeFileLogger();
#endif // !H_LOGGER
