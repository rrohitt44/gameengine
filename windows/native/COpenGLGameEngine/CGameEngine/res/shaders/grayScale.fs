#version 330 core

in vec2 out_tex_coords;

out vec4 FragColor;

uniform sampler2D u_texture_sampler;

void main()
{
	FragColor = texture(u_texture_sampler, out_tex_coords);
    float average = 0.2126 * FragColor.r + 0.7152 * FragColor.g + 0.0722 * FragColor.b;
    FragColor = vec4(average, average, average, 1.0);
}