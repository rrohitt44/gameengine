#include"GeneralShaderProgram.h"

using namespace std;

GLuint gShaderProgramGeneral;
static GLuint gLocModelMatrix;
static GLuint gLocViewMatrix;
static GLuint gLocProjectionMatrix;
static GLuint gLocTextureSampler;

// gets all uniform locations
void getAllUniformLocationsGeneral()
{
	gLocTextureSampler = glGetUniformLocation(gShaderProgramGeneral, "u_texture_sampler");
	gLocModelMatrix = glGetUniformLocation(gShaderProgramGeneral, "u_model_matrix");
	gLocViewMatrix = glGetUniformLocation(gShaderProgramGeneral, "u_view_matrix");
	gLocProjectionMatrix = glGetUniformLocation(gShaderProgramGeneral, "u_projection_matrix");
}

void loadModelMatrixGeneral(glm::mat4 transformationMatrix)
{
	setMat4(gLocModelMatrix, transformationMatrix);
}

void loadViewMatrixGeneral(glm::mat4 transformationMatrix)
{
	setMat4(gLocViewMatrix, transformationMatrix);
}

void loadProjectionMatrixGeneral(glm::mat4 transformationMatrix)
{
	setMat4(gLocProjectionMatrix, transformationMatrix);
}

void loadGeneralTextureSampler(GLuint value)
{
	setInt(gLocTextureSampler, value);
}

void bindTexturesGeneral(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}