// headers
#include"OpenGLUtils.h"

// enables wireframe mode
// wireframe mode shows that the model indeed consists of a triangles.
// It configures how OpenGL draws it's primitives
void enableWireframeMode()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//logStaticData("Enabled wireframe mode");
}

// disables wireframe mode
void disableWireframeMode()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//logStaticData("Disabled wireframe mode");
}

// enables depth testing
void enableDepthTesting()
{
	glEnable(GL_DEPTH_TEST);
	//glDepthMask(GL_TRUE); // perform the depth test on all fragments and discard them accordingly, but not update the depth buffer if set to GL_FALSE
	//glDepthFunc(GL_LESS); // Passes if the fragment's depth value is less than the stored depth value.
	//logStaticData("Enabled depth testing");
}

// disable depth testing
void disableDepthTesting()
{
	glDisable(GL_DEPTH_TEST);
	//logStaticData("Disabled depth testing");
}

// starts the shader program
void startProgram(GLuint shaderProgram)
{
	glUseProgram(shaderProgram);
}

// stops the shader program
void stopProgram()
{
	glUseProgram(0);
}

void enableStencileTesting()
{
	glEnable(GL_STENCIL_TEST);

	//allows us to set a bitmask that is ANDed with the stencil value about to be written to the buffer
	//glStencilMask(0xFF); // each bit is written to the stencil buffer as is
	//glStencilMask(0x00); // each bit ends up as 0 in the stencil buffer (disabling writes)

	/*
	sfail: action to take if the stencil test fails.
	dpfail: action to take if the stencil test passes, but the depth test fails.
	dppass: action to take if both the stencil and the depth test pass.
	*/
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE); // updating the stencil buffer with 1s wherever the objects' fragments are rendered
}

void disableStencileTesting()
{
	glDisable(GL_STENCIL_TEST);
}

/*
enableBlending:
While discarding fragments is great and all, it doesn't give us the flexibility to render semi-transparent images; 
we either render the fragment or completely discard it. To render images with different levels of transparency 
we have to enable blending. Like most of OpenGL's functionality we can enable blending by enabling GL_BLEND
*/
void enableBlending()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void disableBlending()
{
	glDisable(GL_BLEND);
}

void enableFaceCulling()
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
}

void disableFaceCulling()
{
	glDisable(GL_CULL_FACE);
}