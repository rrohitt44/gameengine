/**
 * 
 */
package terrains;

/**
 * @author Rohit Muneshwar
 * @date May 24, 2019
 * This class represents terrain texture
 */
public class TerrainTexture
{
	private int textureID;

	
	public TerrainTexture(int textureID)
	{
		super();
		this.textureID = textureID;
	}

	// getters/setters
	public int getTextureID()
	{
		return textureID;
	}

	public void setTextureID(int textureID)
	{
		this.textureID = textureID;
	}
	
	
}
