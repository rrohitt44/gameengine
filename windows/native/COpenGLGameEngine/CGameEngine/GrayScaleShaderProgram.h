#ifndef H_SHADERPROGRAM_GRAYSCALE
#define H_SHADERPROGRAM_GRAYSCALE

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsGrayScale(); // get uniform locations
// uniforms
extern GLuint gShaderProgramGrayScale;

void loadTextureSamplerGrayScale(GLuint value);
void bindTexturesGrayScale(struct TexturedModel model);
#endif // !H_SHADERPROGRAM_GRAYSCALE
