// headers
#include"GrayScaleRenderer.h"

// global variables
extern GLuint gShaderProgramGrayScale;
extern struct Entity fillScreenQuadEntity;

void prepareGrayScale(struct FBO * fbo)
{
	// local variables
	const std::string vertexFile = SHADER_RESOURCE_FILE_LOC + std::string("fillScreenQuad.vs");
	const std::string fragmentFile = SHADER_RESOURCE_FILE_LOC + std::string("grayScale.fs");

	// code
	// create shader program object
	gShaderProgramGrayScale = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("GrayScale Shader program object created.");

	// Load uniforms
	getAllUniformLocationsGrayScale(); // get the uniform locations
	logStaticData("Loaded GrayScale uniform locations.");

	// create FBo for storing inversion buffers
	setUpFrameBuffer(fbo);

	// load uniform data
	startProgram(gShaderProgramGrayScale);
	loadTextureSamplerGrayScale(0);
	stopProgram();

}


void renderGrayScale(struct FBO* inputFbo, struct FBO* outputFbo)
{
	bindFbo(outputFbo->fboId);
	startPostProcessing();
	startProgram(gShaderProgramGrayScale);
	glBindVertexArray(fillScreenQuadEntity.texturedModel.rawModel.vaoID);
	fillScreenQuadEntity.texturedModel.textureID = inputFbo->colorAttachment;
		bindTexturesInversion(fillScreenQuadEntity.texturedModel);
		// draw entity
		draw(fillScreenQuadEntity.texturedModel.rawModel.vertexCount, true);
		glBindVertexArray(0);
		stopProgram();
		unbindFbo();
		stopPostProcessing();
}