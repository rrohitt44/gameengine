#ifndef H_SHADERPROGRAM_BLUREFFECT
#define H_SHADERPROGRAM_BLUREFFECT

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"

void getAllUniformLocationsBlurEffect(); // get uniform locations
// uniforms
extern GLuint gShaderProgramBlurEffect;

void loadTextureSamplerBlurEffect(GLuint value);
void bindTexturesBlurEffect(struct TexturedModel model);
#endif // !H_SHADERPROGRAM_BLUREFFECT
