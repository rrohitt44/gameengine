#ifndef H_SHADERPROGRAM_ENTITY_MAPS
#define H_SHADERPROGRAM_ENTITY_MAPS

#include<iostream>
#include<stdio.h>
#include<vector>
#include<Windows.h>

#define GLEW_STATIC
#include<GL/glew.h>
#include<gl/GL.h> // for OpenGL

#include"ShaderUtils.h"
#include"GameUtils.h"
#include"Models.h"
#include"MathUtils.h"
#include"Logger.h"
#include"Camera.h"

void getAllUniformLocationsEntityMaps(); // get uniform locations

// uniforms
extern GLuint gShaderProgramEntityMaps;
extern GLuint gLocColorEntityMaps;
extern GLuint gLocPositionOffsetEntityMaps;
extern GLuint gLocBricksSamplerEntityMaps;
extern GLuint gLocFaceSamplerEntityMaps;
extern GLuint gLocMixParamEntityMaps;
extern GLuint gLocModelMatrixEntityMaps;
extern GLuint gLocViewMatrixEntityMaps;
extern GLuint gLocProjectionMatrixEntityMaps;
extern GLuint gLocObjectColorEntityMaps;
extern GLuint gLocLightAmbientEntityMaps[6];
extern GLuint gLocLightDiffuseEntityMaps[6];
extern GLuint gLocLightSpecularEntityMaps[6];
extern GLuint gLocLightPositionEntityMaps[6];
extern GLuint gLocLightTypeEntityMaps[6];
extern GLuint gLocLightCutoffEntityMaps[6];
extern GLuint gLocLightOuterCutoffEntityMaps[6];
extern GLuint gLocLightDirectionEntityMaps[6]; // light direction for directional light
extern GLuint gLocLightAttenuationLineraEntityMaps[6];
extern GLuint gLocLightAttenuationQuadraticEntityMaps[6];
extern GLuint gLocLightAttenuationConstantEntityMaps[6];
extern GLuint gLocViewerPositionEntityMaps;
extern GLuint gLocMaterialDiffuseEntityMaps;
extern GLuint gLocMaterialSpecularEntityMaps;
extern GLuint gLocMaterialShininessEntityMaps;

void loadPositionOffsetEntityMaps(float offset);
void bindTextureUnitsEntityMaps(struct TexturedModel modelTexture, struct EntityMapMaterial entityMapMapterial);
void loadBricksTextureSamplerEntityMaps(GLuint samplerID);
void loadFaceTextureSamplerEntityMaps(GLuint samplerID);
void loadDiffuseSamplerEntityMaps(GLuint samplerID);
void loadSpecularSamplerEntityMaps(GLuint samplerID);
void loadMixParamEntityMaps(GLfloat value);
void loadUseFakeLightingEntityMaps(bool value);
void loadModelMatrixEntityMaps(glm::mat4 transformationMatrix);
void loadViewMatrixEntityMaps(glm::mat4 transformationMatrix);
void loadProjectionMatrixEntityMaps(glm::mat4 transformationMatrix);
void loadObjectColorEntityMaps(glm::vec3 objectColor);
void loadSkyColorEntityMaps(glm::vec3 skyColor);
void loadViewerPositionEntityMaps(glm::vec3 viewerPos);
void loadMaterialPropertiesEntityMaps(struct EntityMapMaterial material);
void loadLightPropertiesEntityMaps(std::vector<struct Light> lights, struct Camera* camera);
#endif // !H_SHADERPROGRAM_ENTITY_MAPS
