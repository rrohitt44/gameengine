#include"BlurEffectShaderProgram.h"

using namespace std;

GLuint gShaderProgramBlurEffect;
static GLuint gLocTextureSamplerBlurEffect;

// gets all uniform locations
void getAllUniformLocationsBlurEffect()
{
	gLocTextureSamplerBlurEffect = glGetUniformLocation(gShaderProgramBlurEffect, "u_texture_sampler");
}

void loadTextureSamplerBlurEffect(GLuint value)
{
	setInt(gLocTextureSamplerBlurEffect, value);
}

void bindTexturesBlurEffect(struct TexturedModel model)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, model.textureID);
}