// headers
#include"MathUtils.h"
#include"DisplayManager.h"

extern struct Camera camera;

glm::mat4 creatTranslationMatrix(glm::mat4 translationMatrix, glm::vec3 translateBy)
{
	return glm::translate(translationMatrix, translateBy);
}


// creates orthographic projection matrix
glm::mat4 createOrthographicProjectionMatrix()
{
	return glm::ortho(
		0.0f, // left
		800.0f, // right
		0.0f, // bottom
		600.0f, // top
		0.1f, // neaf
		100.0f // far
	);
}

glm::mat4 createPerspectiveProjectionMatrix(float fov, int width, int height)
{
	glm::mat4 proj = glm::perspective(
		glm::radians(fov), // fovy
		(float) width/ (float) height, // aspect ratio
		0.1f, // near
		1000.0f // far
	);
	return proj;
}

glm::mat4 createViewMatrix(struct Camera camera)
{
	glm::mat4 view = glm::mat4(1.0);
	view = glm::lookAt(
		camera.position, // position
		camera.position + camera.cameraFront, // looking at
		camera.up // up axis
	);
	return view;
}

// create translation matrix from translation vector
glm::mat4 getTransformationMatrix(glm::mat4 transformationMatrix, struct Entity entity)
{
	transformationMatrix = glm::translate(transformationMatrix, entity.translate);
	transformationMatrix = glm::rotate(transformationMatrix, glm::radians(entity.rotateX), glm::vec3(1.0f, 0.0f, 0.0f));
	transformationMatrix = glm::rotate(transformationMatrix, glm::radians(entity.rotateY), glm::vec3(0.0f, 1.0f, 0.0f));
	transformationMatrix = glm::rotate(transformationMatrix, glm::radians(entity.rotateZ), glm::vec3(0.0f, 0.0f, 1.0f));
	transformationMatrix = glm::scale(transformationMatrix, glm::vec3(entity.scale, entity.scale, entity.scale));
	return transformationMatrix;
}

// create translation matrix from translation vector
glm::mat4 getTransformationMatrixEntityMaps(glm::mat4 transformationMatrix, struct EntityMap entity)
{
	transformationMatrix = glm::translate(transformationMatrix, entity.translate);
	transformationMatrix = glm::rotate(transformationMatrix, glm::radians(entity.rotateX), glm::vec3(1.0f, 0.0f, 0.0f));
	transformationMatrix = glm::rotate(transformationMatrix, glm::radians(entity.rotateY), glm::vec3(0.0f, 1.0f, 0.0f));
	transformationMatrix = glm::rotate(transformationMatrix, glm::radians(entity.rotateZ), glm::vec3(0.0f, 0.0f, 1.0f));
	transformationMatrix = glm::scale(transformationMatrix, glm::vec3(entity.scale, entity.scale, entity.scale));
	return transformationMatrix;
}

float toRadians(float value)
{
	float result = value * PI / 180;
	return result;
}

float toDegrees(float value)
{
	float result = value * 180 / PI;
	return result;
}

float getNextFloatRandomNumberBetween0And1()
{
	float val = (float) rand() / (float)RAND_MAX;
	//logString("random number - "+std::to_string(val)+" "+ std::to_string(rand()));
	return val;
}