package guis;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import entities.Camera;
import entities.Light;
import shaders.ShaderProgram;
import toolbox.Maths;

/**
 * @author Rohit Muneshwar
 * @date May 22, 2019
 */
public class GuiShader extends ShaderProgram
{

	private static final String vertexShaderFile = "src/guis/guiVertexShader";
	private static final String fragmentShaderFile = "src/guis/guiFragmentShader";
	
	// uniform locations
	private int locationTransformationMatrix;
	
	public GuiShader()
	{
		super(vertexShaderFile, fragmentShaderFile);
	}

	@Override
	public void bindAttributes()
	{
		super.bindAttribute(0, "aPosition");
	}

	@Override
	public void getAllUniformLocations()
	{
		locationTransformationMatrix = super.getUniformLocation("u_transaformation_matrix");
	}
	
	/**
	 * Connects all the necessary texture units
	 */
	public void connectTextureUnits()
	{
		
	}
	
	
	/**
	 * @param transformationMatrix
	 * transformation means translation, rotation and scaling
	 */
	public void loadTransformationMatrix(Matrix4f transformationMatrix)
	{
		super.loadMatrix(locationTransformationMatrix, transformationMatrix);
	}
}
