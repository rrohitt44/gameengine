// headers
#include"TerrainRenderer.h"
using namespace std;

// global variables
std::vector<EntityMap> terrains;
extern GLuint gShaderProgramTerrain;
extern float gMixParam;
extern struct Camera camera;

// prepare the entities for rendering
void prepareTerrainRenderer(glm::mat4 projectionMatrix)
{
	// local variables
	const string vertexFile = SHADER_RESOURCE_FILE_LOC + string("terrainVertexShader.vs");
	const string fragmentFile = SHADER_RESOURCE_FILE_LOC + string("terrainFragmentShader.fs");

	// code
	// create shader program object
	gShaderProgramTerrain = buildShaderProgramObject(vertexFile.c_str(), fragmentFile.c_str());
	logStaticData("Terrain Shader program object created.");

	// Load uniforms
	getAllUniformLocationsTerrain(); // get the uniform locations
	logStaticData("Loaded terrain uniform locations.");

	startProgram(gShaderProgramTerrain);
	loadProjectionMatrixTerrain(projectionMatrix);
	loadSamplersTerrain();
	loadSkyColorTerrain(glm::vec3(RED, GREEN, BLUE));
	stopProgram();
}

// add entity to the entity list for rendering
void processTerrain(struct EntityMap entity)
{
	terrains.push_back(entity);
}

// render entities
void renderTerrain()
{

	for (int i = 0; i < terrains.size(); i++)
	{
		// bind VAO
		glBindVertexArray(terrains[0].texturedModel.rawModel.vaoID);
		glm::mat4 transformationMatrix = glm::mat4(1.0);
		// load uniforms
		loadModelMatrixTerrain(getTransformationMatrixEntityMaps(transformationMatrix, terrains[i]));
		loadMaterialPropertiesTerrain(terrains[i].material);
		// bind texture units
		bindTextureUnitsTerrain(terrains[i].texturedModel, terrains[i].material);
		// draw entity
		draw(terrains[i].texturedModel.rawModel.vertexCount, true);
		//unbind VAO
		glBindVertexArray(0);
	}
}


// clean up activities
void cleanUpTerrain()
{
	for (int i = 0; i < terrains.size(); i++)
	{
		// delete VAOS
		if (terrains[i].texturedModel.rawModel.vaoID)
		{
			glDeleteVertexArrays(1, &terrains[i].texturedModel.rawModel.vaoID);
			terrains[i].texturedModel.rawModel.vaoID = 0;
		}
	}
	terrains.clear();
}
