// headers
#include"ObjLoader.h"

#define BUFFER_SIZE 256


void loadObjFile(std::string objFileName, RawModel *model)
{
	struct MeshModel mesh;

	char line[BUFFER_SIZE];

	std::vector<std::vector<float>> gVertices;
	std::vector<std::vector<float>> gTexCoords;
	std::vector<std::vector<float>> gNormals;
	std::vector<std::vector<int>> gFaceTri;
	std::vector<std::vector<int>> gFaceTexCoords;
	std::vector<std::vector<int>> gFaceNormal;

	// Open mesh file
	FILE* file;
	fopen_s(&file, objFileName.c_str(), "r");
	if (!file)
	{
		logString("Can not open Obj file "+objFileName+"\n");
	}

	// Seperator strings
	const char *sep_space = " ";
	const char* sep_slash = "/";

	char* first_token = NULL;


	char* face_tokens[3];
	int nr_token;
	char* token=NULL;

	char* token_vertex_index = NULL;
	char* token_texture_index = NULL;
	char* token_normal_index = NULL;

	char* next_token1 = NULL;
	char* next_token2 = NULL;
	// loop till there is a line in a file
	while (fgets(line, BUFFER_SIZE, file) != NULL)
	{
		first_token = strtok_s(line, sep_space, &next_token1);

		if (strcmp(first_token, "v") == 0) // vertex data
		{
			std::vector<float> vec_point_coord(3);
			for (int i = 0; i < 3; i++)
			{
				vec_point_coord[i] = atof(strtok_s(NULL, sep_space, &next_token1));
			}
			gVertices.push_back(vec_point_coord);
		}else if (strcmp(first_token, "vt") == 0) // tex coords data
		{
			std::vector<float> vec_tex_coord(2);
			for (int i = 0; i < 2; i++)
			{
				vec_tex_coord[i] = atof(strtok_s(NULL, sep_space, &next_token1));
			}
			gTexCoords.push_back(vec_tex_coord);
		}else if (strcmp(first_token, "vn") == 0) // normal data
		{
			std::vector<float> vec_normal_coord(3);
			for (int i = 0; i < 3; i++)
			{
				vec_normal_coord[i] = atof(strtok_s(NULL, sep_space, &next_token1));
			}
			gNormals.push_back(vec_normal_coord);
		}else if (strcmp(first_token, "f") == 0) // faces data
		{
			std::vector<int> triangle_vertex_indices(3);
			std::vector<int> texture_vertex_indices(3);
			std::vector<int> normal_vertex_indices(3);

			memset((void *) face_tokens, 0, 3);
			nr_token = 0;

			while (token = strtok_s(NULL, sep_space, &next_token1))
			{
				if (strlen(token) < 3)
				{
					break;
				}
				face_tokens[nr_token] = token;
				nr_token++;
			}

			for (int i = 0; i < 3; i++)
			{
				token_vertex_index = strtok_s(face_tokens[i], sep_slash, &next_token2);
				token_texture_index = strtok_s(NULL, sep_slash, &next_token2);
				token_normal_index = strtok_s(NULL, sep_slash, &next_token2);
				triangle_vertex_indices[i] = atoi(token_vertex_index) -1;
				texture_vertex_indices[i] = atoi(token_texture_index)-1;
				normal_vertex_indices[i] = atoi(token_normal_index)-1;
			}
			gFaceTri.push_back(triangle_vertex_indices);
			gFaceTexCoords.push_back(texture_vertex_indices);
			gFaceNormal.push_back(normal_vertex_indices);
		}
	}

	// load data to VAO
	// setup mesh from data now
	setUpMeshFromVecWithSeperateIndices(&mesh, gVertices,
		gNormals, gTexCoords,
		gFaceTri, gFaceTexCoords, gFaceNormal);

	// create VAO
	loadToVAO(model, &mesh);

	fclose(file);
	file = NULL;
}