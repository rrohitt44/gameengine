// header
#include"Camera.h"

// global variables
struct Camera camera;

glm::vec3 gCameraPosition(camera.position);

float distanceFromPlayer = 10;
float angleAroundPlayer = 0;
float pitchCam = 20;
float yawCam = 0;
float rollCam = 0;

void initializeCamera()
{
	camera.position = player.translate + glm::vec3(0, 0.5, 10);
	updateCameraPosition(camera.position);
		camera.cameraFront = glm::vec3(0.0, 0.0, -1.0); // in front of camera
	camera.up = glm::vec3(0.0, 1.0, 0.0); // up axis

	gCameraPosition = camera.position;
	logStaticData("Camera initialized");
}

void updateCameraPosition(glm::vec3 position)
{
	if (gAKeyPressed == true)
	{
		//gCurrentTurnSpeed = PLAYER_TURN_SPEED;
		angleAroundPlayer -= 0.5f;
	}
	else if (gDKeyPressed == true)
	{
		//gCurrentTurnSpeed = -PLAYER_TURN_SPEED;
		angleAroundPlayer += 0.5f;
	}
	else
	{
		//angleAroundPlayer = 0;
	}

	float dy = distanceFromPlayer * sin(toRadians(pitchCam));
	float dx = distanceFromPlayer * cos(toRadians(pitchCam));

	float theta = player.rotateY + angleAroundPlayer;
	float offsetX = dx * sinf(toRadians(theta));
	float offsetZ = dy * cosf(toRadians(theta));
	camera.position.x = player.translate.x - offsetX;
	camera.position.z = player.translate.z - offsetZ;
	camera.position.y = player.translate.y + dy; // position
	yawCam = 180 - theta;

	/*glm::vec3 front(0,0,0);
	front.x = cos(glm::radians(yawCam)) * cos(glm::radians(yawCam));
	front.y = sin(glm::radians(pitchCam));
	front.z = sin(glm::radians(yawCam)) * cos(glm::radians(pitchCam));
	camera.cameraFront = glm::normalize(front);*/
	//position.z += player.translate.z + 4;
	//position.y += player.translate.y + 3;
	camera.position += position;
}