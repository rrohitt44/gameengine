#ifndef H_KERNELEFFECT_RENDERER
#define H_KERNELEFFECT_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"KernelEffectShaderProgram.h"
#include"Camera.h"
#include"framebuffers.h"
#include"PostProcessing.h"

void prepareKernelEffect(struct FBO* fbo);
void renderKernelEffect(struct FBO* inputFbo, struct FBO* outputFbo); // render entities using FBO textures
#endif // !H_KERNELEFFECT_RENDERER
