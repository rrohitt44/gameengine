package renderEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import models.RawModel;

/**
 * @author Rohit Muneshwar
 * @date May 21, 2019
 * 
 * Loader class for loading the OBJ files and return the model out of it
 */
public class ObjLoader
{
	public static RawModel loadObjModel(String fileName, Loader loader)
	{
		FileReader fr = null;
		try
		{
			fr = new FileReader(new File("resources/" + fileName + ".obj"));
		} catch (FileNotFoundException e)
		{
			System.err.println("Couldn't load file: " + fileName);
			e.printStackTrace();
		}

		// read from file
		BufferedReader reader = new BufferedReader(fr);
		String line; // current line
		List<Vector3f> vertices = new ArrayList<Vector3f>(); // position
		List<Vector2f> textures = new ArrayList<Vector2f>(); // tex coords
		List<Vector3f> normals = new ArrayList<Vector3f>(); // normals
		List<Integer> indices = new ArrayList<Integer>(); // indices

		float[] verticesArray = null; // position array
		float[] normalsArray = null; // normal array
		float[] textureArray = null; // tex coords array
		int[] indicesArray = null; // indices array

		try
		{
			while (true) // we will break the loop when faces line arrive
			{
				line = reader.readLine();
				String[] currentLine = line.split(" ");
				if (line.startsWith("v ")) // vertex position
				{
					Vector3f vertex = new Vector3f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]),
							Float.parseFloat(currentLine[3]));
					vertices.add(vertex);
				} else if (line.startsWith("vt ")) // texcoord
				{
					Vector2f texture = new Vector2f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]));
					textures.add(texture);
				} else if (line.startsWith("vn ")) // normal
				{
					Vector3f normal = new Vector3f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]),
							Float.parseFloat(currentLine[3]));
					normals.add(normal);
				} else if (line.startsWith("f ")) // face; we will break out
													// from here and process
													// further in other loop
													// below
				{
					textureArray = new float[vertices.size() * 2];
					normalsArray = new float[vertices.size() * 3];
					break;
				}
			}

			// process the faces now
			while (line != null)
			{
				if (!line.startsWith("f ")) // if not faces then read next line
											// and continue
				{
					line = reader.readLine();
					continue;
				}

				// we have got the face now
				String[] currentLine = line.split(" ");

				// these will contain position/texcoord/normal data of each
				// coordinate of the vertex
				String[] vertex1 = currentLine[1].split("/"); // first coord of
																// a vertex
				String[] vertex2 = currentLine[2].split("/"); // second coord of
																// a vertex
				String[] vertex3 = currentLine[3].split("/"); // third coord of
																// a vertex

				processVertex(vertex1, indices, textures, normals, textureArray, normalsArray);
				processVertex(vertex2, indices, textures, normals, textureArray, normalsArray);
				processVertex(vertex3, indices, textures, normals, textureArray, normalsArray);
				line = reader.readLine(); // read next line
			}

			reader.close(); // close the reader now
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		// load the vertices into vertices arrya and it's indices into indices array
		verticesArray = new float[vertices.size()*3];
		indicesArray = new int[indices.size()];
		
		int vertexPointer = 0;
		for(Vector3f vertex:vertices){
			verticesArray[vertexPointer++] = vertex.x;
			verticesArray[vertexPointer++] = vertex.y;
			verticesArray[vertexPointer++] = vertex.z;
		}
		
		for(int i=0;i<indices.size();i++){
			indicesArray[i] = indices.get(i);
		}
		
		return loader.loadToVAO(verticesArray, textureArray, normalsArray, indicesArray);
	}
		
	/**
	 * @param vertexData
	 * @param indices
	 * @param textures
	 * @param normals
	 * @param textureArray
	 * @param normalsArray
	 * 
	 * Process each face attribute and store it in appropriate array
	 */
	private static void processVertex(String[] vertexData, List<Integer> indices, List<Vector2f> textures,
			List<Vector3f> normals, float[] textureArray, float[] normalsArray)
	{
		int currentVertexPointer = Integer.parseInt(vertexData[0]) - 1; // position
		indices.add(currentVertexPointer);
		Vector2f currentTex = textures.get(Integer.parseInt(vertexData[1]) - 1); // tex coord
		textureArray[currentVertexPointer * 2] = currentTex.x;
		textureArray[currentVertexPointer * 2 + 1] = 1 - currentTex.y;
		Vector3f currentNorm = normals.get(Integer.parseInt(vertexData[2]) - 1); // normal
		normalsArray[currentVertexPointer * 3] = currentNorm.x;
		normalsArray[currentVertexPointer * 3 + 1] = currentNorm.y;
		normalsArray[currentVertexPointer * 3 + 2] = currentNorm.z;
	}
}
