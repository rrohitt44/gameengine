#ifndef H_INVERSION_RENDERER
#define H_INVERSION_RENDERER

// headers
#include"Models.h"
#include"GameUtils.h"
#include"ShaderProgram.h"
#include"OpenGLUtils.h"
#include"inversionShaderProgram.h"
#include"Camera.h"
#include"framebuffers.h"
#include"PostProcessing.h"

//void processFillScreenQuadEntity(struct Entity* entity);
void prepareInversion(struct FBO* inversionFbo);
void renderInversion(struct FBO* fbo, struct FBO* inversionFbo); // render entities using FBO textures
#endif // !H_INVERSION_RENDERER
